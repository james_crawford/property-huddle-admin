<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

## custom settings

## local settings

//$config['data_files']	= $_SERVER['DOCUMENT_ROOT'].'/data.propertyhuddle/';
//$config['media_files']	= $_SERVER['DOCUMENT_ROOT'].'/media.propertyhuddle/';


## live server settings

$config['data_files']	= '../data/';
$config['media_files']	= '../media/';



/* End of file config.php */
/* Location: ./application/config/config.php */
