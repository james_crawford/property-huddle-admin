<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class profilemodel extends CI_Model
{

	function MyModel()
	{

		parent::__construct();
	}

	public function getprofileDetails($id = '')
	{
		if ($id != '')
		{
			$this->db->select('*');
			$this->db->where('id', $id);
			$query = $this->db->get('admin');
			if ($query->num_rows > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function update_profile()
	{
		$txtPassword = $this->input->post('password');
		if ($txtPassword != '')
		{
			$data = array(
				'Name' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'Email' => $this->input->post('email')
			);
		}
		else
		{
			$data = array(
				'Name' => $this->input->post('username'),
				'Email' => $this->input->post('email')
			);
		}
		$this->db->where(array('id' => $this->input->post('id')));
		return $this->db->update('admin', $data);
	}

}