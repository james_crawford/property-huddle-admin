<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Staff_model extends CI_Model
{

	var $table = "branches";

	public function __construct()
	{
		parent::__construct();
	}

	public function getBranchStaff($id, $agentID = false, $type = false)
	{
		$this->db->select('branches.branch_name, branch_agent_staff.`name`, branch_agent_staff.id ');
		$this->db->join('branches', 'branch_agent_staff.branches_id = branches.id');
		$this->db->from('branch_agent_staff');
		if ($agentID)
		{
			$this->db->where('agents_id', $agentID);
		}
		else
		{
			$this->db->where('branches_id', $id);
		}

		if ($type)
		{
			$this->db->where('branches_agent_staff_type', $id);
		}
		$query = $this->db->get();

		if ($query->num_rows() >= 1)
		{
			$i = 0;
			$array = $query->result_array();
			foreach ($array as $staff)
			{
				$result[$staff['id']] = $staff['name'].' ('.$staff['branch_name'].')';

				$i++;
			}
		}
		else
		{
			$result = array('x' => 'No Staff Assigned');
		}
		return $result;
	}

		public function getStaffRecords($id)
	{
		$this->db->select('name, email,telephone, branch_agent_staff.id, branch_agent_staff.branches_id, agent_id, staff_image_id, active ');
		$this->db->from('branch_agent_staff');
		$this->db->where('branch_agent_staff.branches_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$i = 0;
			$result = $query->result_array();
			foreach($result as $staff)
			{
				if($staff['staff_image_id']!= '0')
				{
					$this->db->select('domain_url, image_name');
					$this->db->from('branch_images');
					$this->db->where('id', $staff['staff_image_id']);
					$imageResult = $this->db->get()->result_array();
					$result[$i]['domain_url'] = $imageResult[0]['domain_url'];
					$result[$i]['image_name'] = $imageResult[0]['image_name'];
				}
				else
				{
					$result[$i]['domain_url'] = "http://admin.propertyhuddle.com/assets/branch/noPhoto.png";
					$result[$i]['image_name'] = "No photo";
				}
				$i++;
			}
			
		}
		else
		{
			$result = false;
		}
		return $result;
	}

	
	
	public function getStaffData($id)
	{
		$this->db->select('name, email,telephone, branch_agent_staff.id, branch_agent_staff.branches_id, agent_id, branch_agent_staff_type, staff_image_id, active ');
		$this->db->from('branch_agent_staff');
		$this->db->where('branch_agent_staff.id', $id);
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$i = 0;
			$result = $query->result_array();
			foreach($result as $staff)
			{
				if($staff['staff_image_id']!= '0')
				{
					$this->db->select('domain_url, image_name');
					$this->db->from('branch_images');
					$this->db->where('id', $staff['staff_image_id']);
					$imageResult = $this->db->get()->result_array();
					$result[$i]['domain_url'] = $imageResult[0]['domain_url'];
					$result[$i]['image_name'] = $imageResult[0]['image_name'];
				}
				else
				{
					$result[$i]['domain_url'] = "http://admin.propertyhuddle.com/assets/branch/noPhoto.png";
					$result[$i]['image_name'] = "No photo";
				}
				$i++;
			}
			
		}
		else
		{
			$result = false;
		}
		return $result[0];
	}
	
	
}

?>
