<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class AgentModel extends CI_Model
{

	var $table = "agents";
	var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %h:%i:%s";
	var $currentDate = '';
	var $currentDateTime = '';

	public function __construct()
	{
		parent::__construct();

		$this->currentDate = mdate($this->datestring, time());
		$this->currentDateTime = mdate($this->dateStringWithTime, time());
	}

	public function getAgentRecord($id = false, $contacts = false)
	{

		$i = 0;
		$agentData = array();
		$this->db->select('agent_name, agent_website, id, company_vat_number, agent_location,agent_postcode, company_registration_number, agent_short_code, agent_email_address, agent_type, agent_telephone_number, status, manager_staff_id, billing_staff_id, trade_staff_id, active,about_us, external_feed');
		$this->db->from('agents');
		if ($id)
		{
			$this->db->where('id', $id);
		}
		$agentQuery = $this->db->get();
		$agentResult = $agentQuery->result_array();
		foreach ($agentResult as $agent)
		{
			$agentData['agents'][$i] = $agent;
			if ($contacts)
			{
				$this->db->select('name As manager_name, email AS manager_email, telephone as  manager_telephone, staff_image_id AS manager_staff_image_id');
				$this->db->from('branch_agent_staff');
				$this->db->where('id', $agentData['agents'][$i]['manager_staff_id']);
				$managerQuery = $this->db->get();
				if ($managerQuery->num_rows() >= 1)
				{
					$managerResult = Common::assocArray($managerQuery->result_array());
					$agentData['agents'][$i]['manager'] = $managerResult[0];
				}
				else
				{
					$agentData['agents'][$i]['manager'] = array(
						'manager_name' => 'Not Set',
						'manager_email' => 'Not Set',
						'manager_telephone' => 'Not Set',
						'manager_staff_image_id' => '0',
					);
				}
				$this->db->select('name As trade_contact_name, email AS trade_contact_email, telephone as  trade_contact_telephone_number, staff_image_id AS trade_contact_staff_image_id');
				$this->db->from('branch_agent_staff');
				$this->db->where('id', $agentData['agents'][$i]['trade_staff_id']);
				$tradeContactQuery = $this->db->get();
				if ($tradeContactQuery->num_rows() >= 1)
				{
					$tradeContactResult = Common::assocArray($tradeContactQuery->result_array());
					$agentData['agents'][$i]['trade_contact'] = $tradeContactResult[0];
				}
				else
				{
					$agentData['agents'][$i]['trade_contact'] = array(
						'trade_contact_name' => 'Not Set',
						'trade_contact_email' => 'Not Set',
						'trade_contact_telephone_number' => 'Not Set',
						'trade_contact_staff_image_id' => '0',
					);
				}
				$this->db->select('name As billing_contact_name, email AS billing_contact_email, telephone as  billing_contact_telephone_number, staff_image_id AS billing_staff_image_id');
				$this->db->from('branch_agent_staff');
				$this->db->where('id', $agentData['agents'][$i]['billing_staff_id']);
				$billingContactQuery = $this->db->get();
				if ($billingContactQuery->num_rows() >= 1)
				{
					$billingResult = Common::assocArray($billingContactQuery->result_array());
					$agentData['agents'][$i]['billing'] = $billingResult[0];
				}
				else
				{
					$agentData['agents'][$i]['billing'] = array(
						'billing_contact_name' => 'Not Set',
						'billing_contact_email' => 'Not Set',
						'billing_contact_telephone_number' => 'Not Set',
						'billing_staff_image_id' => '0');
				}
			}
			$i++;
		}

		if ($id)
		{
			//if we want a single record we only want the array data sent across, not the container too
			return $agentData['agents'][0];
		}
		return $agentData;
	}

	public function saveAgent($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('agents', $data);
	}

	public function getBranchStaff($id, $agentID = false, $type = false)
	{
		$this->db->select('branches.branch_name, branch_agent_staff.`name`, branch_agent_staff.id ');
		$this->db->join('branches', 'branch_agent_staff.branches_id = branches.id');
		$this->db->from('branch_agent_staff');
		if ($agentID)
		{
			$this->db->where('agent_id', $id);
		}
		else
		{
			$this->db->where('branches_id', $id);
		}

		if ($type)
		{
			$this->db->where('branches_agent_staff_type', $id);
		}
		$query = $this->db->get();

		if ($query->num_rows() >= 1)
		{
			$result = $query->result_array();
		}
		else
		{
			$result = array('x' => 'No Staff Assigned');
		}
		return $result;
	}

	public function getAllAgentIDs()
	{
		$result[0] = 'Please select an agent';
		$this->db->select('id, agent_name');
		$this->db->from('agents');
		$this->db->where('active', '1');
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$array = $query->result_array();
			foreach ($array as $agent)
			{
				$result[$agent['id']] = $agent['agent_name'];
			}
		}
		else
		{
			$result = array('x' => 'No Agencies Created');
		}
		return $result;
	}

	public function getAgentName($id)
	{
		$this->db->select('agent_name');
		$this->db->from('agents');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['agent_name'];
	}

}
