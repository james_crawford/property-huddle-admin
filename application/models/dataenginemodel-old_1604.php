<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DataengineModel extends CI_Model {

	var $table = "admin";	
	var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %h:%i:%s";
	var $currentDate = '';
    var $currentDateTime = '';
	 var $data      = array();
	
	
	public function __construct(){
        
		parent::__construct();
		
    }
	
   function ParsDataintoDb($data_file){
	
	 if (!empty($data_file)) {
			$i = 0;
            $pid = '';
			
			//echo '<pre>'; print_r($data_file); 
			
			foreach ($data_file as $records) {
			
			//echo '<pre>'; print_r($records); die;
			
			 /// propert Address
			 $pid = explode("_",$records['AGENT_REF']);
                        
                 $data['propertyID'] = $pid[1];
                            
				 $data['AGENT_REF'] = $records['AGENT_REF'];
				
				 $data['ADDRESS_1'] = $records['ADDRESS_1'];
				
				 $data['ADDRESS_2'] = $records['ADDRESS_2'];
				
				 $data['ADDRESS_3'] = $records['ADDRESS_3'];
				
				 $data['TOWN'] = $records['TOWN'];
				
				 $data['POSTCODE1'] = $records['POSTCODE1'];
				
				 $data['POSTCODE2'] = $records['POSTCODE2'];
				 
				 $data['LAT'] 		= $records['LAT'];
				 
				 $data['LONG'] 		= $records['LONG'];
				
				// $data['displayAddress'] = $records['DISPLAY_ADDRESS'];
			 
			    /// propert fetures
			 	
				 $data['FEATURE1'] = $records['FEATURE1'];
				
				 $data['FEATURE2'] = $records['FEATURE2'];
				
				 $data['FEATURE3'] = $records['FEATURE3'];
				
				 $data['FEATURE4'] = $records['FEATURE4'];
				
				 $data['FEATURE5'] = $records['FEATURE5'];
				
				 $data['FEATURE6'] = $records['FEATURE6'];
				
				 $data['FEATURE7'] = $records['FEATURE7'];
				
				 $data['FEATURE8'] = $records['FEATURE8'];
				
				 $data['FEATURE9'] = $records['FEATURE9'];
				
				 $data['FEATURE10'] = $records['FEATURE10'];
			 
			 // Property Summery
			 	
				 $data['SUMMARY'] = $records['SUMMARY'];
				
			 // Property Description
			 	
				 $data['DESCRIPTION'] = $records['DESCRIPTION'];
				
			// Property Branch Id
				
				 $data['BRANCH_ID'] = $records['BRANCH_ID'];
				
			// for status id	
				 if(trim($records['STATUS_ID']) == 'Available'){
				 	
					$StatusId = 0;
					
				 }elseif(trim($records['STATUS_ID']) == trim('SSTC (Sales only)')){
				 	
					$StatusId = 1;
					
				 }elseif(trim($records['STATUS_ID']) == trim('SSTCM (Scottish Sales only)')){
				 	
					$StatusId = 1;
					
				 }elseif(trim($records['STATUS_ID']) == trim('Under Offer (Sales only)')){
				 	
					$StatusId = 3;
					
				 }elseif(trim($records['STATUS_ID']) == trim('Reserved (Sales only)')){
				 	
					$StatusId = 4;
					
				 }elseif(trim($records['STATUS_ID']) == trim('Let Agreed (Lettings only)')){
				 	
					$StatusId = 5;
					
				 }
				 
				 $data['STATUS_ID'] = $StatusId;
				 
				 
				 
				 $data['BEDROOMS'] = $records['BEDROOMS'];
				 
				 $data['PRICE'] = $records['PRICE'];
				 
				 //It's PRICE QUALIFIER
				 if(trim($records['PRICE_QUALIFIER']) == 'POA'){
				 	
					$PriceQualifier = 1;
					
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Guide Price'){
				 
				 	$PriceQualifier = 2;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Fixed Price'){
				 
				 	$PriceQualifier = 3;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Offers in Excess of'){
				 
				 	$PriceQualifier = 4;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'OIRO'){
				 
				 	$PriceQualifier = 5;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Sale by Tender'){
				 
				 	$PriceQualifier = 6;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'From'){
				 
				 	$PriceQualifier = 7;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Shared Ownership'){
				 
				 	$PriceQualifier = 9;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Offers Over'){
				 
				 	$PriceQualifier = 10;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Part Buy Part Rent'){
				 
				 	$PriceQualifier = 11;
				 
				 }elseif(trim($records['PRICE_QUALIFIER']) == 'Shared Equity'){
				 
				 	$PriceQualifier = 12;
				 
				 }
				 
				 $data['PRICE_QUALIFIER'] = $PriceQualifier;
				 
				 
				// property Type
				
				//$ProperyType = $this->GetPropertyType($records['PROP_SUB_ID']);
				$ProperyType = 1;
				$data['PROP_SUB_ID'] = $ProperyType;
				 
				 //dates are define for media tablr insert
				 $created_date = $records['CREATE_DATE'];
				 
				 $updated_date = $records['UPDATE_DATE'];
				 
				 $data['CREATE_DATE'] = $records['CREATE_DATE'];
				 
				 $data['UPDATE_DATE'] = $records['UPDATE_DATE'];
				 
				 $data['DISPLAY_ADDRESS'] = $records['DISPLAY_ADDRESS'];
				 
				$active = 0;
				
				if(trim($records['PUBLISHED_FLAG']) == 'Visible'){
					
					$active = 1;
				}
								
				 $data['PUBLISHED_FLAG'] = $active;
				 
				 
				 $data['LET_DATE_AVAILABLE'] = $records['LET_DATE_AVAILABLE'];
				 
				 $data['LET_BOND'] = $records['LET_BOND'];
				 
				 
				 
				 
				 $latTypeId = 0;
				 if(trim($records['LET_TYPE_ID']) == 'Long Term'){
				 	
					$latTypeId = 1;
				 
				 }elseif(trim($records['LET_TYPE_ID']) == 'Short Term'){
				 
				 	$latTypeId = 2;
					
				 }elseif(trim($records['LET_TYPE_ID']) == 'Student'){
				 
				 	$latTypeId = 3;
					
				 }elseif(trim($records['LET_TYPE_ID']) == 'Commercial'){
				 
				 	$latTypeId = 4;
					
				 }
				 
				 $data['LET_TYPE_ID'] = $latTypeId;
				 
				 
				 
				 $furnishedID = 0;
				 if(trim( $records['LET_FURN_ID']) == 'Part Furnished' ){
				 
				 	$furnishedID =1;
				 
				  }elseif(trim( $records['LET_FURN_ID']) == 'Unurnished' ){
				  
				  	$furnishedID =2;
				  
				  }elseif(trim( $records['LET_FURN_ID']) == 'Not Specified' ){
				  
				  	$furnishedID =3;
				  
				  }elseif(trim($records['LET_FURN_ID']) == 'Furnished/Un Furnished'){
				  
				  	$furnishedID =4;
				  
				  }
				 
				 $data['LET_FURN_ID'] = $furnishedID;
				 
				 $rentType = 0;
				 if(trim($records['LET_RENT_FREQUENCY']) =='Monthly'){
				 
				 	$rentType = 1;
					
				 }elseif(trim($records['LET_RENT_FREQUENCY']) =='Quarterly'){
				 
				 	$rentType = 2;
				 
				 }elseif(trim($records['LET_RENT_FREQUENCY']) =='Annual'){
				 
				 	$rentType = 3;
				 
				 }elseif(trim($records['LET_RENT_FREQUENCY']) =='Per person per week'){
				 
				 	$rentType = 4;
				 
				 }
				 
				 $data['LET_RENT_FREQUENCY'] = $rentType;
				 
				 
				 
				 $tranceType = 1;
				 if(trim($records['TRANS_TYPE_ID']) =='Lettings'){
				 
				 	$tranceType = 2;
				 
				 }
				 
				 $data['TRANS_TYPE_ID'] = $tranceType;
				 
				 $newHome = 'Y';
				 if(trim($records['NEW_HOME_FLAG'])=='Non New Home'){
				 
				 	$newHome = 'N';
				 
				 }
				 $data['NEW_HOME_FLAG'] = $newHome;
				 
				 
				 // Insert Into database only data recoreds
				 $table = 'properties';
				 
				 $this->db->insert($table, $data);
				 
				 
				if($this->db->insert_id()){
					//echo $this->db->insert_id();
				}else{
					echo "not inserted";
				}
				
				
				
				$i= 0;
				$zero_occurance = '';
				$images = array();
				$images_text =array();
				$media_floor = array();
				$media_doc = array();
				$data_file = $records;
				foreach($data_file as $subkey=>$value){
					$operate = explode('_',$subkey);
					$zero_occurance = $operate[0];
					
						if(preg_match("/MEDIA_IMAGE_[0-9]{2}/",$subkey)){
						
							$images[$subkey] = $value;
						}
						if(preg_match("/MEDIA_IMAGE_TEXT_[0-9]{2}/",$subkey)){
						
							$images_text[$subkey] = $value;
						}
						if(preg_match("/MEDIA_FLOOR_PLAN_[0-9]{2}/",$subkey)){
						
							$media_floor[$subkey] = $value;
						}
						if(preg_match("/MEDIA_DOCUMENT_[0-9]{2}/",$subkey)){
						
							$media_doc[$subkey] = $value;
						}
				
				}
				
				// Lopp for Media Files
				
				$imageName ='';
				$branchId= '';
				$propertyIdtbl='';
				$imageOrder ='';
				//echo '<pre>'; print_r($images);
                                foreach($images as $keyimg => $valueimg){
					
				if($valueimg !=''){
					
					//Get Property Id From Media
					$propertyId =  explode('_',$valueimg);
					
					$dataMedia['propertyID'] = $pid[1]; //$propertyId[1];
					
					$propertyIdtbl = $pid[1]; //$propertyId[1];
					
					// Set Property Id
					$branchId = $pid[0]; //$propertyId[0];
					
					// Set Image Name
					$imageName = $valueimg;
					
					//Get Image Order
					$getOrder = explode('_',$imageName);
					
					$imageOrderex = $getOrder[3]; 
					
					$order = explode('.',$imageOrderex);
					
					$imageOrder = $order[0]; 
					
					
					//$dataMedia['domainURL'] = 'http://media2.jupix.co.uk/'.$branchId.'/'.$propertyIdtbl.'/'.$imageName;
                                        $dataMedia['domainURL'] = 'http://media.propertyhuddle.com/'.$branchId.'/'.$propertyIdtbl.'/'.$imageName;
					
					$dataMedia['imageName'] = $imageName;
					
					
					$keyrxplode = explode("_",$keyimg);
					
					$text_desc_key = 'MEDIA_IMAGE_TEXT_'.$keyrxplode[2];
					
					//echo 'file desc '.$text_desc_key.' => '.$images_text[$text_desc_key]."<br />";
					
					$dataMedia['imageDescription'] = $images_text[$text_desc_key];
					
					$dataMedia['imageOrder'] = $imageOrder;
					
					$dataMedia['dateCreated'] = $created_date;
					
					$dataMedia['dateModified'] = $updated_date;
				
					$table = 'media';
				 
					// Insert Into database only media
					$this->db->insert($table, $dataMedia);
				   }	 
					
				}
				
			} 
			//echo "Recoreds Inserted ";
		
		} 
	}
	
	
		/*public function GetPropertyType($propertyName){
		
			$this->db->select('id');
       		$this->db->where('property_type', $propertyName);
            
			$query = $this->db->get('property_type');
        	
			$result = $query->result_array();
			
			return $result[0];
			
		}	*/
	
// End Extend
}
