<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Staticpagesmodel extends CI_Model
{

	var $table = "static_pages";

	public function __construct()
	{
		parent::__construct();
	}

	public function getRecords()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else
		{
			return array();
		}
	}

	function deleterecord()
	{

	}

}
