<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Branchmodel extends CI_Model
{

	var $table = "branches";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('image_model', 'image');
		$this->load->model('staff_model', 'staff');
	}

	public function getBranchRecords($id = false, $contacts = false)
	{
		$i = 0;
		$this->db->select('branches.*,agents.agent_name');
		$this->db->from($this->table);
		$this->db->join('agents', 'agents.id = branches.agents_id', 'INNER');
		if (is_int($id) || is_string($id))
		{
			$this->db->where('branches.id', $id);
		}
		if (is_array($id))
		{
			$this->db->where_in('branches.id', $id);
		}
		$query = $this->db->get();
		if ($query->num_rows > 0)
		{
			$branchResult = $query->result_array();
			foreach ($branchResult as $branch)
			{
				$branchData['branches'][$i] = $branch;
				if ($contacts)
				{
					$this->db->select('name As manager_name, email AS manager_email, telephone as  manager_telephone, staff_image_id AS manager_staff_image');
					$this->db->from('branch_agent_staff');
					$this->db->where('id', $branchData['branches'][$i]['manager_staff_id']);
					$managerQuery = $this->db->get();
					if ($managerQuery->num_rows() >= 1)
					{
						$managerResult = Common::assocArray($managerQuery->result_array());
						$branchData['branches'][$i]['manager'] = $managerResult[0];
						if ($branchData['branches'][$i]['manager']['manager_staff_image'] != '0')
						{
							$branchData['branches'][$i]['manager']['manager_staff_image'] = $this->image->getImagePath($branchData['branches'][$i]['manager']['manager_staff_image']);
						}
					}
					else
					{
						$branchData['branches'][$i]['manager'] = array(
							'manager_name' => 'Not Set',
							'manager_email' => 'Not Set',
							'manager_telephone' => 'Not Set',
							'manager_staff_image' => '0',
						);
					}


					$branchImageID = $this->getBranchImage($id);
					$branchData['branches'][$i]['branch']['branch_image'] = $this->image->getImagePath($branchImageID);

					$teamImageID = $this->getBranchImage($id, 'team_image_id');
					$branchData['branches'][$i]['branch']['team_image'] = $this->image->getImagePath($teamImageID);

					$logoImageID = $this->getBranchImage($id, 'branch_logo_image_id');
					$branchData['branches'][$i]['branch']['branch_logo'] = $this->image->getImagePath($logoImageID);




					$this->db->select('name As trade_contact_name, email AS trade_contact_email, telephone as  trade_contact_telephone_number, staff_image_id AS trade_contact_staff_image');
					$this->db->from('branch_agent_staff');
					$this->db->where('id', $branchData['branches'][$i]['trade_staff_id']);
					$tradeContactQuery = $this->db->get();
					if ($tradeContactQuery->num_rows() >= 1)
					{
						$tradeContactResult = Common::assocArray($tradeContactQuery->result_array());
						$branchData['branches'][$i]['trade_contact'] = $tradeContactResult[0];
						if ($branchData['branches'][$i]['trade_contact']['trade_contact_staff_image'] != '0')
						{
							$imagePath = $this->image->getImagePath($branchData['branches'][$i]['trade_contact']['trade_contact_staff_image']);
						}
					}
					else
					{
						$branchData['branches'][$i]['trade_contact'] = array(
							'trade_contact_name' => 'Not Set',
							'trade_contact_email' => 'Not Set',
							'trade_contact_telephone_number' => 'Not Set',
							'trade_contact_staff_image' => '0'
						);
					}
					$this->db->select('name As billing_contact_name, email AS billing_contact_email, telephone as  billing_contact_telephone_number, staff_image_id AS billing_staff_image');
					$this->db->from('branch_agent_staff');
					$this->db->where('id', $branchData['branches'][$i]['billing_staff_id']);
					$billingContactQuery = $this->db->get();
					if ($billingContactQuery->num_rows() >= 1)
					{
						$billingResult = Common::assocArray($billingContactQuery->result_array());
						$branchData['branches'][$i]['billing'] = $billingResult[0];
						if ($branchData['branches'][$i]['billing']['billing_staff_image'] != '0')
						{
							$branchData['branches'][$i]['billing']['billing_staff_image'] = $this->image->getImagePath($branchData['branches'][$i]['billing']['billing_staff_image']);
						}
					}
					else
					{
						$branchData['branches'][$i]['billing'] = array(
							'billing_contact_name' => 'Not Set',
							'billing_contact_email' => 'Not Set',
							'billing_contact_telephone_number' => 'Not Set',
							'billing_staff_image' => '0');
					}
				}
				if ($branchData['branches'][$i]['branch_logo_image_id'] != '0')
				{
					$branchData['branches'][$i]['branch_logo'] = $this->image->getImagePath($branchData['branches'][$i]['branch_logo_image_id']);
				}
				if ($branchData['branches'][$i]['branch_image_id'] != '0')
				{
					$branchData['branches'][$i]['branch_image'] = $this->image->getImagePath($branchData['branches'][$i]['branch_image_id']);
				}
				if ($branchData['branches'][$i]['team_image_id'] != '0')
				{
					$branchData['branches'][$i]['team_image'] = $this->image->getImagePath($branchData['branches'][$i]['team_image_id']);
				}
				$i++;
			}
			if ($id)
			{
				return $branchData['branches'][0];
			}
			else
			{
				return $branchData;
			}
		}
		else
		{
			return array();
		}
	}

	public function getBranchImage($id, $file = false)
	{
		if (!$file)
		{
			$file = 'branch_image_id';
		}
		$this->db->select($file);
		$this->db->from('branches');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		if ($query->num_rows() > 0)
		{
			return $result[0][$file];
		}
		else
		{
			return '0';
		}
	}

	public function getBranchProfileData($id)
	{
		//initialise empty values:
		$return['sales'] = '';
		$return['lettings'] = '';
		$return['commercial'] = '';
		$return['landAgency'] = '';
		$return['salesDPID'] = '';
		$return['lettingsDPID'] = '';
		$return['commercialDPID'] = '';
		$return['landAgencyDPID'] = '';
		
		$this->db->select('department_code, branch_ref, dataproviders_id');
		$this->db->from('branch_department');
		$this->db->where('branches_id', $id);
		$query = $this->db->get();
		if ($query->num_rows > 0)
		{
			$i = 0;
			$result = $query->result_array();
			foreach ($result as $branchRef)
			{
				$strBinBranchRef = strrev ((string) $this->decextbin($branchRef['department_code'], 4));
				if (isset($strBinBranchRef[0]) && $strBinBranchRef[0] == '1')
				{
					$return['sales'] = $branchRef['branch_ref'];
					$return['salesDPID'] = $branchRef['dataproviders_id'];
				}
				if (isset($strBinBranchRef[1]) && $strBinBranchRef[1] == '1')
				{
					$return['lettings'] = $branchRef['branch_ref'];
					$return['lettingsDPID'] = $branchRef['dataproviders_id'];
				}
				if (isset($strBinBranchRef[2]) && $strBinBranchRef[2] == '1')
				{
					$return['commercial'] = $branchRef['branch_ref'];
					$return['commercialDPID'] =  $branchRef['dataproviders_id'];
				}
				if (isset($strBinBranchRef[3]) && $strBinBranchRef[3] == '1')
				{
					$return['landAgency'] = $branchRef['branch_ref'];
					$return['landAgencyDPID'] =  $branchRef['dataproviders_id'];
				}
			}
		}
		else
		{
			$return = false;
		}
		return $return;
	}

	public function protfolioBranchIsAvailable($branchRef)
	{
		$this->db->select('department_code');
		$this->db->from('branch_department');
		$this->db->where('branch_ref', $branchRef);
		$query = $this->db->get();
		if ($query->num_rows > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function decextbin($decimalnumber, $bit)
	{
		$binarynumber = '';
		/* decextbin function
		  by James Preece (j.preece@gmail.com)
		  http://www.lovingit.co.uk

		  Please feel free to use this function. If you find
		  if useful I would love to hear from you.
		 */

		/* Function to return a binary number with as many
		  bits as are requested...

		  First we find that maximum value represented by the
		  leftmost binary digit. For error checking purposes
		  we also calulate the maximum number we can display
		  using the number of bits requested: */

		$maxval = 1;
		$sumval = 1;
		for ($i = 1; $i < $bit; $i++)
		{
			$maxval = $maxval * 2;
			$sumval = $sumval + $maxval;
		}

		/* Using our sumval we now check if it is possible
		  to display the decimal number our function received: */

		if ($sumval < $decimalnumber)
			return 'ERROR - Not enough bits to display this figure in binary.';

		/* Then we work down through the figures, to get a
		  better idea of how this works remove the commenting
		  from the echo lines */

		for ($bitvalue = $maxval; $bitvalue >= 1; $bitvalue = $bitvalue / 2)
		{
			//echo 'Bit Value: '.$bitvalue.'<br />';
			//echo 'Decimal Number: '.$decimalnumber.'<br />';

			if (($decimalnumber / $bitvalue) >= 1)
				$thisbit = 1; else
				$thisbit = 0;

			//echo 'This Bit: '.$thisbit.'<br /><br />';

			if ($thisbit == 1)
				$decimalnumber = $decimalnumber - $bitvalue;

			$binarynumber .= $thisbit;
		}

		/* Finally we return the output... */

		return $binarynumber;
	}

	public function getBranchName($id)
	{
		$this->db->select('branch_name');
		$this->db->from('branches');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['branch_name'];
	}

	public function getAllBranchIDs()
	{
		$result[0] = 'Please select a Branch';
		$this->db->select('id, branch_name');
		$this->db->from('branches');
		$this->db->where('active', '1');
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$array = $query->result_array();
			foreach ($array as $branch)
			{
				$result[$branch['id']] = $branch['branch_name'];
			}
		}
		else
		{
			$result = array('x' => 'No Branches Created');
		}
		return $result;
	}
	
	public function getBranchNumbers($id)
	{
		
		$this->db->select('branch_sales_telephone_number,branch_lettings_telephone_number,branch_commercial_telephone_number,branch_new_homes_telephone_number');
		$this->db->from('branches');
		$this->db->where('id', $id);
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$array = $query->result_array();
			if($array[0]['branch_sales_telephone_number'] !='')
			{
			$result['branch_sales_telephone_number'] = "(Sales) ".$array[0]['branch_sales_telephone_number'];
			}
			if($array[0]['branch_lettings_telephone_number'] !='')
			{
				$result['branch_lettings_telephone_number'] = "(Lettings) ".$array[0]['branch_lettings_telephone_number'];
			}
			if($array[0]['branch_commercial_telephone_number'] !='')
			{
				$result['branch_commercial_telephone_number'] = "(Commercial) ".$array[0]['branch_commercial_telephone_number'];
			}
			if($array[0]['branch_new_homes_telephone_number'] !='')
			{
				$result['branch_new_homes_telephone_number'] = "(New Homes) ".$array[0]['branch_new_homes_telephone_number'];
			}
		}
		if(empty($result))
		{
			$result[0] = 'Please assign phone numbers in branches';
		}else
		{
			$result[0] = 'Please select a telephone number';
		}
		
		return $result;
	}
	
	public function getBranchData($id)
	{
			$i = 0;
		$this->db->select('branches.*,agents.agent_name');
		$this->db->from($this->table);
		$this->db->join('agents', 'agents.id = branches.agents_id', 'INNER');
		if (is_int($id) || is_string($id))
		{
			$this->db->where('branches.id', $id);
		}
		if (is_array($id))
		{
			$this->db->where_in('branches.id', $id);
		}
		$query = $this->db->get();
		if ($query->num_rows > 0)
		{
			$result =  $query->result_array();
			return $result[0];
		}
		else
		{
			return array();
		}
	}
	
}
