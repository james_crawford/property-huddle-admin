<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Image_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Common');
	}

	public function getImagePath($id)
	{
		$this->db->select('domain_url');
		$this->db->from('branch_images');
		$this->db->where('id', $id);

		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$result = $query->result_array();
			return $result[0]['domain_url'];
		}
		else
		{
			return '0';
		}
	}

	public function saveBranchImage($name, $branchID, $description, $orderBy = '1')
	{
		$data = array('domain_url' => 'http://admin.propertyhuddle.com/assets/branch/'.$name,
			'branches_id' => $branchID,
			'image_name' => $name,
			'image_description' => $description,
			'image_order' => $orderBy);
		$this->db->insert('branch_images', $data);
		return $this->db->insert_id();
	}

	public function getBranchImage($id, $type = 'branch_image_id')
	{
		$this->db->select($type);
		$this->db->from('branches');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0][$type];
	}
	
	public function getStaffImage($id)
	{
		$this->db->select('staff_image_id');
		$this->db->from('branch_agent_staff');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['staff_image_id'];
	}

}

?>
