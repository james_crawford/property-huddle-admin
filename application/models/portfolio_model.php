<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class BranchDepartment_model extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UniversalModel', '', TRUE);
		$this->load->library('session');
		$this->load->library('Common');
	}

	/**
	 * This Class uses a binary value system to assign departments to a data provider,
	 * this way we can have up to four departments against a single branch in in a single row
	 * 	RS	RL	C	LA	Val
	 * 	1	0	0	0	1	= Sales only
	 * 	0	1	0	0	2	= Lettings Only
	 * 	1	1	0	0	3	= Sales & Lettings
	 * 	0	0	1	0	4	= Commercial only
	 * 	1	0	1	0	5	= Sales & Commercial
	 * 	0	1	1	0	6	= Lettings & Commercial
	 * 	1	1	1	0	7	= Sales, Lettings & Commercial
	 * 	0	0	0	1	8
	 * 	1	0	0	1	9
	 * 	0	1	0	1	10
	 * 	1	1	0	1	11
	 * 	0	0	1	1	12
	 * 	1	0	1	1	13
	 * 	0	1	1	1	14
	 * 	1	1	1	1	15	= All departments
	 *
	 *
	 *
	 */
}

?>
