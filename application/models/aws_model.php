<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/*
 * Class to perform Batch processing of multiple record updates
 * e.g Feed data
 */

Class Aws_model extends CI_Model
{

	public function validateEmailMultiple($email, $validateSyntax=true, $validateDomain=true) 
{
	$valid = true;
	$toAddress = '';
	$toAddressCC = array();
	
	$email = trim($email);
	$email = trim($email, ",;");
	$explodeEmails = preg_split("/[,;]+/", $email);
	if (count($explodeEmails)>0)
	{
		$i = 0;
		foreach ($explodeEmails as $emailAddress) 
		{
			$emailAddress = trim($emailAddress);
			$continue = false;
			
			if (!$validateSyntax && !$validateDomain)
			{
				$continue = true;
			}
			if (!$validateSyntax && $validateDomain && validateEmail2($emailAddress))
			{
				$continue = true;
			}
			if ($validateSyntax && !$validateDomain && validateEmail($emailAddress))
			{
				$continue = true;
			}
			if ($validateSyntax && $validateDomain && validateEmail($emailAddress) && validateEmail2($emailAddress))
			{
				$continue = true;
			}
			
			if ($continue)
			{
				if ($i==0) { $toAddress = $emailAddress; }else{ array_push($toAddressCC, $emailAddress); }
			}
			else
			{
				$valid = false;
			}
			$i++;
		}
	}
	else
	{
		$valid = false;
	}
	
	return array($valid, $toAddress, $toAddressCC);
	
}
	
	/**
	 * Keeps a running check if a 'loose' file
	 * (not in a zip) can be attached to an initialised batch
	 *
	 * @var mixed Boolean false when intialised Int if set
	 */
	public $looseBatchHasID = false;

	/**
	 * Intialise the new object and create access to other classes
	 */
	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}

	/*
	  $attachments = array(
	  array(
	  "path" => "", // path to attachment
	  "name" => "" // name of file
	  )
	  );
	 */

	function sendEmailSES($toAddress, $toName, $fromAddress, $fromName, $subject, $emailBody, $attachments = array())
	{
		list($email, $toAddress, $toAddressCC) = $this->validateEmailMultiple($toAddress, false, false);

		if (!defined('DISPLAY_XPM4_ERRORS'))
		{
			define('DISPLAY_XPM4_ERRORS', true);
		} // display XPM4 errors
		if (!isset($_SESSION['cron']))
		{
			require_once('includes/xPertMailer/MAIL.php'); // path to 'MAIL.php' file from XPM4 package
		}
		$m = new MAIL; // initialize MAIL class

		$sendEmail = 1;

		if ($m->AddTo(trim($toAddress), trim($toName), 'utf-8', 'base64'))
		{

		}
		else
		{
			$sendEmail = 0;
			echo '<br><br>Error setting To address: '.$toAddress.' ('.$toName.')<br><br>';
		}
		if (is_array($toAddressCC) && count($toAddressCC) > 0)
		{
			foreach ($toAddressCC as $cc)
			{
				// Add CC address and name
				if ($m->AddCc(trim($cc), trim($toName), 'utf-8', 'base64'))
				{

				}
				else
				{
					$sendEmail = 0;
					echo '<br><br>Error setting CC address: '.$cc.' ('.$toName.')<br><br>';
				}
			}
		}
		if ($m->From(trim($fromAddress), trim($fromName)))
		{
			$m->Path(trim($fromAddress));
		}
		else
		{
			$sendEmail = 0;
			echo '<br><br>Error setting From: '.$fromAddress.' ('.$fromName.')<br><br>';
		}
		// set subject
		if ($m->Subject($subject))
		{

		}
		else
		{
			$sendEmail = 0;
			echo '<br><br>Error setting subject: '.$subject.'<br><br>';
		}
		if ($m->Text('You must have a HTML enabled email client to view this email'))
		{

		}
		else
		{
			$sendEmail = 0;
			echo '<br><br>Error setting Text<br><br>';
		}
		if ($emailBody != '' && $m->Html($emailBody))
		{

		}
		else
		{
			$sendEmail = 0;
			echo '<br><br>Error setting HTML: '.$emailBody.'<br><br>';
		}

		if (count($attachments) > 0)
		{
			$i = 1;
			foreach ($attachments as $attachment)
			{
				$attachmentPath = '';
				$attachmentName = '';
				if (isset($attachment['path']))
				{
					$attachmentPath = $attachment['path'];
				}
				if (isset($attachment['name']))
				{
					$attachmentName = $attachment['name'];
				}

				if ($attachmentPath != "")
				{
					$f = $attachmentPath;
					$fileContents = file_get_contents($f);
					if ($fileContents != "")
					{
						// get filename
						if (trim($attachmentName) == "")
						{
							$explodePath = explode(".", $f);
							$extension = $explodePath[count($explodePath) - 1];
							$attachmentName = "attached_".$i.".".$extension;
							$i++;
						}
						//
						if ($m->Attach($fileContents, FUNC::mime_type($f), $attachmentName, 'utf-8', 'base64', 'attachment'))
						{

						}
						else
						{
							$sendEmail = 0;
							echo 'Error while attaching file '.$attachmentPath;
						}
					}
					else
					{
						$sendEmail = 0;
						echo 'Attached file '.$attachmentPath.' is empty';
					}
				}
			}
		}

		if ($sendEmail == 1)
		{

			//echo 'connecting';
			// connect to MTA server 'smtp.hostname.net' port '25' with authentication: 'username'/'password'
			$c = $m->Connect('email-smtp.us-east-1.amazonaws.com', 465, 'AKIAJQ5JP3ENOL55UOXQ', 'AhdfE+3XWJxwigaB08UVCsEq8FTTWwgBBmLWEp+EcoFm', 'tls') or die(print_r($m->Result));

			//echo 'sending';
			// send mail relay using the '$c' resource connection
			if ($m->Send($c))
			{

			}
			else
			{
				$errors = $m->Result[330];
				foreach ($errors as $error)
				{
					echo 'E:'.$error.'<br />';
				}
				$sendEmail = 0;
				die();
			}
		}

		$m->Disconnect(); // disconnect from server
		//print_r($m->History); // optional, for debugging

		return (($sendEmail == 0) ? false : true);
	}

	function checkSESSenderVerified($fromAddress, $sendIfNotVerified = true)
	{
		require_once 'AWSSDKforPHP/sdk.class.php';
		$email = new AmazonSES(array('credentials' => 'jupix-ses'));

		$response = $email->list_verified_email_addresses();

		if ($response->isOK())
		{
			$validEmails = $response->body->ListVerifiedEmailAddressesResult->VerifiedEmailAddresses;

			$validEmails = (array) $validEmails;
			$validEmails = $validEmails['member'];

			if (in_array($fromAddress, $validEmails))
			{
				//echo 'send';
				return true;
			}
			else
			{
				//echo 'verify';
				if ($sendIfNotVerified)
				{
					die('<div style="padding:10px;">Service is temporarily not available for your account</div>');
					$response = $email->verify_email_address($fromAddress);
					if ($response->isOK())
					{
						//echo 'Verification email sent';
						return false;
					}
					else
					{
						logged_die('Verification email sending failed');
						return false;
					}
				}
			}
		}
		else
			return false;
	}

	function sendSESVerficationEmail($fromAddress)
	{
		require_once 'AWSSDKforPHP/sdk.class.php';
		$email = new AmazonSES(array('credentials' => 'jupix-ses'));

		$response = $email->verify_email_address($fromAddress);
		if ($response->isOK())
		{
			//echo 'Verification email sent';
			return true;
		}
		else
		{
			logged_die('Verification email sending failed');
			return false;
		}
	}

// $file - Location of the file. Can be local or remote (eg. http...)
	function putFileAWS($sourceFile, $destinationFileName, $bucket = 'jupix-emailed-documents', $acl = 'private', $filetype = 'detect')
	{
		require_once 'AWSSDKforPHP/sdk.class.php';

		// if we aren't explicitely setting the folder put it in clients/CLIENTID/
		if (strpos($destinationFileName, "/") === FALSE)
		{
			$filename = 'clients/'.$_SESSION['clientID'].'/'.$destinationFileName;
		}
		else
		{
			$filename = $destinationFileName;
		}

		// Instantiate the class
		$s3 = AmazonS3::factory(array(
					'credentials' => $bucket
				));
		//Set the region to EU
		$s3->set_region(AmazonS3::REGION_EU_W1);

		if ($filetype == 'detect')
		{
			$finfo = finfo_open();
			$filemime = finfo_file($finfo, $sourceFile, FILEINFO_MIME);
			finfo_close($finfo);
		}
		else
		{
			$filemime = $filetype;
		}

		//Set ACL and body of file
		if ($acl == 'private')
		{
			$objectOptions = array(
				'contentType' => $filemime,
				'acl' => AmazonS3::ACL_PRIVATE,
				'body' => file_get_contents($sourceFile)
			);
		}
		else
		{
			$objectOptions = array(
				'contentType' => $filemime,
				'acl' => AmazonS3::ACL_PUBLIC,
				'body' => file_get_contents($sourceFile)
			);
		}
		/* if (strpos($sourceFile, "http")!==FALSE)
		  {
		  $objectOptions['body'] = file_get_contents($sourceFile);
		  }
		  else
		  {
		  $file_resource = fopen($sourceFile, 'r');
		  $objectOptions['fileUpload'] = $file_resource;
		  } */

		//echo "Putting ".$sourceFile." into bucket ".$bucket." as ".$destinationFileName."\n";

		$response = $s3->create_object($bucket, $filename, $objectOptions);
		// Success?
		if ((int) $response->isOK())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function getFileAWS($filename, $publicFilename = "", $bucket = 'assets.propertyhuddle.com', $outputFile = true, $inline = false)
	{
		require_once 'AWSSDKforPHP/sdk.class.php';

		if ($publicFilename == "")
		{
			$publicFilename = $filename;
		}

		// Instantiate the class
		$s3 = new AmazonS3(array(
					'credentials' => $bucket));

		$response = $s3->get_object($bucket, $filename);

		$contentType = $response->header['_info']['content_type'];

		if ($outputFile)
		{
			header("Content-type: $contentType");
			header("Pragma: ");
			header("Cache-Control: ");
			header('Content-Disposition: '.($inline ? 'inline' : 'attachment').'; filename='.$publicFilename);
			echo $response->body;
		}
		else
		{

			return array('contents' => $response->body, 'content_type' => $contentType);
		}
	}

	/**
	 * Check if a file exists in a cloud bucket
	 *
	 * @param string $file (Required) the filename of the file to check
	 * @param string $bucket (Required) the name of the bucket to check
	 * @return boolean true if file was found, or false if it wasn't
	 */
	function existsFileAWS($file, $bucket)
	{
		require_once 'AWSSDKforPHP/sdk.class.php';

		// Instantiate the class
		$s3 = new AmazonS3(array('credentials' => $bucket));

		if ($s3->if_object_exists($bucket, $file))
		{
			//echo 'File '.$file.' exists in bucket '.$bucket;
			return true;
		}
		else
		{
			//echo 'File '.$file.' does not exist in bucket '.$bucket;
			return false;
		}
	}

	function deleteFileAWS($filename, $bucket = 'jupix-emailed-documents')
	{
		require_once 'AWSSDKforPHP/sdk.class.php';

		// Instantiate the class
		$s3 = new AmazonS3(array('credentials' => $bucket));

		$response = $s3->delete_object($bucket, $filename);

		// returns bool true or false
		return $response->isOK();
	}

	/**
	 * Check if a file exists in a cloud bucket and delete it if so
	 *
	 * @param string $files (Required) an array of files to check and delete
	 * @param string $bucket (Required) the name of the bucket to check
	 * @return null
	 */
	function deleteFilesIfExistsAWS($files, $bucket)
	{
		require_once 'AWSSDKforPHP/sdk.class.php';

		// Instantiate the class
		$s3 = new AmazonS3(array('credentials' => $bucket));

		foreach ($files as $file)
		{
			// Check if the file exists in the cloud
			if (existsFileAWS($file, $bucket))
			{
				// Yes the file exists in the cloud so delete it
				deleteFileAWS($file, $bucket);
			}
		}
	}

}

?>
