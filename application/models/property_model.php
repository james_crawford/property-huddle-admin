<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Property_model extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UniversalModel', '', TRUE);
		$this->load->library('session');
		$this->load->library('Common');
	}

	public function loadAllProperties()
	{
		$sales = $this->loadSalesData();
		$commercial = $this->loadCommercialData();
		$lettings  = $this->loadLettingsData();
		$salLet = array_merge($sales, $lettings);
		$salLetCom = array_merge($salLet, $commercial);
		return $salLetCom;

	}
	
	
	
	public function loadSalesData()
	{
		$this->db->select('display_address,property_residential_sales.property_ref_key_id,
		address_1, address_2, property_residential_sales.town,
		summary,
		branches_id,
		price,
		branch_name,
		agent_name,
		property_ref_key.active,
		branches.agents_id,
		property_ref_key.branches_id
		');
		$this->db->join('property_ref_key', 'property_residential_sales.property_ref_key_id = property_ref_key.id');
		$this->db->join('branches', 'property_ref_key.branches_id = branches.id');
		$this->db->join('agents', 'branches.agents_id = agents.id');
		$this->db->from('property_residential_sales');
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $property)
		{
			$return[$property['property_ref_key_id']] = $property;
			$return[$property['property_ref_key_id']]['department'] = 'Sales';
		}
		return $return;
	}
	public function loadLettingsData()
	{
		$this->db->select('display_address,property_residential_lettings.property_ref_key_id,
		address_1, address_2, property_residential_lettings.town,
		summary,
		branches_id,
		price,
		branch_name,
		agent_name,
		property_ref_key.active,
		branches.agents_id,
		property_ref_key.branches_id
		');
		$this->db->join('property_ref_key', 'property_residential_lettings.property_ref_key_id = property_ref_key.id');
		$this->db->join('branches', 'property_ref_key.branches_id = branches.id');
		$this->db->join('agents', 'branches.agents_id = agents.id');
		$this->db->from('property_residential_lettings');
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $property)
		{
			
			$return[$property['property_ref_key_id']] = $property;
			$return[$property['property_ref_key_id']]['department'] = 'Lettings';

		}
		return $return;
	}
	public function loadCommercialData()
	{
		$this->db->select('display_address,property_commercial.property_ref_key_id,
		address_1, address_2, property_commercial.town,
		summary,
		branches_id,
		price,
		branch_name,
		agent_name,
		property_ref_key.active,
		branches.agents_id,
		property_ref_key.branches_id
		');
		$this->db->join('property_ref_key', 'property_commercial.property_ref_key_id = property_ref_key.id');
		$this->db->join('branches', 'property_ref_key.branches_id = branches.id');
		$this->db->join('agents', 'branches.agents_id = agents.id');
		$this->db->from('property_commercial');
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $property)
		{
			$return[$property['property_ref_key_id']] = $property;
			$return[$property['property_ref_key_id']]['department'] = 'Commercial';
		}
		return $return;
	}
	
	public function loadPropertyData($department,$propertyRefKey)
	{
		switch ($department)
		{
			case'sales':
			{
				$table = 'property_residential_sales';
				break;
			}
			case'lettings':
			{
				$table = 'property_residential_lettings';
				break;
			}
			case'commercial':
			{
				$table = 'property_commercial';
				break;
			}
		}
		
		$this->db->select($table.'.*,
		property_ref_key.active,
		branches.agents_id,
		branches.branch_name,
		agents.agent_name,
		domain_url,
		image_name,
		property_ref_key.branches_id
		');
		$this->db->join('property_ref_key', $table.'.property_ref_key_id = property_ref_key.id');
		$this->db->join('branches', 'property_ref_key.branches_id = branches.id');
		$this->db->join('agents', 'branches.agents_id = agents.id');
		$this->db->join('branch_images', 'branches.branch_logo_image_id = branch_images.id');
		$this->db->where('property_ref_key.id', $propertyRefKey);
		$this->db->from($table);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0];
	}
	
	public function loadArchiveData($propertyRefKey)
	{
		$this->db->select('*, property_archive.active AS propertyActive');
		$this->db->join('property_ref_key', 'property_ref_key_id = property_ref_key.id');
		$this->db->join('branches', 'property_ref_key.branches_id = branches.id');
		$this->db->join('agents', 'branches.agents_id = agents.id');
		$this->db->join('branch_images', 'branches.branch_logo_image_id = branch_images.id');
		$this->db->where('property_ref_key.id', $propertyRefKey);
		$this->db->order_by("date_archived", "desc"); 
		$this->db->from('property_archive');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	
	
	
}

?>
