<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Statistics_model extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DataproviderModel', '', TRUE);
		$this->load->model('UniversalModel', '', TRUE);
		$this->load->library('session');
		$this->load->library('Common');
	}

	/**
	 * Get the current number of properties stored in the DB
	 * @param type $id
	 * @return type
	 */
	public function getDashboardPropertyStatistics()
	{
		$this->db->select("COUNT(*) as count");
		$this->db->from('property_ref_key');
		$this->db->where('active', '1');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['count'];
	}

}

?>
