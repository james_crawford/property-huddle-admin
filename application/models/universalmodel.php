<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class UniversalModel extends CI_Model
{

	function MyModel()
	{

		parent::__construct();
	}

	public function save($table, $columns = array(), $where = NULL, $id = NULL)
	{
		if ($id)
		{
			$this->db->where($where, $id);
			$result = $this->db->update($table, $columns);
		}
		else
		{

			$result = $this->db->insert($table, $columns);
		}

		//print_r($this->db->last_query());		 die;

		if ($result)
		{
			if (!$id)
				return $this->db->insert_id();
			else
				return TRUE;
		} else
			return FALSE;
	}

	public function getRecords($table, $where = NULL, $order_by = NULL, $num = NULL, $offset = NULL )
	{
		$this->db->select('*');
		if ($where != NULL)
		{
			$this->db->where($where);
		}

		if ($num != NULL || $offset != NULL)
		{
			$this->db->limit($num, $offset);
		}

		if ($order_by)
		{
			$this->db->order_by($order_by[0], $order_by[1]);
		}
		else
		{
			//$this->db->order_by('Id', 'DESC');
		}

		//$this->db->order_by('title desc, name asc');

		$query = $this->db->get($table);

		//print_r($this->db->last_query()); die;

		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else
		{

			return array();
			;
		}
	}

	// Delete Records
	public function deleteRecord($table, $where = array())
	{
		$this->db->where($where);

		$res = $this->db->delete($table);

		if ($res)
			return TRUE;
		else
			return FALSE;
	}

	public function checkID($table, $where = array())
	{
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get($table);
		if ($query->num_rows > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

### for Genarate Rendom Provider ID

	public function getRandomID($chars_min = 6, $chars_max = 8, $use_upper_case = false, $include_numbers = true, $include_special_chars = false)
	{
		$length = rand($chars_min, $chars_max);
		$selection = '';
		if ($include_numbers)
		{
			$selection .= "1234567890";
		}
		if ($include_special_chars)
		{
			$selection .= "!@04f7c318ad0360bd7b04c980f950833f11c0b1d1quot;#$%&[]{}?|";
		}
		$ProviderId = "";
		for ($i = 0; $i < $length; $i++)
		{
			$current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
			$ProviderId .= $current_letter;
		}
		return $ProviderId;
	}

### for Genarate Rendom Store passowrs

	public function getRandomPassword($chars_min = 12, $chars_max = 18, $use_upper_case = true, $include_numbers = true, $include_special_chars = true)
	{
		$length = rand($chars_min, $chars_max);
		$selection = 'aeuoyibcdfghjklmnpqrstvwxz';
		if ($include_numbers)
		{
			$selection .= "1234567890";
		}
		if ($include_special_chars)
		{
			$selection .= "!@04f7c318ad0360bd7b04c980f950833f11c0b1d1quot;#$%&[]{}?|";
		}
		$password = "";
		for ($i = 0; $i < $length; $i++)
		{
			$current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
			$password .= $current_letter;
		}
		return $password;
	}

}