<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class AdminModel extends CI_Model
{

	var $table = "admin";
	var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %h:%i:%s";
	var $currentDate = '';
	var $currentDateTime = '';

	public function __construct()
	{
		parent::__construct();

		$this->currentDate = mdate($this->datestring, time());
		$this->currentDateTime = mdate($this->dateStringWithTime, time());
	}

	public function validateUser($email, $pass)
	{
		$conditions = array('Email' => $email, 'Password' => $pass);
		$this->db->select('*');
		$this->db->where($conditions);
		$query = $this->db->get($this->table);
		$count = $query->num_rows();
		if ($count)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function validateUserEmail($email)
	{
		$conditions = array('Email' => $email);
		$this->db->select('*');
		$this->db->where($conditions);
		$query = $this->db->get($this->table);
		$count = $query->num_rows();
		if ($count)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getAdminDetails($email = '')
	{
		$this->db->select('*');

		if ($email != '')
		{
			$this->db->where('Email', $email);
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else
		{

			return false;
		}
	}

	public function getAdminDetailsById($id = '')
	{
		$this->db->select('*');

		if ($id > 0)
		{
			$this->db->where('id', $id);
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else
		{

			return false;
		}
	}

	public function checkRecord($table, $where = array())
	{
		$this->db->select('*');

		$this->db->where($where);

		$query = $this->db->get($table);

		if ($query->num_rows > 0)
		{
			return TRUE;
		}
		else
		{

			return false;
		}
	}

	# Check Recored on Update

	public function checkRecord_update($table, $where = array(), $DpId)
	{
		$this->db->select('*');

		$this->db->where($where);
		$this->db->where('id !=', $DpId);

		$query = $this->db->get($table);

		if ($query->num_rows > 0)
		{
			return TRUE;
		}
		else
		{

			return false;
		}
	}

	public function validatePassword($oldpassword, $id)
	{
		$result = $this->getAdminDetailsById($id);

		if ($result[0]['Password'] == md5($oldpassword))
		{
			return true;
		}
		else
		{

			return false;
		}
	}

	public function changepassword($id, $password)
	{

		$data = array('Password' => md5($password), 'UpdatedDate' => $this->currentDateTime);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
	}

	public function editprofile($id)
	{
		$data = array('Email' => $this->input->post('Email'),
			'Name' => $this->input->post('Name'),
			'UpdatedDate' => $this->currentDateTime);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
	}

	public function generateRandomPassword($length)
	{
		$this->load->helper('string');
		return random_string('alnum', $length);
	}

}
