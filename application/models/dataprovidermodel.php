<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class DataproviderModel extends CI_Model
{

	var $table = "admin";
	var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %h:%i:%s";
	var $currentDate = '';
	var $currentDateTime = '';

	public function __construct()
	{
		parent::__construct();

		$this->currentDate = mdate($this->datestring, time());
		$this->currentDateTime = mdate($this->dateStringWithTime, time());
	}

### Check Provider Id Already Exists Or Not

	public function checkProviderId($id)
	{

		$this->db->select('id');
		$this->db->where('id', $id);
		$query = $this->db->get('dataproviders');

		if ($query->num_rows > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function checkProviderinId($DataProviderId)
	{
		$this->db->select('id');
		$this->db->where('id', $DataProviderId);
		$query = $this->db->get('dataproviders');

		if ($query->num_rows > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

## Delete Data Provider

	function DeletDataprovider($Id)
	{
		if ($Id > 0)
		{
			//echo $Id."Delet will be....."; die;
			$this->db->where('id', $Id);
			$this->db->delete('dataproviders');
		}
	}

	public function getDataProviderDropDown($incPrefix = true, $replaceSpace = false)
	{
		if ($incPrefix)
		{
			$result = array('0' => 'Please select a provider');
		}
		$this->db->select('id, name');
		$this->db->from('dataproviders');
		$this->db->where('active', '1');
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$array = $query->result_array();
			foreach ($array as $dataprovider)
			{
				if($replaceSpace)
				{
					$name =  str_replace(' ', '_', $dataprovider['name']);
				}
				else
				{
					$name = $dataprovider['name'];
				}
				$result[$dataprovider['id']] = $name;
			}
		}
		else
		{
			$result['x'] = 'No Dataproviders Created';
		}
		return $result;
	}

	public function validateUserEmail($email)
	{
		$conditions = array('Email' => $email);
		$this->db->select('*');
		$this->db->where($conditions);
		$query = $this->db->get($this->table);
		$count = $query->num_rows();
		if ($count)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getAdminDetails($email = '')
	{
		$this->db->select('*');

		if ($email > 0)
		{
			$this->db->where('Email', $email);
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else
		{

			return false;
		}
	}

	public function getAdminDetailsById($id = '')
	{
		$this->db->select('*');

		if ($id > 0)
		{
			$this->db->where('id', $id);
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else
		{

			return false;
		}
	}

	public function validatePassword($oldpassword, $id)
	{
		$result = $this->getAdminDetailsById($id);

		if ($result[0]['Password'] == md5($oldpassword))
		{
			return true;
		}
		else
		{

			return false;
		}
	}

	public function changepassword($id, $password)
	{

		$data = array('Password' => md5($password), 'UpdatedDate' => $this->currentDateTime);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
	}

	public function editprofile($id)
	{
		$data = array('Email' => $this->input->post('Email'),
			'Name' => $this->input->post('Name'),
			'UpdatedDate' => $this->currentDateTime);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
	}

	public function generateRandomPassword($length)
	{
		$this->load->helper('string');
		return random_string('alnum', $length);
	}

}