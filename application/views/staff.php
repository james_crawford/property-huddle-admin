<!-- Main window -->
<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">


		<div class="widget widget-padding span6" style="width:100%;">
			<?php
			$message = $this->session->flashdata('Success');
			if (isset($message) AND $message != '')
			{
				?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $message; ?>
				</div><?php } ?>
			<?php
			$staffMSG = $this->session->flashdata('staffMSG');
			if (isset($staffMSG) AND $staffMSG != '')
			{
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $staffMSG; ?>
				</div><?php } ?>


			<div class="widget-header">
				<i class="icon-sitemap"></i>
				<h5>staffes</h5>
				<div class="widget-buttons">
					<a href="<?php echo site_url('staff/add'); ?>" data-title="Add User" data-toggle="" data-target=""><i class="icon-plus"></i></a>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>staff Name</th>
							<th>staff Email Address</th>
							<th>Agent Name</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($dataRecords as $record)
						{
							foreach ($record as $key => $value)
							{
								?>
								<tr>
									<td><?php echo $value['staff_name']; ?></td>
									<td><?php echo $value['staff_email_address']; ?></td>
									<td><?php echo $value['agent_name']; ?></td>
									<td>
									<?php
									if ($value['active'] == '1')
									{
										?>
										<a href="#"  onclick="setstaffVal(<?php echo $value['id'] ?>)"  data-toggle="modal" data-target="#modalDeactivatestaffBox" class="demo_notify_dialog" data-type="confirm"><span class="label label-success">Active</span></a>
										<?php
									}
									else
									{
										?>
										<a href="#"  onclick="setstaffVal(<?php echo $value['id'] ?>)"  data-toggle="modal" data-target="#modalActivatestaffBox" class="demo_notify_dialog" data-type="confirm"><span class="label">Inactive</span></a>
										<?php
									}
									?>
								</td>
									<td>
										<div class="btn-group">
											<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
												Action
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu pull-right">
											  <!--<li><a href="#"><i class="icon-envelope"></i> Email</a></li>-->
												<li><a href="<?php echo site_url('staff/edit/'.$value['agents_id'].'/'.$value['id']); ?>"><i class="icon-edit"></i> Edit</a></li>

												<li><a href="#"  onclick="DelRecord('<?php echo site_url('staff/delete/'.$value['id']); ?>');" class="demo_notify_dialog" data-type="confirm"><i class="icon-trash"></i> Delete Record</a></li>

												<?php /* ?><li><a href="#" onclick="dletesure(<?php echo $value['id'];?>);"><i class="icon-trash"></i> Delete</a></li><?php */ ?>
											</ul>
										</div>
									</td>
								</tr>
								<?php
							}
						}
						?>
					</tbody>
				</table>
            </div> <!-- /widget-body -->
		</div> <!-- /widget -->
	</div>
</div>
<!--- For Show Sucess Message at footer-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
$staffMSG = $this->session->flashdata('staffMSG');
$staffMSGType = $this->session->flashdata('staffMSGType');
if (isset($staffMSG) AND $staffMSG != '')
{
	?>
	<script>
		$(document).ready(function() {
			ShowMessage('<?php echo $staffMSG; ?>','<?php echo $staffMSGType; ?>');
		});
		
	</script>
<?php } ?>
	<script>
	
	function setstaffVal(staffID) {
		$("#deactivatedstaffID").val(staffID);
		$("#activatedstaffID").val(staffID);
	}
		function activatestaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activatestaffStatus') ; ?>/'+ $("#activatedstaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('staff');?>');
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		

	}
	function deactivatestaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivatestaffStatus') ; ?>/'+ $("#deactivatedstaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('staff');?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
			;
}
	
			
</script>