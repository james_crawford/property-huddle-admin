<!-- Add New Provider Main window -->
<div class="main_container" id="users_page" style="padding-top:20px;" >
	<div class="row-fluid">
		<?php echo $this->session->flashdata('ProviderIdError'); ?>
		<form action="<?php echo site_url("dataprovider/add") ?>" id="add_dataprovider" class="form-horizontal" method="post">
		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-list-alt"></i><h5>Add Data Provider</h5>
			                <div style="float:right">
					<a href="<?php echo site_url("dataprovider"); ?>"  style="margin-top:10px; margin-right: 5px;" class="btn" id="cancel">Back</a>
                </div>
			
			</div>

			<div id="register_form1" class="form-horizontal">
				<div class="widget-body">
					<div class="widget-forms clearfix ">
						<div class="control-group" style="height: 5px;">
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>
						<div class="control-group ">
							<label class="control-label">Data Provider Name:</label>
							<div class="controls">
								<input type="text" name="name" id="form_name" value="<?php echo set_value("name"); ?>">
								<?php echo form_error('name', '<p class="error1">'); ?> </div>
						</div>
						<div class="control-group ">
							<label class="control-label">Data Provider Email:</label>
							<div class="controls">
								<input type="text" name="email" id="form_email" value="<?php echo set_value("email"); ?>">
								<?php echo form_error('email', '<p class="error1">'); ?></div>
						</div>
						<div class="control-group ">
							<label class="control-label">Website URL:</label>
							<div class="controls">
								<input type="text" name="website_url" id="form_website_url" value="<?php echo set_value("website_url"); ?>">
								<?php echo form_error('website_url', '<p class="error1">'); ?></div>
						</div>
						<div class="control-group ">
							<label class="control-label">Contact Name:</label>
							<div class="controls">
								<input type="text" name="contact_name" id="form_contact_name" value="<?php echo set_value("contact_name"); ?>">
								<?php echo form_error('contact_name', '<p class="error1">'); ?></div>
						</div>
						<div class="control-group ">
							<label class="control-label">Contact Email:</label>
							<div class="controls">
								<input type="text" name="contact_email" id="form_contact_email" value="<?php echo set_value("contact_email"); ?>">
								<?php echo form_error('contact_email', '<p class="error1">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label">Phone Number:</label>
							<div class="controls">
								<input type="text" name="contact_phone" id="form_contact_phone" value="<?php echo set_value("contact_phone"); ?>">
								<?php echo form_error('contact_phone', '<p class="error1">'); ?></div>
							<input type="hidden" name="date_created" id="date_created" value="<?php //echo date();  ?>"/>
						</div>
						<div class="control-group ">
							<label class="control-label">Data Provider Address:</label>
							<div class="controls">
								<textarea type="text" name="address" id="form_address" rows="8"><?php echo set_value("address"); ?> </textarea>
								<?php echo form_error('address', '<p class="error1">'); ?></div>
						</div>

					</div>
				</div>
			</div>

		</div>
		<div class="widget-footer">
			<!-- <button type="submit" class="pull-left btn btn-info btn-small">Submit</button>-->
			<div class="modal-footer" style="padding-left:350px;" >
				<!--<button type="button" class="btn">Cancel</button>
				  <button type="cancel" class="pull-left btn btn-info btn-small">Cancel</button>-->
				<input class="btn btn-primary pull-left" type="submit" value="Save" />
			</div>
		</div>
		</form> </div> </div>
<!-- /Main window -->
