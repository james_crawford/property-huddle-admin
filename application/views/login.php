<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Propertyhuddle - Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Bluth Company">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">
		<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/theme.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/alertify.css" rel="stylesheet">
		<link rel="Favicon Icon" href="favicon.ico">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
	</head>
	<body>
		<div id="wrap">
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="row-fluid">
							<div class="widget container-narrow">
								<div class="widget-header"> <i class="icon-user"></i>
									<h5>Log in to your account</h5>
								</div>
								<div class="widget-body clearfix" style="padding:25px;">
									<?php echo form_open("login"); ?>
									<div class="ErrorMsg" style="padding-bottom:2px; color:red">
										<?php
										echo validation_errors('<p class="error">');
										echo '<p class="error">'.$this->session->flashdata('message').'</p>';
										?>
									</div>
									<div class="control-group">
										<div class="controls">
											<input class="btn-block" type="text" id="inputEmail" name="email" placeholder="Email">
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<input class="btn-block" type="password" id="inputPassword" name="password" placeholder="Password">
										</div>
									</div>
									<div class="control-group">
										<div class="controls clearfix">
											<label style="width:auto" class="checkbox pull-left">
												<input type="checkbox">
												Remember me </label>
											<a style="padding: 5px 0px 0px 5px;" href="<?php echo site_url(); ?>/forgotpassword" class="pull-right">Forgot Password?</a> </div>
									</div>
									<button type="submit" class="btn pull-right">Sign in</button>
									<?php echo form_close(); ?> </div>
							</div>
							<!--<div style="text-align:center">
							  <p>Neen an account? <a href="#">Create Account</a></p>
							</div>-->
						</div>
						<!--/row-fluid-->
					</div>
					<!--/span10-->
				</div>
				<!--/row-fluid-->
			</div>
			<!--/.fluid-container-->
		</div>
		<!-- wrap ends-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
		<script type="text/javascript" src='<?php echo base_url(); ?>assets/js/sparkline.js'></script>
		<script type="text/javascript" src='<?php echo base_url(); ?>assets/js/morris.min.js'></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.masonry.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.imagesloaded.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.facybox.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.elfinder.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.alertify.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/realm.js"></script>
	</body>
</html>