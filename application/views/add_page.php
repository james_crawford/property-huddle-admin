<!-- Add New Provider Main window -->
<script type="text/javascript" src="http://admin.propertyhuddle.com/assets/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		theme_advanced_toolbar_location : "top",
		theme_advanced_buttons1 : "bold,italic,underline",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : ""

		//plugins : "maxchars",
		//max_chars : 10,
		//max_chars_indicator : "characterCounter"
	});


</script>

<div class="main_container" id="forms_page" style="padding-top:20px;" >

	<div class="row-fluid">

		<div class="widget widget-padding span12">

			<?php if (isset($message) AND $message != '')
			{ ?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
				<?php echo $message; ?> </div>
<?php } ?>
			<div class="widget-header"><i class="icon-list-alt"></i>
				<h5><?php echo $title ?></h5>
				<div style="float:right">
					<button  style="margin-top:10px; margin-right: 5px;" onClick="window.location.href='<?php echo site_url("staticpages"); ?>'"  class="btn" id="cancel">Back</button>
				</div>
			</div>
			<form action="<?php echo site_url('staticpages/add'); ?>" method="post" id="branch_form" enctype="multipart/form-data" class="form-horizontal form-horizontal2">
				<div class="widget-body" style="min-height:330px;">
					<div class="widget-forms clearfix">
						<div class="control-group" style="height: 5px;">
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>
						<div class="control-group">
							<label class="control-label">Select Page</label>
							<div class="controls">
								<select name="selectpageid" data-placeholder="Select here..">
									<option>Aboutus</option>
									<option>Privacypolicy</option>
									<option>Tremsofuse</option>
								</select>
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label">Page Content * </label>
							<div class="controls">
								<textarea rows="8"  style="width:500px;" name="page_description" id="form_page_description"><?php echo set_value("page_description"); ?></textarea>
<?php echo form_error('page_description', '<p class="error">'); ?>
								<div id="characterCounter"></div>
								<div id="characterCountermsg" style="color:#FF0000;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="widget-footer">
					<button style="margin-left: 250px;" type="submit" name="submit" class="btn btn-primary">Submit</button>
				</div>
<?php echo form_close(); ?>

		</div>

	</div>

</div>
<!-- /Main window -->
