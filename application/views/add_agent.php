<!-- Main window -->
<div class="main_container" id="forms_page" style="padding-top:20px;">

	<div class="row-fluid" >
		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-list-alt"></i><h5><?php echo $title ?></h5>
                <div style="float:right">
					<button  style="margin-top:10px; margin-right: 5px;" onClick="window.location.href='<?php echo site_url("agents"); ?>'"  class="btn" id="cancel">Back</button>
                </div>
            </div>
            <form action="<?php echo site_url("agents/add") ?>" id="agent_form" class="form-horizontal" method="post">
				<div class="widget-body">
					<div class="widget-forms clearfix">

						<div class="control-group" style="height: 5px;" >
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>

						<div class="control-group " >
							<label class="control-label" style="width:220px">Agent Name<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_name" id="form_agent_name" value="<?php echo set_value("agent_name"); ?>">
								<?php echo form_error('agent_name', '<p class="error">'); ?>

							</div>
						</div>

						<div class="control-group " >
							<label class="control-label" style="width:220px">Agent Location<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_location" id="agent_location" value="<?php echo set_value("agent_location"); ?>">
								<?php echo form_error('agent_location', '<p class="error">'); ?>

							</div>
						</div>

                        <div class="control-group " >
							<label class="control-label" style="width:220px">Agent Location Postcode<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_postcode" id="agent_postcode" value="<?php echo set_value("agent_postcode"); ?>">
								<?php echo form_error('agent_postcode', '<p class="error">'); ?>

							</div>
						</div>

                        <div class="control-group " >
							<label class="control-label" style="width:220px">Agent Short Code<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_short_code" id="agent_short_code" value="<?php echo set_value("agent_short_code"); ?>">
								<?php echo form_error('agent_short_code', '<p class="error">'); ?>

							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">External Feed?</label>
							<div class="controls"  style="margin-left:230px"> 
								<table width="120px" border="0" cellpadding="2">
									<tr>
										<td align="center" valign="middle"><input type="radio" name="externalFeed" value="1"></td>
										<td align="left" ><span style="position:relative; top: 4px">External</span></td>
									</tr>
									<tr>
										<td align="center" valign="middle"><input type="radio" checked="checked" name="externalFeed" value="0"></td>
										<td align="left" ><span style="position:relative; top: 4px">Direct</span></td>
									</tr>
								</table>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Registration Number<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="company_registration_number" id="company_registration_number" value="<?php echo set_value("company_registration_number"); ?>">
								<?php echo form_error('company_registration_number', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Company VAT Number<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="company_vat_number" id="company_vat_number" value="<?php echo set_value("company_vat_number"); ?>">
								<?php echo form_error('company_vat_number', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Website<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_website" id="agent_website" value="<?php echo set_value("agent_website"); ?>">
								<?php echo form_error('agent_website', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Email Address<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_email_address" id="agent_email_address" value="<?php echo set_value("agent_email_address"); ?>">
								<?php echo form_error('agent_email_address', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Phone Number<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_telephone_number" id="agent_telephone_number" value="<?php echo set_value("agent_telephone_number"); ?>">
								<?php echo form_error('agent_telephone_number', '<p class="error">'); ?></div>
						</div>
						<div class="control-group ">
							<label  class="control-label" style="width:220px">About the Agent<span style="color: red">*</span></label>
							<div  class="controls" style="margin-left:230px"><textarea rows="8"  style="width:450px;" name="agent_about_us" id="form_agent_about_us"></textarea>
								<?php echo form_error('agent_about_us', '<p class="error">'); ?>
								<div id="characterCounter"></div>
								<div id="characterCountermsg" style="color:#FF0000;"></div>
							</div>
						</div>
					</div>
				</div>
		</div>
		<div class="widget-footer">
			<button  style="margin-left: 250px; margin-top:10px;"  class="btn btn-primary" id="save">Save</button>
		</div>
		</form>
	</div>

</div>
<!-- /Main window -->

