<!-- Main window -->
<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">


		<div class="widget widget-padding span6" style="width:100%;">
			<?php
			$message = $this->session->flashdata('Success');
			if (isset($message) AND $message != '')
			{
				?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $message; ?>
				</div><?php } ?>
			<?php
			$propertiesMSG = $this->session->flashdata('propertiesMSG');
			if (isset($propertiesMSG) AND $propertiesMSG != '')
			{
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $propertiesMSG; ?>
				</div><?php } ?>


			<div class="widget-header">
				<i class="icon-home"></i>
				<h5>Properties</h5>
				<div class="widget-buttons">
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Branch</th>
							<th>Address</th>
							<th>Price</th>
							<th>Department</th>
							<th>Summary</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($propertyData as $record)
						{
							?>
							<tr>
								<td><?php echo $record['agent_name']." (".$record['branch_name'].")"; ?></td>
								<td><?php echo $record['address_1']." ".$record['address_2'].", ".$record['town']; ?></td>
								<td>&pound;<?php echo number_format($record['price'], 0); ?></td>
								<td><?php echo $record['department']; ?></td>
								<td><?php echo $record['summary']; ?></td>
								<td>
									<?php
									if ($record['active'] == '1')
									{
										?>
										<a href="#"  onclick="setpropertiesVal(<?php echo $record['branches_id'] ?>)"  data-toggle="modal" data-target="#modalDeactivatepropertiesBox" class="demo_notify_dialog" data-type="confirm"><span class="label label-success">Active</span></a>
										<?php
									}
									else
									{
										?>
										<a href="#"  onclick="setpropertiesVal(<?php echo $record['branches_id'] ?>)"  data-toggle="modal" data-target="#modalActivatepropertiesBox" class="demo_notify_dialog" data-type="confirm"><span class="label">Inactive</span></a>
										<?php
									}
									?>
								</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
											Action
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu pull-right">
										  <!--<li><a href="#"><i class="icon-envelope"></i> Email</a></li>-->
											<li><a href="<?php echo site_url('properties/view/'.strtolower($record['department']).'/'.$record['property_ref_key_id']); ?>"><i class="icon-edit"></i> View</a></li>
										</ul>
									</div>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
            </div> <!-- /widget-body -->
		</div> <!-- /widget -->
	</div>
</div>
<!--- For Show Sucess Message at footer-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
$propertiesMSG = $this->session->flashdata('propertiesMSG');
$propertiesMSGType = $this->session->flashdata('propertiesMSGType');
if (isset($propertiesMSG) AND $propertiesMSG != '')
{
	?>
	<script>
		$(document).ready(function() {
			ShowMessage('<?php echo $propertiesMSG; ?>','<?php echo $propertiesMSGType; ?>');
		});




	</script>
<?php } ?>
<script>

	var oTable;
	var giRedraw = false;

	$(document).ready(function() {

	} );



	function setpropertiesVal(propertiesID) {
		$("#deactivatedpropertiesID").val(propertiesID);
		$("#activatedpropertiesID").val(propertiesID);
	}
	function activatepropertiesStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activatepropertiesStatus'); ?>/'+ $("#activatedpropertiesID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('properties'); ?>');
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});


	}
	function deactivatepropertiesStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivatepropertiesStatus'); ?>/'+ $("#deactivatedpropertiesID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('properties'); ?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		;
	}


</script>