<!-- Main window -->
<div class="main_container" id="forms_page" style="padding-top:20px; margin-bottom: -35px;">
	<div class="row-fluid" >
		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-home"></i><h5><?php echo $title ?></h5>
                <div class="widget-buttons">
                    <button  onClick="window.location.href='<?php echo site_url("properties"); ?>'"  class="btn" id="cancel">Back</button>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>
			<div class="widget-body">
                <div class="widget-forms clearfix" >
                    <table width="100%" cellpadding="2" cellspaceing="2">
						<tr>
							<td valign="top" width="40%" >
								<table  cellpadding="2" cellspaceing="2">
									<tr>
										<th style="text-align: left" valign="top" >Address</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['address_1']." ".$propertyData['address_2'] ?></td>
									</tr>

									<?php if( $propertyData['address_3'] != '' && $propertyData['address_3'] != ' ')
									{ 
									?>
									<tr>
										<th style="text-align: left"  valign="top"></th>
										<td ></td>
										<td><?php echo $propertyData['address_3']; ?></td>
									</tr>
									<?php
									}
									?>
									<?php if( $propertyData['address_4'] != '' && $propertyData['address_4'] != ' ')
									{ 
									?>
									<tr>
										<th style="text-align: left"  valign="top"></th>
										<td ></td>
										<td><?php echo $propertyData['address_4']; ?></td>
									</tr>
									<?php
									}
									?>
									<tr>
										<th style="text-align: left" valign="top" ></th>
										<td style="width: 20px;"></td>
										<td><?php echo $propertyData['town'] ?></td>
									</tr>
									<tr>
										<th style="text-align: left" valign="top" >Post Code</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['postcode_1']." ".$propertyData['postcode_2'] ?></td>
									</tr>
									<tr>
										<th style="text-align: left" valign="top" >Price </th>
										<td style="width: 20px;">:</td>
										<td>&pound; <?php echo number_format($propertyData['price'], 0)." ".$propSubType[$propertyData['property_sub_type_id']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Bedrooms</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['bedrooms'] ?></td>
									</tr>
									<tr>
										<th style="text-align: left" valign="top" >Published</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['published']]; ?></td>
									</tr>

									
									
								</table>
							</td>
							<td width="40%" valign="top">


								<table cellpadding="2" cellspaceing="2">
<tr>
										<th style="text-align: left" valign="top" >Active</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['active']]; ?></td>
									</tr>


									<tr>
										<th style="text-align: left" valign="top" >Transaction Type</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $transType[$propertyData['trans_type_id']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Status</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $status[$propertyData['status_id']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Minimum Size</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['min_size']; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Maximum Size</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['max_size']; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Area Units</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $areaUnits[$propertyData['area_sized_unit_id']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Business For Sale</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['for_sale']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Price Per Unit</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['price_per_unit']; ?></td>
									</tr>

									<tr>
										<th style="text-align: left" valign="top" >Property Type</th>
										<td style="width: 20px;">:</td>
										<td>
											<?php echo $propSubType[$propertyData['property_sub_type_id']]; ?></td>
									</tr>
								</table>
							</td>
							<td width="40%" valign="top">
								<img src="<?php echo $propertyData['domain_url']; ?>" width="135" height="83" alt="<?php echo $propertyData['image_name']; ?>">
							</td>
						</tr>
						<tr>
							<th colspan="3" width="600px" style="text-align: left"  valign="top">Summary</th>
						</tr>
						<tr>
							<td colspan="3"  ><?php echo $propertyData['summary'] ?></td>
						</tr>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>
<div class="main_container" id="users_page">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h5>Property Timeline</h5>
				<div class="widget-buttons">
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<?php
				if (!empty($archiveData))
				{
					?>
					<table id="users" class="table table-striped table-bordered dataTable">
						<thead>
							<tr>
								<th>Address</th>
								<th>Beds</th>
								<th>Price</th>
								<td>Transaction Type</td>
								<td>Status</td>
								<td>Min. Size</td>
								<td>Max. Size</td>
								<td>Area Units</td>
								<td>Business For Sale</td>
								<td>Price per unit</td> 
								<td>Property Type</td>
								<th>Published</th>
								<th>Active</th>
								<th>Date Archived</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php
								$address = $propertyData['address_1']." ";
								if($propertyData['address_2'] != '' && $propertyData['address_2'] != ' ' && $propertyData['address_2'] != null)
								{
									$address .= " ".$propertyData['address_2'].", ";
								}
								if($propertyData['address_3'] != '' && $propertyData['address_3'] != ' ' && $propertyData['address_3'] != null)
								{
									$address .= $propertyData['address_3'].", ";
								}
								if($propertyData['address_4'] != '' && $propertyData['address_4'] != ' ' && $propertyData['address_4'] != null)
								{
									$address .= $propertyData['address_4'].", ";
								}
										$address .= " ".$propertyData['town'].", ";
										$address .= $propertyData['postcode_1'];
										$address .= " ".$propertyData['postcode_2']; 
									echo $address;
								?></td>
								<td><?php echo $propertyData['bedrooms']; ?></td>
								<td><?php echo "&pound".number_format($propertyData['price'], 0)." ".$priceQualifier[$propertyData['price_qualifier']]; ?></td>
								<td><?php echo $transType[$propertyData['trans_type_id']]; ?></td>
								<td><?php echo $status[$propertyData['status_id']]; ?></td>
								<td><?php echo $propertyData['min_size']; ?></td>
								<td><?php echo $propertyData['max_size']; ?></td>
								<td><?php echo $areaUnits[$propertyData['area_sized_unit_id']]; ?></td>
								<td><?php echo $yesNo[$propertyData['for_sale']]; ?></td>
								<td><?php echo $propertyData['price_per_unit']; ?></td> 
								<td><?php echo $propSubType[$propertyData['property_sub_type_id']]; ?></td>
								<td><?php echo $yesNo[$propertyData['published']]; ?></td>
								<td><?php echo $yesNo[$propertyData['active']]; ?></td>
								<td>Current</td>
							</tr>	





							<?php
							foreach ($archiveData as $archive => $record)
							{
								?>
								<tr>
									<td><?php
								$address = $record['address_1']." ";
								if($record['address_2'] != '' && $record['address_2'] != ' ')
								{
									$address .= " ".$propertyData['address_2'].", ";
								}
								if($record['address_3'] != '' && $record['address_3'] != ' ')
								{
									$address .= $record['address_3'].", ";
								}
								if($record['address_4'] != '' && $record['address_4'] != ' ')
								{
									$address .= $record['address_4'].", ";
								}
										$address .= " ".$record['town'].", ";
										$address .= $record['postcode_1'];
										$address .= " ".$record['postcode_2']; 
									echo $address;
								?></td>
									<td><?php echo $record['bedrooms']; ?></td>
									<td><?php echo "&pound".number_format($record['price'], 0); ?></td>
									<td><?php echo $transType[$record['trans_type_id']]; ?></td>
									<td><?php echo $status[$record['status_id']]; ?></td>
									<td><?php echo $record['min_size']; ?></td>
									<td><?php echo $record['max_size']; ?></td>
									<td><?php echo $areaUnits[$record['area_sized_unit_id']]; ?></td>
									<td><?php echo $yesNo[$record['for_sale']]; ?></td>
									<td><?php echo $record['price_per_unit']; ?></td> 
									<td><?php echo $propSubType[$record['property_sub_type_id']]; ?></td>
									<td><?php echo $yesNo[$record['published']]; ?></td>
									<td><?php echo $yesNo[$record['propertyActive']]; ?></td>
									<td><?php echo date("d/m/Y", strtotime($record['date_archived'])); ?></td>

								</tr>
							<?php }
							?>
						</tbody>
					</table>
					<?php
				}
				?>
            </div>
		</div> 
	</div>
</div>
<script>
	function setDPVal(dpID) {
		$("#deactivatedStaffID").val(dpID);
		$("#activatedStaffID").val(dpID);
		
	}
	
	function activateStaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activateStaffStatus'); ?>/'+ $("#activatedStaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('branch/view/'.$branchData['agents_id'].'/'.$branchData['id'].'/'); ?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		

	}
	function deactivateStaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivateStaffStatus'); ?>/'+ $("#deactivatedStaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('branch/view/'.$branchData['agents_id'].'/'.$branchData['id'].'/'); ?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		;
	}
	
	
	
</script>

<!-- Agent Brach's start  -->
<!-- /Main window -->

