<!-- Main window -->
<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header">
				<i class="icon-group"></i>
				<h5><?php echo $title ?></h5>
				<div class="widget-buttons">
					<a href="<?php echo site_url('agents/add'); ?>" data-title="Add Agents" data-toggle="" data-target=""><i class="icon-plus"></i></a>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Agent Name</th>
							<th>No. of Branches</th>
							<th>Agent Email Address</th>
							<th>Telephone Number</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($agents as $key => $value)
						{
							?>
							<tr>
								<td><?php echo $value['agent_name']; ?></td>
								<td><?php echo $value['branches']; ?></td>
								<td><?php echo $value['agent_email_address']; ?></td>
								<td><?php echo $value['agent_telephone_number']; ?></td>
								<td>
									<?php
									if ($value['active'] == '1')
									{
										?>
										<a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalDeactivateAgentBox" class="demo_notify_dialog" data-type="confirm"><span class="label label-success">Active</span></a>
										<?php
									}
									else
									{
										?>
										<a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalActivateAgentBox" class="demo_notify_dialog" data-type="confirm"><span class="label">Inactive</span></a>
										<?php
									}
									?>
								</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
											Action
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu pull-right">
											<li><a href="<?php echo site_url('agents/edit/'.$value['id']); ?>"><i class="icon-edit"></i>Edit</a></li>
											<li><a href="<?php echo site_url('agents/view/'.$value['id']); ?>"><i class="icon-folder-open-alt"></i>View</a></li>
																						<?php
											if ($value['active'] == '1')
											{
												?>
												<li><a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalDeactivateAgentBox" class="demo_notify_dialog" data-type="confirm"><i class="icon-exchange"></i> Change Status</a></li> <!-- @TODO set dynamic active/deactive button -->
												<?php
											}
											else
											{
												?>
												<li><a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalActivateAgentBox" class="demo_notify_dialog" data-type="confirm"><i class="icon-exchange"></i> Change Status</a></li> <!-- @TODO set dynamic active/deactive button -->
												<?php
											}
											?></ul>
									</div>
								</td>
							</tr>
<?php } ?>
					</tbody>
				</table>
            </div> <!-- /widget-body -->
		</div> <!-- /widget -->
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
$agentMSG = $this->session->flashdata('agentsMSG');
$agentsMSGType = $this->session->flashdata('agentsMSGType');
if (isset($agentMSG) AND $agentMSG != '')
{
	?>
	<script>
		$(document).ready(function() {
			ShowMessage('<?php echo $agentMSG; ?>','<?php echo $agentsMSGType; ?>');
		});
	</script>
<?php } ?>
<script>
	function setDPVal(dpID) {
		$("#deactivatedAgentID").val(dpID);
		$("#activatedAgentID").val(dpID);
		
	}
	
	function activateAgentStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activateAgentStatus') ; ?>/'+ $("#activatedAgentID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('agents');?>');
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		

	}
	function deactivateAgentStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivateAgentStatus') ; ?>/'+ $("#deactivatedAgentID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('agents');?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
			;
}
	
	
	
</script>
<!-- /Main window -->





