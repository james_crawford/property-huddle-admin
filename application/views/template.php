<?php
//$class = strtolower($this->uri->segment(2));
//$site_name = substr(base_url(),7,-1);
//echo "<pre>"; print_r($AdminInfo); die;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Property Huddle :: <?php echo $title ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Bluth Company">
		<link rel="shortcut icon" href="<?php echo base_url() ?>assets/ico/favicon.png">
		<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/css/theme.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/css/alertify.css" rel="stylesheet">

		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
		<link rel="Favicon Icon" href="favicon.ico">
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
			<script src="<?php echo base_url() ?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.reveal.js"></script>
				</head>
	<body>

		<div id="wrap">

			<div id="myselector" style="display:none;">Welcome <?php echo $AdminInfo[0]['Name'] ?></div>

			<?php if ($header) echo $header; ?>

			<div class="container-fluid">

				<?php if ($left) echo $left; ?>

				<?php if ($middle) echo $middle; ?>

			</div>

		</div>

		<?php include("examples.php"); ?>

		<!-- main container ends here-->
		<?php if ($footer) echo $footer; ?>

	</body>
</html>