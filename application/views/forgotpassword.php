<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Propertyhuddle - Forgot Password</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Bluth Company">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">
		<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/theme.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/alertify.css" rel="stylesheet">
		<link rel="Favicon Icon" href="favicon.ico">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="wrap">
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="row-fluid">
							<div class="widget container-narrow">
								<div class="widget-header">
									<i class="icon-user"></i>
									<h5>Forgot Password</h5>
								</div>
								<div class="widget-body clearfix" style="padding:25px;">
									<?php echo form_open("forgotpassword"); ?>

									<div class="ErrorMsg" style="padding-bottom:2px; color:red">
										<?php
										echo validation_errors('<p class="error">');
										echo '<p class="error">'.$this->session->flashdata('message').'</p>';
										?>
									</div>
									<div class="control-group">
										<div class="controls">
											<input class="btn-block" type="text" id="inputEmail" name="email" placeholder="Email">
										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<button type="submit" class="btn pull-right">Get password</button>
										</div>
										<div class="controls">
											<div class="btn pull-left" onclick="window.location='<?php echo site_url('') ?>'">Back</div>
										</div>
									</div>
									<?php echo form_close(); ?>
								</div>
							</div>

						</div><!--/row-fluid-->
					</div><!--/span10-->
				</div><!--/row-fluid-->
			</div><!--/.fluid-container-->
		</div><!-- wrap ends-->


	</body>
</html>
