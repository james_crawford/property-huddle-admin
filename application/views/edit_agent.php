<!-- Main window -->
<div class="main_container" id="forms_page" style="padding-top:20px;">
	<div class="row-fluid" >
		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-list-alt"></i><h5><?php echo $title ?></h5>
                <div style="float:right">
					<button  style="margin-top:10px; margin-right: 5px;" onClick="window.location.href='<?php echo site_url("agents/view/".$agentData["id"]); ?>'"  class="btn" id="cancel">Back</button>
                </div>
            </div>
            <form action="<?php echo site_url("agents/edit/".$agentData['id']) ?>" id="agent_form" class="form-horizontal" method="post">
				<div class="widget-body">
					<div class="widget-forms clearfix">

						<div class="control-group" style="height: 5px;" >
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Name<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input value="<?php echo $agent; ?>" type="text" name="agent_name" id="form_agent_name">
								<input value="<?php echo $agentData["id"]; ?>" type="hidden" name="agent_id" id="form_agent_id">
								<?php echo form_error('agent_name', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Location<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_location" id="agent_location" value="<?php echo $agentData["agent_location"]; ?>">
								<?php echo form_error('agent_location', '<p class="error">'); ?>
							</div>
						</div>

                        <div class="control-group ">
							<label class="control-label" style="width:220px">Agent Location Postcode<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_postcode" id="agent_postcode" value="<?php echo $agentData["agent_postcode"]; ?>">
								<?php echo form_error('agent_postcode', '<p class="error">'); ?>
							</div>
						</div>

                        <div class="control-group " >
							<label class="control-label" style="width:220px">Agent Short Code<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_short_code" id="agent_short_code" value="<?php echo $agentData["agent_short_code"]; ?>">
								<?php echo form_error('agent_short_code', '<p class="error">'); ?>

							</div>
						</div>


						<div class="control-group ">
							<label class="control-label" style="width:220px">External Feed?</label>
							<div class="controls"  style="margin-left:230px">
								<table width="120px" border="0" cellpadding="2">
									<tr>
										<td align="center" valign="middle"><input type="radio" name="externalFeed" <?php if  ($agentData['external_feed'] == '1') { echo ' checked="checked" ';} ?>  value="1"></td>
										<td align="left" ><span style="position:relative; top: 4px">External</span></td>
									</tr>
									<tr>
										<td align="center" valign="middle"><input type="radio" <?php if  ($agentData['external_feed'] == '0') { echo ' checked="checked" ';} ?> name="externalFeed" value="0"></td>
										<td align="left" ><span style="position:relative; top: 4px">Direct</span></td>
									</tr>
								</table>


							</div>
						</div>



						<div class="control-group ">
							<label class="control-label" style="width:220px">Registration Number<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="company_registration_number" id="company_registration_number" value="<?php echo $agentData["company_registration_number"]; ?>">
<?php echo form_error('company_registration_number', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Company VAT Number<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="company_vat_number" id="company_vat_number" value="<?php echo $agentData["company_vat_number"]; ?>">
<?php echo form_error('company_vat_number', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Website<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_website" id="agent_website" value="<?php echo $agentData["agent_website"]; ?>">
<?php echo form_error('agent_website', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Email Address<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_email_address" id="agent_email_address" value="<?php echo $agentData["agent_email_address"]; ?>">
<?php echo form_error('agent_email_address', '<p class="error">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Agent Phone Number<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><input type="text" name="agent_telephone_number" id="agent_telephone_number" value="<?php echo $agentData["agent_telephone_number"]; ?>">
<?php echo form_error('agent_telephone_number', '<p class="error">'); ?></div>
						</div>

						
						<div class="control-group ">
							<label class="control-label" style="width:220px">Trade Contact</label>
							<div class="controls" style="margin-left:230px"><?php echo form_dropdown('trade_staff_id', $staff, $agentData['trade_staff_id']); ?></div>
						</div>


						<div class="control-group ">
							<label class="control-label" style="width:220px">Billing Contact Name</label>
							<div class="controls" style="margin-left:230px"><?php echo form_dropdown('billing_staff_id', $staff, $agentData['billing_staff_id']); ?></div>
						</div>
						
						<div class="control-group ">
							<label class="control-label" style="width:220px">About the Agent<span style="color: red">*</span></label>
							<div class="controls" style="margin-left:230px"><textarea rows="8"  style="width:450px;" name="agent_about_us" id="form_agent_about_us"><?php echo $agentData["about_us"]; ?></textarea>
								<?php echo form_error('agent_about_us', '<p class="error">'); ?>
								<div id="characterCounter"></div>
								<div id="characterCountermsg" style="color:#FF0000;"></div>
							</div>
						</div>
					</div>
				</div>
		</div>
		<div class="widget-footer">
			<input type="hidden" name="update" value="hasUpdate" />
			<input type="submit"  style="margin-left:250px; margin-top:10px;" value="Save"  class="btn btn-primary" id="save" />
		</div>
		</form>
	</div>

</div>
<!-- /Main window -->
</div><!--/.fluid-container-->
