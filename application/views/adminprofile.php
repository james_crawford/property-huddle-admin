<!-- Main window -->

<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">
		<?php if (isset($message) AND $message != '')
		{ ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
	<?php echo $message; ?>
			</div><?php } ?>

<?php echo form_open("profile"); ?>
		<div class="widget widget-padding span6" style="width:100%;">
            <div class="widget-header"><i class="icon-list-alt"></i><h5>Admin Profile</h5></div>
            <form action="#register_form2" id="register_form2">
				<div class="widget-body" style="min-height:330px;">
					<div class="widget-forms clearfix">
						<div class="control-group ">
							<label for="form_username">Name:</label>
							<?php
							$uname = (isset($userdetails[0]["Name"]) && $userdetails[0]["Name"] != '') ? $userdetails[0]["Name"] : '';
							?>
							<div class="controls"><input type="text" name="username" id="username" value="<?php echo (set_value('username') != '') ? set_value('username') : $uname; ?>">
<?php echo form_error('username', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label for="form_password">Password:</label>
							<div class="controls"><input type="password" name="password" id="password" value="<?php echo set_value("password"); ?>">
<?php echo form_error('password', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label for="password_confirm">Confirm Password:</label>
							<div class="controls"><input type="password" name="password_confirm" id="password_confirm" value="<?php echo set_value("password_confirm"); ?>">
<?php echo form_error('password_confirm', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label for="form_email">E-mail:</label>
							<?php
							$uemail = (isset($userdetails[0]["Email"]) && $userdetails[0]["Email"] != '') ? $userdetails[0]["Email"] : '';
							?>
							<div class="controls"><input type="text" name="email" id="form_email" value="<?php echo (set_value('email') != '') ? set_value('email') : $uemail; ?>">
<?php echo form_error('email', '<p class="error">'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="widget-footer">
					<input type="hidden" id="id" name="id" value="<?php echo $userdetails[0]["id"]; ?>">
					<button type="submit" id="submit" name="submit" class="pull-left btn btn-info btn-small">Submit</button>
				</div>
            </form>
		</div>
<?php echo form_close(); ?>
	</div>
</div>
<!-- /Main window -->
