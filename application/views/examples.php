<!-- example modal -->
<div id="example_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Support Ticket</h3>
	</div>
	<div class="modal-body">
        <p>Here you can view and manage this support ticket.</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
	</div>
</div>

<!-- example modal -->
<div id="example_modal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Example Modal</h3>
	</div>
	<div class="modal-body">
        <p>Here you can write more information about the object you clicked</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
	</div>
</div>

<!-- task_modal modal -->
<div id="task_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Task info example</h3>
	</div>
	<div class="modal-body">
        <div class="clearfix">
			<img src="<?php echo base_url(); ?>assets/img/avatars/11.jpg" class="img-circle" style="float: left; width: 65px; margin-right: 20px;">
			<h3 style="margin:0">John</h3>
			<p class="muted">Marketing</p>
        </div>
        <hr>
        <h5>Task</h5>
        <p>Create a marketing plan for the new campaign</p>
        <h5>status&nbsp;&nbsp;<small>60%</small></h5>
        <div class="progress">
			<div class="bar" style="width: 60%;"></div>
        </div>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>


<!-- Deactivate modal -->
<div id="modalDeactivateBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Deactivate Data Provider</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to deactivate this Dataprovider?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="deactivatedDpID" id="deactivatedDpID" >
        <button onclick="deactivateDPStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>



<!-- Activate modal -->
<div id="modalActivateBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Activate Data Provider</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to activate this Dataprovider?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="activatedDpID" id="activatedDpID" >
        <button onclick="activateDPStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>



<!-- Deactivate modal -->
<div id="modalDeactivateAgentBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Deactivate Agent</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to deactivate this Agent?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="deactivatedAgentID" id="deactivatedAgentID" >
        <button onclick="deactivateAgentStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>



<!-- Activate modal -->
<div id="modalActivateAgentBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Activate Agent</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to activate this Agent?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="activatedAgentID" id="activatedAgentID" >
        <button onclick="activateAgentStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>

<!-- Deactivate modal -->
<div id="modalDeactivateBranchBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Deactivate Branch</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to deactivate this branch?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="deactivatedBranchID" id="deactivatedBranchID" >
        <button onclick="deactivateBranchStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>



<!-- Activate modal -->
<div id="modalActivateBranchBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Activate Branch</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to activate this branch?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="activatedBranchID" id="activatedBranchID" >
        <button onclick="activateBranchStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>



<!-- Deactivate modal -->
<div id="modalDeactivateStaffBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Deactivate Staff</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to deactivate this staff member?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="deactivatedStaffID" id="deactivatedStaffID" >
        <button onclick="deactivateStaffStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>



<!-- Activate modal -->
<div id="modalActivateStaffBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Activate Staff</h3>
	</div>
	<div class="modal-body">
        <p>Are you sure you want to activate this staff member?</p>
	</div>
	<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<input type="hidden" value="" name="activatedStaffID" id="activatedStaffID" >
        <button onclick="activateStaffStatus()" class="btn btn-primary">Save changes</button>
	</div>
</div>

