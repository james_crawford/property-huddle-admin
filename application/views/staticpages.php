<!-- Main window -->
<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<?php
			$message = $this->session->flashdata('Success');
			if (isset($message) AND $message != '')
			{
				?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $message; ?>
				</div><?php } ?>
			<?php
			$branchMSG = $this->session->flashdata('branchMSG');
			if (isset($branchMSG) AND $branchMSG != '')
			{
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $branchMSG; ?>
				</div><?php } ?>
			<div class="widget-header">
				<i class="icon-sitemap"></i>
				<h5>Pages</h5>
				<div class="widget-buttons">
					<!--<a href="" data-title="Add User" data-toggle="modal" data-target="#example_modal"><i class="icon-plus"></i></a>-->
					<a href="<?php echo site_url('staticpages/add'); ?>" data-title="Add User" data-toggle="" data-target=""><i class="icon-plus"></i></a>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Page Name</th>
							<th>Page Content</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						//for($i=0; $i<=count($dataRecords); $i++); {
						foreach ($dataRecords as $key => $value)
						{
							?>
							<tr>
								<td><?php echo $value['page_name']; ?></td>

								<td><?php echo $value['page_content']; ?></td>
								<!--<span class="label label-success"></span>-->
								<td>
									<div class="btn-group">
										<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
											Action
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu pull-right">
										  <!--<li><a href="#"><i class="icon-envelope"></i> Email</a></li>-->
											<li><a href="<?php echo site_url('staticpages/edit/'.$value['id']); ?>"><i class="icon-edit"></i> Edit</a></li>

											<li><a href="#"  onclick="DelRecord('<?php echo site_url('staticpages/delete/'.$value['id']); ?>');" class="demo_notify_dialog" data-type="confirm"><i class="icon-trash"></i> Delete Record</a></li>

											<?php /* ?><li><a href="#" onclick="dletesure(<?php echo $value['id'];?>);"><i class="icon-trash"></i> Delete</a></li><?php */ ?>
										</ul>
									</div>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
            </div> <!-- /widget-body -->
		</div> <!-- /widget -->
	</div>
</div>
<!--- For Show Sucess Message at footer-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
$BranchMSG = $this->session->flashdata('BranchMSG');
$BranchMSGType = $this->session->flashdata('BranchMSGType');
if (isset($BranchMSG) AND $BranchMSG != '')
{
	?>
	<script>
		$(document).ready(function() {
			ShowMessage('<?php echo $BranchMSG; ?>','<?php echo $BranchMSGType; ?>');
		});
	</script>
<?php } ?>