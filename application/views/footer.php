<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src='<?php echo base_url(); ?>assets/js/sparkline.js'></script>
<script type="text/javascript" src='<?php echo base_url(); ?>assets/js/morris.min.js'></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.masonry.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.facybox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.alertify.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.knob.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/fullcalendar.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.gritter.min.js'></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.alertify.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/realm.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.alertify.min.js"></script>
<script>

	function DelRecord(url){ //alert(url);
		alertify.confirm( 'Are you sure to delete this record!!.', function (e) {
			if (e) {
				//after clicking OK
				window.location.href = url;
			} else {
				//after clicking Cancel
				return false;
			}
		});
	};
	function ShowMessage(message,type){
		switch(type){
			case 'info':
				alertify.log( message, 'info' );
				break;
			case 'error':
				alertify.log( message, 'error' );
				break;
			case 'success':
				alertify.log( message, 'success' );
				break;
		}

	}
</script>



