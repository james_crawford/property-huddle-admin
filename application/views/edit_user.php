<!-- Main window -->

<div class="main_container" id="forms_page" style="padding-top:20px;">
	<div class="row-fluid" >
		<div class="widget widget-padding span6" style="width:100%;" >
			<div class="widget-header"><i class="icon-list-alt"></i>
				<h5><?php echo $title ?></h5>
				<div style="float:right">
					<button  style="margin-top:10px; margin-right: 5px;" onClick="window.location.href='<?php echo site_url("users"); ?>'"  class="btn" id="cancel">Back</button>
				</div>
			</div>
			<form action="<?php echo site_url("users/edit/".$userData[0]['id']) ?>" id="user_form" class="form-horizontal" method="post">
				<div class="widget-body">
					<div class="widget-forms clearfix">
						<div class="control-group" style="height: 5px;" >
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Title :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="title" id="form_user_title" value="<?php echo $userData[0]["title"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Forename :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="forename" id="forename" value="<?php echo $userData[0]["forename"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Surname :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="surname" id="surname" value="<?php echo $userData[0]["surname"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">User Email<span style="color: red">*</span> :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="email_address" id="email_address" value="<?php echo $userData[0]["email_address"]; ?>">
								<?php echo form_error('email_address', '<p class="error">'); ?></div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Telephone Number :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="telephone_number" id="telephone_number" value="<?php echo $userData[0]["telephone_number"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">House Name/Nnumber :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="house_name_number" id="house_name_number" value="<?php echo $userData[0]["house_name_number"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Street :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="street" id="street" value="<?php echo $userData[0]["street"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Address2 :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="address2" id="address2" value="<?php echo $userData[0]["address2"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Town :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="town" id="town" value="<?php echo $userData[0]["town"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Country :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="country" id="country" value="<?php echo $userData[0]["country"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">County :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="county" id="county" value="<?php echo $userData[0]["county"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Postcode :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="postcode" id="postcode" value="<?php echo $userData[0]["postcode"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">House to Let/Sell ? :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="house_to_let_sell" id="house_to_let_sell" value="<?php echo $userData[0]["house_to_let_sell"]; ?>">
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label" style="width:220px">Need to move by :</label>
							<div class="controls" style="margin-left:230px">
								<input type="text" name="need_to_move_by" id="need_to_move_by" value="<?php echo $userData[0]["need_to_move_by"]; ?>">
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label" style="width:220px">Marketing ?:</label>
							<div class="controls" style="margin-left:230px">
								<?php $userData[0]["offers_and_news"]; ?>
								<input type="checkbox" name="newsletter" id="newsletter" value="1" <?php if ($userData[0]["offers_and_news"] == '1')
								{ ?> checked="checked"<?php } ?>/>
							</div>
						</div>


					</div>
				</div>
		</div>
		<div class="widget-footer">
			<button  style="margin-left:250px; margin-top:10px;"  class="btn btn-primary" id="save">Save changes</button>
		</div>
		</form>
	</div>
</div>
<!-- /Main window -->
</div>
<!--/.fluid-container-->
