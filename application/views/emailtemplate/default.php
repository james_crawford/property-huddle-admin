<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>propertyhuddle</title>
		<style type="text/css">
			a { text-decoration: none !important; }
			a:hover { text-decoration: none !important; }
		</style>
	</head>
	<body bgcolor="#FF3469" style="text-align: left; padding: 0; margin: 0; background: #FF3469;">
		<table width="650" align="center" border="0" cellspacing="0" cellpadding="0" style="font-family:Calibri, Arial, Helvetica, sans-serif">

			<tr>
				<td style="padding-top:5px;"><img src="<?php echo base_url() ?>images/top-bg-top.png" width="650" height="20"></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top" style="padding-left:13px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/find-a-helper.png" width="369" height="77" border="0"></a></td>
									</tr>

									<tr>
										<td style="font:20px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<?php for ($i = 0; $i < count($category); $i++)
												{ ?>
													<tr>
														<td align="left" width="60px;"><img src="<?php echo base_url() ?>images/arrow.jpg" width="32" height="31"></td>
														<td align="left"><?php echo $category[$i]['Category']; ?></td>
													</tr>
<?php } ?>
											</table></td>
									</tr>
								</table></td>
							<td align="right"><img src="<?php echo base_url() ?>images/babby.jpg" width="256" height="376"></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF" style="padding:0px 10px 0px 10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td bgcolor="#5DA82F" style="padding:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><font style="font-size:32px; color:#FFF">Need Help?</font></td>
												</tr>
												<tr>
													<td><table width="100%" border="0" cellspacing="3" cellpadding="3">
															<tr>
																<td align="center" valign="top"><img src="<?php echo base_url() ?>images/help.png" width="94" height="96"></td>
																<td><table width="100%" border="0" cellspacing="4" cellpadding="4">

																		<tr>
																			<td><a href="<?php echo base_url() ?>register/index/seeker"><img src="<?php echo base_url() ?>images/get-started.png" width="154" height="43" border="0"></a></td>
																		</tr>
																	</table></td>
															</tr>
														</table></td>
												</tr>
											</table></td>
										<td bgcolor="#FF9333"  style="padding:5px;" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><font style="font-size:32px; color:#FFF">Help Providers</font></td>
												</tr>
												<tr>
													<td><table width="100%" border="0" cellspacing="3" cellpadding="3">
															<tr>
																<td align="center" valign="top"><img src="<?php echo base_url() ?>images/help-provider.png" width="88" height="90"></td>
																<td><table width="100%" border="0" cellspacing="4" cellpadding="4">
																		<tr>
																			<td valign="top"><font style="font-size:16px; color:#FFFFFF">Sign up today to become a Helper
																				and be matched with families
																				throughout the UK.</font></td>
																		</tr>
																		<tr>
																			<td><a href="<?php echo base_url() ?>register/index/provider"><img src="<?php echo base_url() ?>images/get-started.png" width="154" height="43" border="0"></a></td>
																		</tr>
																	</table></td>
															</tr>
														</table></td>
												</tr>
											</table></td>
									</tr>
								</table></td>
						</tr>
						<tr>
							<td><img src="<?php echo base_url() ?>images/line.jpg" width="628" height="25"></td>
						</tr>
						<tr>
							<td align="center"><font size="+3" color="#5DA82F"><strong>Welcome to Find a Helper</strong></font></td>
						</tr>
						<tr>
							<td height="131"><font size="3px" color="#3E3E3E">
								<p>Dear Neil</p>
								<p>findahelper.co.uk has been named a winner in the 2011 O2 Smarta 100 awards, a prestigious collection of the UK’s most savviest, enterprising and smartest small businesses.</p>
								<p>There are just 4 days left to vote for the overall winner as voting closes at 6pm on 20th September.</p>
								<p>If you haven't yet voted then it would mean a great deal to us if you could take just 5 seconds of your time to vote by clicking the link below:</p>
								<h1><a href="#"><font style="background:#FF3469; font-size:15px; color:#FFFFFF; padding:4px;">Neil PLEASE CLICK HERE TO VOTE FOR findahelper.co.uk</font></a></h1>
								<p>As a small thank you to everyone who votes for us, we are offering a £250 cash prize to one lucky winner.</p>
								<p>For a chance to win just click the link above and follow the simple instructions to vote for findahelper.co.uk </p>
								<p>Once you have voted, answer this simple question:</p>
								<p>What colour does the pink 'Vote!' button change to once you have vote?</p>
								<p>Send your answers by email to <a href="mailto:win@findahelper.co.uk"><font color="#FF3469">win@findahelper.co.uk</font></a></p>
								<p>One lucky winner will be picked at random on Wednesday 21st September and the winner will be announced on our Facebook page.</p>
								<h1><a href="#"><font style="background:#FF3469; font-size:15px; color:#FFFFFF; padding:4px;">Keep in touch with us</font></a></h1>
								<p>For all the latest news, offers and special discounts, make sure you <a href="#"><font color="#FF3469">follow us on Twitter</font></a> and <a href="#"><font color="#FF3469">like us on Facebook</font></a></p>
								<p>Thanks again and very best wishes</p>
						</tr>
						<tr>
							<td height="32"><img src="<?php echo base_url() ?>images/line.jpg" width="628" height="25"></td>
						</tr>
						<tr>
							<td style="padding-top:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="60%"><table width="93%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="34%"><font size="+2" color="#5DA82F">Find us on</font></td>
													<td width="66%"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/facebook.jpg" width="35" height="34" border="0"></a> <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/twitter.jpg" width="34" height="34" border="0"></a> <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/google-plus.jpg" width="34" height="34" border="0"></a> <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/linked.jpg" width="35" height="34" border="0"></a> <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/you-tube.jpg" width="35" height="34" border="0"></a></td>
												</tr>
											</table></td>
										<td width="40%" align="right"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/sign-up.jpg" width="189" height="38" border="0"></a></td>
									</tr>
								</table></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td><img src="<?php echo base_url() ?>images/bottom-bg.png" width="650" height="17"></td>
			</tr>
			<tr>
				<td height="12px"></td>
			</tr>
			<tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center"><font color="#FFFFFF">Copyright (C) <?php echo date("Y") ?> Find a Helper All rights reserved.</font></td>
						</tr>
						<tr>
							<td style="padding-bottom:14px;">&nbsp;</td>
						</tr>
					</table></td>
			</tr>
		</table>
	</body>
</html>
