<!-- Add New Provider Main window -->
<script type="text/javascript" src="http://admin.propertyhuddle.com/assets/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode:"textareas",
		theme :"advanced",
		theme_advanced_toolbar_location: "top",
		theme_advanced_buttons1: "bold,italic,underline",
		theme_advanced_buttons2: "",
		theme_advanced_buttons3: "",

		plugins: "maxchars",
		max_chars: 1200,
		max_chars_indicator: "characterCounter"
	});
	var dps = '';
	$(document).ready(function() {
		//document ready here

	});

	function setBranchTelephoneNumbers()
	{
		//now get a branch ref
		$.ajax({
			url: '<?php echo site_url('ajax/getBranchNumbers'); ?>/'+$("#selectbranchid").val(),
			type:'POST',
			dataType: 'json',
			success: function(data)
			{
				$('#staffTelephoneNumber').empty()
				$.each(data, function(i, value) {
					$('#staffTelephoneNumber').append($('<option>').text(value).attr('value', i));
				});
				$('#staffTelephoneNumber').removeAttr("disabled");
			},
			error: function(transport)
			{
				//error
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}

		});
	}


	function setBranch()
	{

		//now get a branch ref
		$.ajax({
			url: '<?php echo site_url('ajax/getAllBranchNames'); ?>/'+$("#selectagentid").val(),
			type:'POST',
			dataType: 'json',
			success: function(data)
			{
				$('#selectbranchid').empty()
				$.each(data, function(i, value) {
					$('#selectbranchid').append($('<option>').text(value).attr('value', i));
				});
				$('#selectbranchid').removeAttr("disabled");
			},
			error: function(transport)
			{
				//error
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}

		});

	}



	var assignedDPS = new Array()
	function enableProviderDrop(dept)
	{
		if($('#'+dept).is(':disabled'))
		{
			$('#'+dept).prop("disabled", null);
			if(!$('#selectsalesDataProviderID').is(':checked') && !$('#selectlettingsDataProviderID').is(':checked') && !$('#selectcommercialDataProviderID').is(':checked') && !$('#selectlandAgencyDataProviderID').is(':checked') )
			{
				var assignedDPS = new Array();
			}
		}
		else
		{
			$('#'+dept).prop("disabled", "true");
			department = dept.replace('select', '');
			department = department.replace('DataProviderID', '');
			$('#select'+department+'DataProviderID').val('0');
			$('#'+department+'_branch_ref').val('');
		}
	}

	function generateBranchRef(dept)
	{

		providerID = $('#select'+dept+'DataProviderID').val();
		if ($('#'+window.dps[providerID]).val() === '')
		{
			//now get a branch ref
			$.ajax({
				url: '<?php echo site_url('ajax/setBranchRef'); ?>',
				success: function(data)
				{
					$('#'+window.dps[providerID]).val(data)
					$('#'+dept+'_branch_ref').val(data);
				},
				error: function(transport)
				{
					//error
					alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
				}

			});
		}
		else
		{
			$('#'+dept+'_branch_ref').val($('#'+window.dps[providerID]).val());
		}

	}
</script>

<div class="main_container" id="forms_page" style="padding-top:20px;" >
	<div class="row-fluid">
		<div class="widget widget-padding span12">
			<?php
			if (isset($message) AND $message != '')
			{
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">-</button>
					<?php echo $message; ?>
				</div>
				<?php
			}
			?>
			<div class="widget-header"><i class="icon-list-alt"></i><h5><?php echo $title ?></h5>
				<div style="float:right">
					<button  style="margin-top:10px; margin-right: 5px;" onClick="window.location.href='<?php echo site_url("branch/view/".$branchData['agents_id']."/".$branchData['id']); ?>'"  class="btn" id="cancel">Back</button>
				</div>
			</div>
			<form action="<?php echo site_url('staff/edit/'.$branchData['id'].'/'.$staffData['id']); ?>" method="post" id="staff_form" enctype="multipart/form-data" class="form-horizontal form-horizontal2">
				<div class="widget-body" style="min-height:330px;">
					<div class="widget-forms clearfix">
						<div class="control-group" style="height: 5px;">
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>
						<div class="control-group">

							<label class="control-label">Select Agent<span style="color: red">*</span></label>
							<div class="controls">

								<?php
								if ($branchData['agents_id'] == '0')
								{
									echo form_dropdown('selectagentid', $agents, $branchData['agents_id'], " id=\"selectagentid\" onchange=\"$('#agentID').val($(this).val());setBranch()\" ");
									?>
									<input type="hidden" value="<?php echo $branchData['agents_id']; ?>" name="agentID" id="agentID" >
									<?php
								}
								else
								{
									echo form_dropdown('selectagentid', $agents, $branchData['agents_id'], " id=\"selectagentid\"  disabled=\"disabled\"");
									?>
									<input type="hidden" value="<?php echo $branchData['agents_id']; ?>" name="agentID" id="agentID" >
									<?php
								}
								?>
								<input type="hidden" value="<?php echo $staffData['id']; ?>" name="staffID" id="staffID" >


							</div>
						</div>

						<div class="control-group">

							<label class="control-label">Select Branch<span style="color: red">*</span></label>
							<div class="controls">

								<?php
								if ($branchID == '0')
								{
									?>
									<select onchange="$('#branchID').val($(this).val());setBranchTelephoneNumbers()" disabled="disabled" id="selectbranchid" name="selectbranchid">
										<option selected="selected" value="0">Please first select an Agent</option>
									</select>
									<input type="hidden" value="<?php echo $branchID; ?>" name="branchID" id="branchID" >
									<?php
								}
								else
								{
									echo form_dropdown('selectbranchid', $branches, $branchID, " id=\"selectbranchid\" disabled=\"disabled\"");
									?>
									<input type="hidden" value="<?php echo $branchID; ?>" name="branchID" id="branchID" >
									<?php
								}
								?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Full Name<span style="color: red">*</span></label>
							<div class="controls"><input value="<?php echo $staffData['name']; ?>" type="text" name="staff_fullname" id="form_staff_fullname">
								<?php echo form_error('staff_fullname', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Staff Email<span style="color: red">*</span></label>
							<div class="controls"><input value="<?php echo $staffData['email']; ?>" type="text" name="staff_email" id="form_staff_email">
								<?php echo form_error('staff_email ', '<p class="error">'); ?>
							</div>
						</div>
						<div class="control-group " id="branchNumbers" name="branchNumbers">
							<label class="control-label">Telephone Number</label>
							<div class="controls">
								<input type="text" id="staffTelephoneNumber" name="staffTelephoneNumber" Value="<?php echo $staffData['telephone']; ?>" />
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label">Staff Type</label>
							<div class="controls">
								<table width="120px" border="0" cellpadding="2">
									<tr>
										<td align="center" valign="middle"><input type="radio" <?php if ($staffData['branch_agent_staff_type'] == '1')
								{
									echo " checked=\"checked\" ";
								} ?> name="staff_type" value="1"></td>
										<td align="left" valign="middle">Manager</td>
									</tr>
									<tr>
										<td align="center" valign="middle"><input type="radio" name="staff_type" value="0" <?php if ($staffData['branch_agent_staff_type'] == '0')
								{
									echo " checked=\"checked\" ";
								} ?>></td>
										<td align="left" valign="middle">Normal Staff</td>
									</tr>
								</table>
							</div>
						</div>
<div class="control-group ">
							<label class="control-label">Staff Photo</label>
							<div class="controls">
								<table width="120px" border="0" cellpadding="2">
									<tr>
										<td align="center" valign="middle"><input value="" type="file" name="staff_photo" id="form_staff_photo">
											<br />
											<?php echo form_error('staff_photo', '<p class="error">'); ?>
										</td>
										<td align="left" valign="middle">Current Image
											<br /><img src="<?php echo $staffData['domain_url']; ?>" width="135" height="83" alt="<?php echo $staffData['image_name']; ?>"></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="widget-footer">
					<button style="margin-left: 250px;" type="submit" name="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /Main window -->
