<!-- Add New Provider Main window -->
<script type="text/javascript" src="http://admin.propertyhuddle.com/assets/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode:"textareas",
		theme :"advanced",
		theme_advanced_toolbar_location: "top",
		theme_advanced_buttons1: "bold,italic,underline",
		theme_advanced_buttons2: "",
		theme_advanced_buttons3: "",

		plugins: "maxchars",
		max_chars: 1200,
		max_chars_indicator: "characterCounter"
	});
	var dps = '';
	$(document).ready(function() {
		$.ajax({
			url: '<?php echo site_url('ajax/returnJsonDPS'); ?>',
			success: function(data)
			{
				window.dps = jQuery.parseJSON(data);
				$.each( dps, function( key, value ) {
					underScValue = value.replace(/ /g, "_")
					$('#sales_branch_ref').append('<input type="hidden"  name="'+underScValue+'" id="'+underScValue+'" value="" /> ');
				});
			},
			error: function(transport)
			{
				//error
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});



	});
	var assignedDPS = new Array()
	function enableProviderDrop(dept)
	{
		if($('#'+dept).is(':disabled'))
		{
			$('#'+dept).prop("disabled", null);
			if(!$('#selectsalesDataProviderID').is(':checked') && !$('#selectlettingsDataProviderID').is(':checked') && !$('#selectcommercialDataProviderID').is(':checked') && !$('#selectlandAgencyDataProviderID').is(':checked') )
			{
				var assignedDPS = new Array();
			}
		}
		else
		{
			$('#'+dept).prop("disabled", "true");
			department = dept.replace('select', '');
			department = department.replace('DataProviderID', '');
			$('#select'+department+'DataProviderID').val('0');
			$('#'+department+'_branch_ref').val('');
		}
	}

	function generateBranchRef(dept)
	{

		providerID = $('#select'+dept+'DataProviderID').val();
		if ($('#'+window.dps[providerID]).val() === '')
		{
			//now get a branch ref
			$.ajax({
				url: '<?php echo site_url('ajax/setBranchRef'); ?>',
				success: function(data)
				{
					$('#'+window.dps[providerID]).val(data)
					$('#'+dept+'_branch_ref').val(data);
				},
				error: function(transport)
				{
					//error
					alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
				}

			});
		}
		else
		{
			$('#'+dept+'_branch_ref').val($('#'+window.dps[providerID]).val());
		}

	}
</script>

<div class="main_container" id="forms_page" style="padding-top:20px;" >
	<div class="row-fluid">
		<div class="widget widget-padding span12">
			<?php
			if (isset($message) AND $message != '')
			{
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">-</button>
					<?php echo $message; ?>
				</div>
				<?php
			}
			?>
			<div class="widget-header"><i class="icon-list-alt"></i><h5><?php echo $title ?></h5>
				<div style="float:right">
					<button  style="margin-top:10px; margin-right: 5px;" onClick="window.location.href='<?php echo site_url("branch"); ?>'"  class="btn" id="cancel">Back</button>
				</div>
			</div>
			<form action="<?php echo site_url('branch/add'); ?>" method="post" id="branch_form" enctype="multipart/form-data" class="form-horizontal form-horizontal2">
				<div class="widget-body" style="min-height:330px;">
					<div class="widget-forms clearfix">
						<div class="control-group" style="height: 5px;">
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>
						<div class="control-group">

							<label class="control-label">Select Agent<span style="color: red">*</span></label>
							<div class="controls">

								<?php
								if ($agentID == '0')
								{
									echo form_dropdown('selectagentid', $agents, $agentID, " onchange=\"$('#agentID').val($(this).val())\" ");
									?>
									<input type="hidden" value="" name="agentID" id="agentID" >
									<?php
								}
								else
								{
									echo form_dropdown('selectagentid', $agents, $agentID, " disabled=\"disabled\"");
									?>
									<input type="hidden" value="<?php echo $agentID; ?>" name="agentID" id="agentID" >
									<?php
								}
								?>


							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch name<span style="color: red">*</span></label>
							<div class="controls"><input value="<?php echo set_value("branch_name"); ?>" type="text" name="branch_name" id="form_branch_name">
								<?php echo form_error('branch_name', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch location<span style="color: red">*</span></label>
							<div class="controls"><input value="<?php echo set_value("branch_location"); ?>" type="text" name="branch_location" id="form_branch_location">
								<?php echo form_error('branch_location', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch postcode<span style="color: red">*</span></label>
							<div class="controls"><input value="<?php echo set_value("branch_postcode"); ?>" type="text" name="branch_postcode" id="form_branch_postcode">
								<?php echo form_error('branch_postcode ', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group " >
							<label class="control-label" >Branch references
								<br />
							</label>
							<div class="controls" >
								<div>
									<table width="800" border="0" cellpadding="2">
										<tr>
											<td><input type="checkbox" value="1" name="sales" onchange="enableProviderDrop('selectsalesDataProviderID')"> Sales </td>
											<td><?php echo form_dropdown('selectsalesDataProviderID', $dataProviders, array(), " id=\"selectsalesDataProviderID\" onchange=\"generateBranchRef('sales')\" disabled=\"true\""); ?></td>
											<td>Branch Ref <input value="" type="text" name="sales_branch_ref" id="sales_branch_ref" readonly="readonly"></td>
										</tr>
										<tr>
											<td><input type="checkbox" value="2" name="lettings" onchange="enableProviderDrop('selectlettingsDataProviderID')"> Lettings </td>
											<td><?php echo form_dropdown('selectlettingsDataProviderID', $dataProviders, array(), " id=\"selectlettingsDataProviderID\" onchange=\"generateBranchRef('lettings')\" disabled=\"true\""); ?></td>
											<td>Branch Ref <input value="" type="text" name="lettings_branch_ref" id="lettings_branch_ref" readonly="readonly"></td>
										</tr>
										<tr>
											<td><input type="checkbox" value="4" name="commercial" onchange="enableProviderDrop('selectcommercialDataProviderID')"> Commercial </td>
											<td><?php echo form_dropdown('selectcommercialDataProviderID', $dataProviders, array(), " id=\"selectcommercialDataProviderID\" onchange=\"generateBranchRef('commercial')\" disabled=\"true\""); ?></td>
											<td>Branch Ref <input value="" type="text" name="commercial_branch_ref" id="commercial_branch_ref" readonly="readonly"></td>
										</tr>
										<tr>
											<td><input type="checkbox" value="8" name="landagency" onchange="enableProviderDrop('selectlandAgencyDataProviderID')"> Land Agency</td>
											<td><?php echo form_dropdown('selectlandAgencyDataProviderID', $dataProviders, array(), " id=\"selectlandAgencyDataProviderID\" onchange=\"generateBranchRef('landAgency')\" disabled=\"true\""); ?></td>
											<td>Branch Ref <input value="" type="text" name="landAgency_branch_ref" id="landAgency_branch_ref" readonly="readonly"></td>
										</tr>
									</table>
								</div>
								<span style="font-size: 9px; width: 50px;">To create a branch reference, select a deparment and provider who will send your data and a branch ref will be created automatically</span>
							</div>
						</div>


						<div class="control-group ">
							<label class="control-label">Branch Address</label>
							<div class="controls">
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Line 1</label>
							<div class="controls"><input value="<?php echo set_value("line_one"); ?>" type="text" name="line_one" id="form_line_one">
								<?php echo form_error('line_one', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Line 2</label>
							<div class="controls"><input value="<?php echo set_value("line_two"); ?>" type="text" name="line_two" id="form_line_two">
								<?php echo form_error('line_two', '<p class="error">'); ?>
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label">Town</label>
							<div class="controls"><input value="<?php echo set_value("town"); ?>" type="text" name="town" id="form_town">
								<?php echo form_error('townr', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Post Code</label>
							<div class="controls"><input value="<?php echo set_value("postcode"); ?>" type="text" name="postcode" id="form_postcode">
								<?php echo form_error('postcoder', '<p class="error">'); ?>
							</div>
						</div>


						<div class="control-group ">
							<label class="control-label">External Feed?</label>
							<div class="controls">
								<table width="120px" border="0" cellpadding="2">
									<tr>
										<td align="center" valign="middle"><input type="radio" name="externalFeed" value="1"></td>
										<td align="left" ><span style="position:relative; top: 4px">External</span></td>
									</tr>
									<tr>
										<td align="center" valign="middle"><input type="radio" checked="checked" name="externalFeed" value="0"></td>
										<td align="left" ><span style="position:relative; top: 4px">Direct</span></td>
									</tr>
								</table>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Company registration number</label>
							<div class="controls"><input value="<?php echo set_value("company_registration_number"); ?>" type="text" name="company_registration_number" id="form_company_registration_number">
								<?php echo form_error('company_registration_number', '<p class="error">'); ?>
							</div>
						</div>


						<div class="control-group ">
							<label class="control-label">Company VAT number</label>
							<div class="controls"><input value="<?php echo set_value("company_vat_number"); ?>" type="text" name="company_vat_number" id="form_company_vat_number">
								<?php echo form_error('company_vat_number', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch website<span style="color: red">*</span></label>
							<div class="controls"><input value="<?php echo set_value("branch_website"); ?>" type="text" name="branch_website" id="form_branch_website">
								<?php echo form_error('branch_website ', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch email address<span style="color: red">*</span></label>
							<div class="controls"><input value="<?php echo set_value("branch_email_address"); ?>" type="text" name="branch_email_address" id="form_branch_email_address">
								<?php echo form_error('branch_email_address', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch sales telephone number</label>
							<div class="controls"><input value="<?php echo set_value("branch_sales_telephone_number"); ?>" type="text" name="branch_sales_telephone_number" id="form_branch_sales_telephone_number">
								<?php echo form_error('branch_sales_telephone_number', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch lettings telephone number</label>
							<div class="controls"><input value="<?php echo set_value("branch_lettings_telephone_number"); ?>" type="text" name="branch_lettings_telephone_number" id="form_branch_lettings_telephone_number">
								<?php echo form_error('branch_lettings_telephone_number', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch commercial telephone number</label>
							<div class="controls"><input value="<?php echo set_value("branch_commercial_telephone_number"); ?>" type="text" name="branch_commercial_telephone_number" id="form_branch_commercial_telephone_number">
								<?php echo form_error('branch_commercial_telephone_number', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch new homes telephone number</label>
							<div class="controls"><input value="<?php echo set_value("branch_new_homes_telephone_number"); ?>" type="text" name="branch_new_homes_telephone_number" id="form_branch_new_homes_telephone_number">
								<?php echo form_error('branch_new_homes_telephone_number', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Branch photo</label>
							<div class="controls"><input value="<?php echo set_value("branch_photo"); ?>" type="file" name="branch_photo" id="form_branch_photo">
								<?php echo form_error('branch_photo', '<p class="error">'); ?>
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label">Team photo</label>
							<div class="controls"><input value="<?php echo set_value("team_photo"); ?>" type="file" name="team_photo" id="form_team_photo">
								<?php echo form_error('team_photo', '<p class="error">'); ?>
							</div>
						</div>


						<div class="control-group ">
							<label class="control-label">Branch logo</label>
							<div class="controls"><input value="<?php echo set_value("branch_logo"); ?>" type="file" name="branch_logo" id="form_branch_logo">
								<?php echo form_error('branch_logo', '<p class="error">'); ?>
							</div>
						</div>


						<div class="control-group ">
							<label class="control-label">Branch description<span style="color: red">*</span></label>
							<div class="controls"><textarea rows="8"  style="width:500px;" name="branch_description" id="form_branch_description"><?php echo set_value("branch_description"); ?></textarea>
								<?php echo form_error('branch_description', '<p class="error">'); ?>
								<div id="characterCounter"></div>
								<div id="characterCountermsg" style="color:#FF0000;"></div>
							</div>
						</div>


					</div>
				</div>
				<div class="widget-footer">
					<button style="margin-left: 250px;" type="submit" name="submit" class="btn btn-primary">Save</button>
				</div>
				<?php echo form_close(); ?>

		</div>
	</div>
</div>
<!-- /Main window -->
