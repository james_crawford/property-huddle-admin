<!-- Main window -->
<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header"> <i class="icon-group"></i>
				<h5><?php echo $title ?></h5>
				<div class="widget-buttons"> <a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a> </div>
			</div>
			<div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Full Name</th>
							<th>Registered</th>
							<th>Email</th>
							<th>Postcode</th>
							<th>Marketing?</th>
							<th>Need to move by</th>
							<th> House to Let/Sell?</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($users as $key => $value)
						{
							?>
							<tr>
								<td><?php echo $value['title']; ?> <?php echo $value['forename']; ?> <?php echo $value['surname']; ?></td>
								<td><?php echo $value['registred_date']; ?></td>
								<td><?php echo $value['email_address']; ?></td>
								<td><?php echo $value['postcode']; ?></td>
								<td><?php
						if ($value['offers_and_news'] == '1')
						{
							echo "Yes";
						}
						else
						{
							echo "No";
						}
							?></td>
								<td><?php echo $value['need_to_move_by']; ?></td>
								<td><?php echo $value['house_to_let_sell']; ?></td>
								<td><div class="btn-group"> <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"> Action <span class="caret"></span> </a>
										<ul class="dropdown-menu pull-right">
											<li><a href="<?php echo site_url('users/edit/'.$value['id']); ?>"><i class="icon-edit"></i> Edit User</a></li>
											<li><a href="mailto:<?php echo $value['email_address']; ?>"><i class="icon-exchange"></i>Send Mail</a></li>
											<li><a href="#"  onclick="DelRecord('<?php echo site_url('users/delete/'.$value['id']); ?>');" class="demo_notify_dialog" data-type="confirm"><i class="icon-trash"></i> Delete Record</a></li>
										</ul>
									</div></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<!-- /widget-body -->
		</div>
		<!-- /widget -->
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
$agentMSG = $this->session->flashdata('agentsMSG');
$agentsMSGType = $this->session->flashdata('agentsMSGType');
if (isset($agentMSG) AND $agentMSG != '')
{
	?>
	<script>
		$(document).ready(function() {
			ShowMessage('<?php echo $agentMSG; ?>','<?php echo $agentsMSGType; ?>');
		});
	</script>
<?php } ?>
<!-- /Main window -->
