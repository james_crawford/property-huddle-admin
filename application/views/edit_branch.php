<!-- Add New Provider Main window -->
<script type="text/javascript" src="http://admin.propertyhuddle.com/assets/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode :"textareas",
		theme :"advanced",
		theme_advanced_toolbar_location: "top",
		theme_advanced_buttons1 :"bold,italic,underline",
		theme_advanced_buttons2 :"",
		theme_advanced_buttons3 :"",

		plugins: "maxchars",
		max_chars :1200,
		max_chars_indicator :"characterCounter"
	});

	var dps = '';
	$(document).ready(function() {
		$.ajax({
			url: '<?php echo site_url('ajax/returnJsonDPS'); ?>',
			success: function(data)
			{
				window.dps = jQuery.parseJSON(data);
				$.each( dps, function( key, value ) {
					underScValue = value.replace(/ /g, "_")
					$('#sales_branch_ref').append('<input type="hidden"  name="'+underScValue+'" id="'+underScValue+'" value="" /> ');
				});
			},
			error: function(transport)
			{
				//error
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});

		if($('#sales').is(':checked'))
		{
			$('#selectsalesDataProviderID').prop("disabled", null);
			$.ajax({
				url: '<?php echo site_url('ajax/getDPName/'); ?>/'+$('#selectsalesDataProviderID').val(),
				success: function(data)
				{
					$('#'+data).val($('#sales_branch_ref').val());
				},
				error: function(transport)
				{
					//error
					alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
				}
			});
		}
		if($('#lettings').is(':checked'))
		{
			$('#selectlettingsDataProviderID').prop("disabled", null);
			$.ajax({
				url: '<?php echo site_url('ajax/getDPName/'); ?>/'+$('#selectlettingsDataProviderID').val(),
				success: function(data)
				{
					$('#'+data).val($('#lettings_branch_ref').val());
				},
				error: function(transport)
				{
					//error
					alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
				}
			});
		}
		if($('#commercial').is(':checked'))
		{
			$('#selectcommercialDataProviderID').prop("disabled", null);
			$.ajax({
				url: '<?php echo site_url('ajax/getDPName/'); ?>/'+$('#selectcommercialDataProviderID').val(),
				success: function(data)
				{
					$('#'+data).val($('#commercial_branch_ref').val());
				},
				error: function(transport)
				{
					//error
					alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
				}
			});
		}
		if($('#landagency').is(':checked'))
		{
			$('#selectlandAgencyDataProviderID').prop("disabled", null);
			$.ajax({
				url: '<?php echo site_url('ajax/getDPName/'); ?>/'+$('#selectlandAgencyDataProviderID').val(),
				success: function(data)
				{
					$('#'+data).val($('#landAgency_branch_ref').val());
				},
				error: function(transport)
				{
					//error
					alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
				}
			});
		}

	});
	function enableProviderDrop(dept)
	{
		if($('#'+dept).is(':disabled'))
		{
			$('#'+dept).prop("disabled", null);
		}
		else
		{
			$('#'+dept).prop("disabled", "true");
			department = dept.replace('select', '');
			department = department.replace('DataProviderID', '');
			$('#select'+department+'DataProviderID').val('0');
			$('#'+department+'_branch_ref').val("");
			$('#'+department+'_branch_ref_hidden').val("");

		}
	}


	function generateBranchRef(dept)
	{

		providerID = $('#select'+dept+'DataProviderID').val();
		if ($('#'+window.dps[providerID]).val() === '')
		{
			//now get a branch ref
			$.ajax({
				url: '<?php echo site_url('ajax/setBranchRef'); ?>',
				success: function(data)
				{
					$('#'+window.dps[providerID]).val(data)
					$('#'+dept+'_branch_ref').val(data);
					$('#'+dept+'_branch_ref_hidden').val(data);

				},
				error: function(transport)
				{
					//error
					alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
				}

			});
		}
		else
		{
			$('#'+dept+'_branch_ref').val($('#'+window.dps[providerID]).val());
			$('#'+dept+'_branch_ref_hidden').val($('#'+window.dps[providerID]).val());

		}

	}
</script>
<div class="main_container" id="forms_page" style="padding-top:20px;" >
	<div class="row-fluid">
		<div class="widget widget-padding span12">
			<?php
			if (isset($message) AND $message != '')
			{
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $message; ?> </div>
			<?php } ?>
			<div class="widget-header"><i class="icon-sitemap"></i>
				<h5>Edit Branch</h5>
				<div style="float:right">
					<button  style="margin-top:10px; margin-right: 5px;" onClick="window.location.href='<?php echo site_url("branch/view/".$branchData['agents_id']."/".$branchData['id']); ?>'"  class="btn" id="cancel">Back</button>
				</div>
			</div>
			<form action="<?php echo site_url("branch/edit/".$agentID."/".$branchData['id']); ?>" method="post" id="branch_form" enctype="multipart/form-data" class="form-horizontal form-horizontal2">
				<div class="widget-body" style="min-height:330px;">
					<div class="widget-forms clearfix">
						<div class="control-group" style="height: 5px;">
							<div style="float:right; font-size: 11px; color: red">* Mandatory Fields</div>
						</div>
						<div class="control-group">
							<label class="control-label">Agent</label>
							<div class="controls">
								<input value="<?php echo $agentID; ?>" type="hidden" name="branch_id" id="form_branch_id" readonly="readonly">
								<input value="<?php echo $agent; ?>" type="text" name="Agent" id="form_branch_id" readonly="readonly">

								<?php echo form_error('branch_id', '<p class="error">'); ?> </div>
						</div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch name<span style="color: red">*</span></label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_name']; ?>" type="text" name="branch_name" id="form_branch_name">
							<?php echo form_error('branch_name', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch Location<span style="color: red">*</span></label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_location']; ?>" type="text" name="branch_location" id="form_branch_location">
							<?php echo form_error('branch_location', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch Location Postcode<span style="color: red">*</span></label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_postcode']; ?>" type="text" name="branch_postcode" id="form_branch_postcode">
							<?php echo form_error('branch_postcode', '<p class="error">'); ?> </div>
					</div>

					<div class="control-group " >
						<label class="control-label" >Branch references
							<br />
						</label>
						<div class="controls" >
							<div><table width="100%" border="0" cellpadding="0">
									<tr>
										<td width="15%"> <input type="checkbox" value="1" name="sales" id="sales"
											<?php
											if ($branchRefData['sales'] != '')
											{
												echo 'checked="checked"';
											}
											$salesDPID = ($branchRefData['sales'] != '' ? $branchRefData['salesDPID'] : null);
											?> onchange="enableProviderDrop('selectsalesDataProviderID')"> Sales </td>
										<td><?php echo form_dropdown('selectsalesDataProviderID', $dataProviders, $salesDPID, " id=\"selectsalesDataProviderID\" onchange=\"generateBranchRef('sales')\"  disabled=\"true\""); ?></td>
										<td>Branch Ref <input value="<?php echo $branchRefData['sales'] ?>" type="text" name="sales_branch_ref" id="sales_branch_ref" disabled="disabled"><input value="<?php echo $branchRefData['sales'] ?>" type="hidden" name="sales_branch_ref_hidden" id="sales_branch_ref_hidden" ></td>
									</tr>
									<tr>
										<td><input type="checkbox" value="2" name="lettings" id="lettings"
											<?php
											if ($branchRefData['lettings'] != '')
											{
												echo 'checked="checked"';
											}

											$lettingsDPID = ($branchRefData['lettings'] != '' ? $branchRefData['lettingsDPID'] : null);
											?> onchange="enableProviderDrop('selectlettingsDataProviderID')"> Lettings </td>
										<td> <?php echo form_dropdown('selectlettingsDataProviderID', $dataProviders, $lettingsDPID, " id=\"selectlettingsDataProviderID\" onchange=\"generateBranchRef('lettings')\"  disabled=\"true\""); ?>
										</td>
										<td>Branch Ref <input value="<?php echo $branchRefData['lettings'] ?>" type="text" name="lettings_branch_ref" id="lettings_branch_ref" disabled="disabled">
											<input value="<?php echo $branchRefData['lettings'] ?>" type="hidden" name="lettings_branch_ref_hidden" id="lettings_branch_ref_hidden"></td>
									</tr>
									<tr>
										<td><input type="checkbox" value="4" name="commercial" id="commercial"
											<?php
											if ($branchRefData['commercial'] != '')
											{
												echo 'checked="checked"';
											}
											$commercialDPID = ($branchRefData['commercial'] != '' ? $branchRefData['commercialDPID'] : null);
											?> onchange="enableProviderDrop('selectcommercialDataProviderID')"> Commercial</td>
										<td><?php echo form_dropdown('selectcommercialDataProviderID', $dataProviders, $commercialDPID, " id=\"selectcommercialDataProviderID\" onchange=\"generateBranchRef('commercial')\"  disabled=\"true\""); ?>
										</td>
										<td>Branch Ref <input value="<?php echo $branchRefData['commercial'] ?>" type="text" name="commercial_branch_ref" id="commercial_branch_ref" disabled="disabled">
											<input value="<?php echo $branchRefData['commercial'] ?>" type="hidden" name="commercial_branch_ref_hidden" id="commercial_branch_ref_hidden" ></td>
									</tr>
									<tr>
										<td><input type="checkbox" value="8" name="landagency" id="landagency"
											<?php
											if ($branchRefData['landAgency'] != '')
											{
												echo 'checked="checked"';
											}
											$landAgencyDPID = ($branchRefData['landAgency'] != '' ? $branchRefData['landAgencyDPID'] : null);
											?> onchange="enableProviderDrop('selectlandAgencyDataProviderID')"> Land Agency</td>
										<td><?php echo form_dropdown('selectlandAgencyDataProviderID', $dataProviders, $landAgencyDPID, " id=\"selectlandAgencyDataProviderID\" onchange=\"generateBranchRef('landAgency')\"  disabled=\"true\""); ?>
										</td>
										<td>Branch Ref <input value="<?php echo $branchRefData['landAgency'] ?>" type="text" name="landAgency_branch_ref" id="landAgency_branch_ref" disabled="disabled">
											<input value="<?php echo $branchRefData['landAgency'] ?>" type="hidden" name="landAgency_branch_ref_hidden" id="landAgency_branch_ref_hidden" ></td>
									</tr>
								</table></div>
							<span style="font-size: 9px; width: 50px;">To edit a branch reference, select a deparment and provider who will send your data and a branch ref will be created automatically</span>
						</div>
					</div>



					<div class="control-group ">
						<label class="control-label">Branch Address</label>
						<div class="controls">
						</div>
					</div>

					<div class="control-group ">
						<label class="control-label">Line 1</label>
						<div class="controls"><input value="<?php echo $branchData['line_one']; ?>" type="text" name="line_one" id="form_line_one">
							<?php echo form_error('line_one', '<p class="error">'); ?>
						</div>
					</div>

					<div class="control-group ">
						<label class="control-label">Line 2</label>
						<div class="controls"><input value="<?php echo $branchData['line_two']; ?>" type="text" name="line_two" id="form_line_two">
							<?php echo form_error('line_two', '<p class="error">'); ?>
						</div>
					</div>
					<div class="control-group ">
						<label class="control-label">Town</label>
						<div class="controls"><input value="<?php echo $branchData['town']; ?>" type="text" name="town" id="form_town">
							<?php echo form_error('town', '<p class="error">'); ?>
						</div>
					</div>

					<div class="control-group ">
						<label class="control-label">Post Code</label>
						<div class="controls"><input value="<?php echo $branchData['postcode']; ?>" type="text" name="postcode" id="form_postcode">
							<?php echo form_error('postcode', '<p class="error">'); ?>
						</div>
					</div>

					<div class="control-group ">
						<label class="control-label">External Feed?</label>
						<div class="controls">
							<table width="120px" border="0" cellpadding="2">
								<tr>
									<td align="center" valign="middle"><input type="radio" name="externalFeed" <?php if ($branchData['external_feed'] == '1')
							{
								echo ' checked="checked" ';
							} ?>  value="1"></td>
									<td align="left" ><span style="position:relative; top: 4px">External</span></td>
								</tr>
								<tr>
									<td align="center" valign="middle"><input type="radio" <?php if ($branchData['external_feed'] == '0')
							{
								echo ' checked="checked" ';
							} ?> name="externalFeed" value="0"></td>
									<td align="left" ><span style="position:relative; top: 4px">Direct</span></td>
								</tr>
							</table>
						</div>
					</div>


					<div class="control-group ">
						<label class="control-label">Company registration number</label>
						<div class="controls">
							<input value="<?php echo $branchData['company_registration_number']; ?>" type="text" name="company_registration_number" id="form_company_registration_number">
							<?php echo form_error('company_registration_number', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Company VAT number</label>
						<div class="controls">
							<input value="<?php echo $branchData['company_vat_number']; ?>" type="text" name="company_vat_number" id="form_company_vat_number">
							<?php echo form_error('company_vat_number', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch website</label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_website']; ?>" type="text" name="branch_website" id="form_branch_website">
							<?php echo form_error('branch_website', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch email address</label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_email_address']; ?>" type="text" name="branch_email_address" id="form_branch_email_address">
							<?php echo form_error('branch_email_address', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch sales telephone number</label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_sales_telephone_number']; ?>" type="text" name="branch_sales_telephone_number" id="form_branch_sales_telephone_number">
							<?php echo form_error('branch_sales_telephone_number', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch lettings telephone number</label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_lettings_telephone_number']; ?>" type="text" name="branch_lettings_telephone_number" id="form_branch_lettings_telephone_number">
							<?php echo form_error('branch_lettings_telephone_number', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch commercial telephone number</label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_commercial_telephone_number']; ?>" type="text" name="branch_commercial_telephone_number" id="form_branch_commercial_telephone_number">
							<?php echo form_error('branch_commercial_telephone_number', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch new homes telephone number</label>
						<div class="controls">
							<input value="<?php echo $branchData['branch_new_homes_telephone_number']; ?>" type="text" name="branch_new_homes_telephone_number" id="form_branch_new_homes_telephone_number">
<?php echo form_error('branch_new_homes_telephone_number', '<p class="error">'); ?> </div>
					</div>

					<div class="control-group ">
						<label class="control-label">Manager Contact Name:</label>
						<div class="controls" ><?php echo form_dropdown('manager_staff_id', $staff, $branchData['manager_staff_id']); ?></div>
					</div>
					<div class="control-group ">
						<label class="control-label" >Trade Contact:</label>
						<div class="controls" ><?php echo form_dropdown('trade_staff_id', $staff, $branchData['trade_staff_id']); ?></div>
					</div>


					<div class="control-group ">
						<label class="control-label" >Billing Contact Name:</label>
						<div class="controls" ><?php echo form_dropdown('billing_staff_id', $staff, $branchData['billing_staff_id']); ?></div>
					</div>
					<div class="control-group ">

						<label class="control-label">Branch photo:</label>
						<div class="controls">
							<input  type="file" name="branch_photo" id="form_branch_photo">
							<?php
							if ($branchData['branch']['branch_image'] != '')
							{
								?>
								<img align="top" class="branchimg" src="<?php echo $branchData['branch']['branch_image']; ?>" border="0" title="" height="50">
							<?php } ?>
							<?php echo form_error('branch_photo', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Team photo:</label>
						<div class="controls">
							<input type="file" name="team_photo" id="form_team_photo">
							<?php
							if ($branchData['branch']['team_image'] != '0')
							{
								?>
								<img align="top" class="branchimg" src="<?php echo $branchData['branch']['team_image']; ?>" border="0" title="" height="50">
							<?php } ?>
							<?php echo form_error('team_photo', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch logo:</label>
						<div class="controls">
							<input type="file" name="branch_logo" id="form_branch_logo">
							<?php
							if ($branchData['branch']['branch_logo'] != '0')
							{
								?>
								<img align="top" class="branchimg" src="<?php echo $branchData['branch']['branch_logo']; ?>" border="0" title="" height="50">
							<?php } ?>
							<?php echo form_error('branch_logo', '<p class="error">'); ?> </div>
					</div>
					<div class="control-group ">
						<label class="control-label">Branch description</label>
						<div class="controls">
							<textarea rows="5" name="branch_description" id="form_branch_description" style=" width:500px;"><?php echo $branchData['branch_description']; ?></textarea>
<?php echo form_error('branch_description', '<p class="error">'); ?>
							<div id="characterCounter"></div>
							<div id="characterCountermsg" style="color:#FF0000;"></div>
						</div>

					</div>
				</div>
		</div>
		<div class="widget-footer">
			<button style="margin-left: 250px;" type="submit" name="submit" class="btn btn-primary">Save</button>
		</div>
<?php echo form_close(); ?>
	</div>
</div>
</div>
<!-- /Main window -->
