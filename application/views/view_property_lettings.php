<!-- Main window -->
<div class="main_container" id="forms_page" style="padding-top:20px; margin-bottom: -35px;">
	<div class="row-fluid" >
		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-home"></i><h5><?php echo $title ?></h5>
                <div class="widget-buttons">
                    <button  onClick="window.location.href='<?php echo site_url("properties"); ?>'"  class="btn" id="cancel">Back</button>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>
			<div class="widget-body">
                <div class="widget-forms clearfix" >
                    <table width="100%" cellpadding="2" cellspaceing="2">
						<tr>
							<td width="40%" valign="top">
								<table cellpadding="2" cellspaceing="2">
									<tr>
										<th style="text-align: left"  valign="top">Address</th>
										<td style="width: 20px;">:</td>
										<td valign="top"><?php echo $propertyData['address_1']." ".$propertyData['address_2'] ?></td>
									</tr>

									<?php if( $propertyData['address_3'] != '' && $propertyData['address_3'] != ' ')
									{ 
									?>
									<tr>
										<th style="text-align: left"  valign="top"></th>
										<td ></td>
										<td><?php echo $propertyData['address_3']; ?></td>
									</tr>
									<?php
									}
									?>
									<?php if( $propertyData['address_4'] != '' && $propertyData['address_4'] != ' ')
									{ 
									?>
									<tr>
										<th style="text-align: left"  valign="top"></th>
										<td ></td>
										<td><?php echo $propertyData['address_4']; ?></td>
									</tr>
									<?php
									}
									?>
									
									<tr>
										<th style="text-align: left"  valign="top"></th>
										<td style="width: 20px;"></td>
										<td><?php echo $propertyData['town'] ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Post Code</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['postcode_1']." ".$propertyData['postcode_2'] ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Price </th>
										<td style="width: 20px;">:</td>
										<td>&pound;<?php echo number_format($propertyData['price'], 0); ?></td>
									</tr>

									<tr>
										<th style="text-align: left"  valign="top">Bedrooms</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['bedrooms'] ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">No. Bathrooms</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['bathrooms']; ?></td>
									</tr>

									<tr>
										<th style="text-align: left"  valign="top">No. Living rooms</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['living_rooms']; ?></td>
									</tr>


									<tr>
										<th style="text-align: left"  valign="top">Inc. Washing Machine</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['let_washing_machine']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left"  valign="top">Inc. Dishwasher</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['let_dishwasher']]; ?></td>
									</tr>
								</table>
							</td>
							<td width="40%" valign="top">
								<table cellpadding="2" cellspaceing="2">
									<tr>
										<th style="text-align: left"  valign="top">Inc. Gas</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['inc_gas']]; ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Inc. Electric</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['inc_electric']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left"  valign="top">Inc. TV Licence</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['inc_tv_licence']]; ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Inc. TV/Satellite</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['inc_tv_subscription']]; ?></td>
									</tr>

									<tr>
										<th style="text-align: left"  valign="top">Inc. Internet</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $yesNo[$propertyData['inc_internet']]; ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Status</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $status[$propertyData['status_id']]; ?></td>
									</tr>


									<tr>
										<th style="text-align: left"  valign="top">Bond</th>
										<td style="width: 20px;">:</td>
										<td><?php echo "&pound;".number_format($propertyData['let_bond'], 0); ?></td>
									</tr>

									<tr>
										<th style="text-align: left"  valign="top">Availability</th>
										<td style="width: 20px;">:</td>
										<td>
										<?php
										if ($propertyData['let_date_available'] != '0000-00-00 00:00:00')
										{
											echo date("d/m/Y", strtotime($propertyData['let_date_available']));
										}
										else
										{
											echo 'Now';
										}
										?>
										</td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Let Contract</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $propertyData['let_contract_in_months']; ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Rent Frequency</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $letFrequency[$propertyData['let_rent_frequency']]; ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Letttings Type</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $letType[$propertyData['let_type_id']]; ?></td>
									</tr>
									<tr>
										<th style="text-align: left"  valign="top">Furnish Type</th>
										<td style="width: 20px;">:</td>
										<td><?php echo $letFurnishType[$propertyData['let_furnish_type']]; ?></td>
									</tr>
								</table>
							</td>
							<td width="40%" valign="top">
								<img src="<?php echo $propertyData['domain_url']; ?>" width="135" height="83" alt="<?php echo $propertyData['image_name']; ?>">
							</td>
						</tr>
						<tr>
							<th colspan="3" width="600px" style="text-align: left"  valign="top">Summary</th>
						</tr>
						<tr>
							<td colspan="3"  ><?php echo $propertyData['summary'] ?></td>
						</tr>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>
<div class="main_container" id="users_page">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h5>Property Timeline</h5>
				<div class="widget-buttons">
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<?php
				if (!empty($archiveData))
				{
					?>
					<table id="users" class="table table-striped table-bordered dataTable">
						<thead>
							<tr>
								<th>Address</th>
								<th>Beds</th>
								<th>Price</th>
								<th>Status</th>
								<th>Bathrms</th>
								<th>Living Rooms</th>
								<th>Bond</th>
								<th>Avail.</th>
								<th>Contract Len. Months</th>
								<th>Inc. Washing machine</th>
								<th>Inc. Dishwasher</th>
								<th>Inc. Gas</th>
								<th>Inc. Electric</th>
								<th>Inc. TV Licence</th>
								<th>Inc. Cable/ Satelite</th>
								<th>Inc. Internet</th>
								<th>Rent Freq.</th>
								<th>Let Type</th>
								<th>Furn. Type</th>
								<th>Prop. Type</th>
								<th>Pub.</th>
								<th>Active</th>
								<th>Date Archived</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php
								$address = $propertyData['address_1']." ";
								if($propertyData['address_2'] != '' && $propertyData['address_2'] != ' ' && $propertyData['address_2'] != null)
								{
									$address .= " ".$propertyData['address_2'].", ";
								}
								if($propertyData['address_3'] != '' && $propertyData['address_3'] != ' ' && $propertyData['address_3'] != null)
								{
									$address .= $propertyData['address_3'].", ";
								}
								if($propertyData['address_4'] != '' && $propertyData['address_4'] != ' ' && $propertyData['address_4'] != null)
								{
									$address .= $propertyData['address_4'].", ";
								}
										$address .= " ".$propertyData['town'].", ";
										$address .= $propertyData['postcode_1'];
										$address .= " ".$propertyData['postcode_2']; 
									echo $address;
								?></td>
								<td><?php echo $propertyData['bedrooms']; ?></td>
								<td><?php echo "&pound".number_format($propertyData['price'], 0); ?></td>
								<td><?php echo $status[$propertyData['status_id']]; ?></td>
								<td><?php echo $propertyData['bathrooms']; ?></td>
								<td><?php echo $propertyData['living_rooms']; ?></td>
								<td><?php echo "&pound".number_format($propertyData['let_bond'], 0); ?></td>
								<td><?php
									if ($propertyData['let_date_available'] != '0000-00-00 00:00:00')
									{
										echo date("d/m/Y", strtotime($propertyData['let_date_available']));
									}
									else
									{
										echo 'Now';
									}
									?>
								</td>
								<td><?php echo $propertyData['let_contract_in_months']; ?></td>
								<td><?php echo $yesNo[$propertyData['let_washing_machine']]; ?></td>
								<td><?php echo $yesNo[$propertyData['let_dishwasher']]; ?></td>
								<td><?php echo $yesNo[$propertyData['inc_gas']]; ?></td>
								<td><?php echo $yesNo[$propertyData['inc_electric']]; ?></td>
								<td><?php echo $yesNo[$propertyData['inc_tv_licence']]; ?></td>
								<td><?php echo $yesNo[$propertyData['inc_tv_subscription']]; ?></td>
								<td><?php echo $yesNo[$propertyData['inc_internet']]; ?></td>
								<td><?php echo $letFrequency[$propertyData['let_rent_frequency']]; ?></td>
								<td><?php echo $letType[$propertyData['let_type_id']]; ?></td>
								<td><?php echo $propSubType[$propertyData['property_sub_type_id']]; ?></td>
								<td><?php echo $letFurnishType[$propertyData['let_furnish_type']]; ?></td>
								<td><?php echo $yesNo[$propertyData['published']]; ?></td>
								<td><?php echo $yesNo[$propertyData['active']]; ?></td>
								<td>Current</td>
							</tr>
							<?php
							foreach ($archiveData as $archive => $record)
							{
								?>
								<tr>
									<td><?php
								$address = $record['address_1']." ";
								if($record['address_2'] != '' && $record['address_2'] != ' ')
								{
									$address .= " ".$propertyData['address_2'].", ";
								}
								if($record['address_3'] != '' && $record['address_3'] != ' ')
								{
									$address .= $record['address_3'].", ";
								}
								if($record['address_4'] != '' && $record['address_4'] != ' ')
								{
									$address .= $record['address_4'].", ";
								}
										$address .= " ".$record['town'].", ";
										$address .= $record['postcode_1'];
										$address .= " ".$record['postcode_2']; 
									echo $address;
								?></td>
									<td><?php echo $record['bedrooms']; ?></td>
									<td><?php echo "&pound".number_format($record['price'], 0); ?></td>
									<td><?php echo $status[$record['status_id']]; ?></td>
									<td><?php echo $record['bathrooms']; ?></td>
									<td><?php echo $record['living_rooms']; ?></td>
									<td><?php echo "&pound".number_format($record['let_bond'], 0); ?></td>
									<td><?php
						if ($record['let_date_available'] != '0000-00-00 00:00:00')
						{
							echo date("d/m/Y", strtotime($record['let_date_available']));
						}
						else
						{
							echo 'Now';
						}
								?>
									</td>

									<td><?php echo $record['let_contract_in_months']; ?></td>
									<td><?php echo $yesNo[$record['let_washing_machine']]; ?></td>
									<td><?php echo $yesNo[$record['let_dishwasher']]; ?></td>
									<td><?php echo $yesNo[$record['inc_gas']]; ?></td>
									<td><?php echo $yesNo[$record['inc_electric']]; ?></td>
									<td><?php echo $yesNo[$record['inc_tv_licence']]; ?></td>
									<td><?php echo $yesNo[$record['inc_tv_subscription']]; ?></td>
									<td><?php echo $yesNo[$record['inc_internet']]; ?></td>
									<td><?php echo $letFrequency[$record['let_rent_frequency']]; ?></td>
									<td><?php echo $letType[$record['let_type_id']]; ?></td>
									<td><?php echo $propSubType[$record['property_sub_type_id']]; ?></td>
									<td><?php echo $letFurnishType[$record['let_furnish_type']]; ?></td>
									<td><?php echo $yesNo[$record['published']]; ?></td>
									<td><?php echo $yesNo[$propertyData['active']]; ?></td>
								<td><?php echo date("d/m/Y", strtotime($record['date_archived']))?></td>
								</tr>
							<?php }
							?>
						</tbody>
					</table>
					<?php
				}
				?>
            </div>
		</div>
	</div>
</div>
<script>
	function setDPVal(dpID) {
		$("#deactivatedStaffID").val(dpID);
		$("#activatedStaffID").val(dpID);

	}

	function activateStaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activateStaffStatus'); ?>/'+ $("#activatedStaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('branch/view/'.$branchData['agents_id'].'/'.$branchData['id'].'/'); ?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});


	}
	function deactivateStaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivateStaffStatus'); ?>/'+ $("#deactivatedStaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('branch/view/'.$branchData['agents_id'].'/'.$branchData['id'].'/'); ?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		;
	}



</script>

<!-- Agent Brach's start  -->
<!-- /Main window -->

