<!-- Main window -->

<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header"> <i class="icon-group"></i>
				<h5><?php echo $title ?></h5>
				<div class="widget-buttons"> <a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a> </div>
			</div>
			<div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Keyword Text</th>
							<th>Keyword Type</th>
							<th>Time Date</th>
							<th>User IP address</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($keywords as $key => $value)
						{ ?>
							<tr>
								<td><?php echo $value['KeywordText']; ?></td>
								<td><?php echo $value['KeywordType']; ?></td>
								<td><?php echo $value['TimeDate']; ?></td>
								<td><?php echo $value['ipaddress']; ?></td>
								<td>
									<div class="btn-group"> <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"> Action <span class="caret"></span> </a>
										<ul class="dropdown-menu pull-right">
											<li><a href="#"  onclick="DelRecord('<?php echo site_url('keywords/delete/'.$value['id']); ?>');" class="demo_notify_dialog" data-type="confirm"><i class="icon-trash"></i> Delete Record</a></li>
										</ul>
									</div>
								</td>
							</tr>
<?php } ?>
					</tbody>
				</table>
			</div>
			<!-- /widget-body -->
		</div>
		<!-- /widget -->
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
$KeywordMSG = $this->session->flashdata('KeywordMSG');
$KeywordMSGType = $this->session->flashdata('KeywordMSGType');
if (isset($KeywordMSG) AND $KeywordMSG != '')
{
	?>
	<script>
		$(document).ready(function() {
			ShowMessage('<?php echo $KeywordMSG; ?>','<?php echo $KeywordMSGType; ?>');
		});
	</script>
<?php } ?>
<!-- /Main window -->
