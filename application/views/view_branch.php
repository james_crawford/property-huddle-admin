<!-- Main window -->
<div class="main_container" id="forms_page" style="padding-top:20px; margin-bottom: -35px;">
	<div class="row-fluid" >
		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-list-alt"></i><h5><?php echo $title ?></h5>
                <div class="widget-buttons">
                    <button  onClick="window.location.href='<?php echo site_url("agents/view/".$branchData['agents_id']); ?>'"  class="btn" id="cancel">Back</button>
                    <button  onClick="window.location.href='<?php echo site_url("branch/edit/".$branchData['agents_id']."/".$branchData['id']); ?>'"  class="btn" id="cancel">Edit</button>
                    <a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>
			<div class="widget-body">
                <div class="widget-forms clearfix" >
                    <table width="100%" cellpadding="2" cellspaceing="2">
                        <tr>
                            <th style="width: 250px; text-align: left">Agent Name</th>
                            <td style="width: 20px;">:</td>
                            <td><?php echo $branchData['agent_name'] ?></td>
                        </tr>
                        
                        <tr>
                            <th style="text-align: left">Branch Name</th>
                            <td>:</td>
                            <td><?php echo $branchData['branch_name'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Branch Website</th>
                            <td>:</td>
                            <td><?php echo $branchData['branch_website'] ?></td>
                        </tr>
						
                        <tr>
                            <th style="text-align: left">Sales Phone Number</th>
                            <td>:</td>
                            <td><?php echo $branchData['branch_sales_telephone_number']?></td>
                        </tr>
						<tr>
                            <th style="text-align: left">Lettings Phone Number</th>
                            <td>:</td>
                            <td><?php echo $branchData['branch_lettings_telephone_number']?></td>
                        </tr>
						<tr>
                            <th style="text-align: left">Commercial Phone Number</th>
                            <td>:</td>
                            <td><?php echo $branchData['branch_commercial_telephone_number']?></td>
                        </tr>
						
						
                        <tr>
                            <th style="text-align: left">New Homes Phone Number</th>
                           <td>:</td>
                            <td><?php echo $branchData['branch_new_homes_telephone_number']?></td>
                        </tr>
                   
                        <tr>
                            <th style="text-align: left">Address</th>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Line 1</th>
                            <td>:</td>
                            <td><?php echo $branchData['line_one'] ?></td>
                        </tr>
                         <tr>
                            <th style="text-align: left">Line 2</th>
                            <td>:</td>
                            <td><?php echo $branchData['line_two'] ?></td>
                        </tr>
                         <tr>
                            <th style="text-align: left">Town</th>
                            <td>:</td>
                            <td><?php echo $branchData['town'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Post Code</th>
                            <td>:</td>
                            <td><?php echo $branchData['postcode']?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Status</th>
                            <td>:</td>
                            <td>
								<?php
								if ($branchData['active'] == '1')
								{
									?>
									<span class="label label-success">Active</span>
									<?php
								}
								else
								{
									?>
									<span class="label">Inactive</span>
								<?php } ?>
                            </td>
                        </tr>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>


<!-- Agent Brach's start  -->
<div class="main_container" id="users_page">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header">
				<i class="icon-group"></i>
				<h5>Branch Staff</h5>
				<div class="widget-buttons">
					<!--<a href="" data-title="Add User" data-toggle="modal" data-target="#example_modal"><i class="icon-plus"></i></a>-->
					<a href="<?php echo site_url('staff/add/'.$branchData['id']); ?>" data-title="Add User" data-toggle="" data-target=""><i class="icon-plus"></i></a>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
			<?php
			if(!empty($staff))
				{
				?>
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Staff Name</th>
							<th>Email</th>
							<th>Telephone</th>
							<th>Image</th>
							<th>Status</th>
							<th>Action</th>
							
							
						</tr>
					</thead>
					<tbody>
<?php
						foreach ($staff as $key => $value)
						{
							?>
							<tr>
								<td><?php echo $value['name']; ?></td>
								<td><?php echo $value['email']; ?></td>
								<td><?php echo $value['telephone']; ?></td>
								<td><img src="<?php echo $value['domain_url']; ?>" width="135" height="83" alt="<?php echo $value['image_name']; ?>">
									</td>
									<td> 
									<?php
									if ($value['active'] == '1')
									{
										?>
										<a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalDeactivateStaffBox" class="demo_notify_dialog" data-type="confirm"><span class="label label-success">Active</span></a>
										<?php
									}
									else
									{
										?>
										<a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalActivateStaffBox" class="demo_notify_dialog" data-type="confirm"><span class="label">Inactive</span></a>
										<?php
									}
									?>
								</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
											Action
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu pull-right">
											<li><a href="<?php echo site_url('staff/edit/'.$branchData['id'].'/'.$value['id']); ?>"><i class="icon-folder-open-alt"></i>View</a></li>
											<li><a href="<?php echo site_url('staff/edit/'.$branchData['id'].'/'.$value['id']); ?>"><i class="icon-edit"></i>Edit</a></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php } 
				?>

				</tbody>
				</table>
					<?php
				}
			?>
				
				
            </div> <!-- /widget-body -->
		</div> <!-- /widget -->
	</div>
</div>
<script>
	function setDPVal(dpID) {
		$("#deactivatedStaffID").val(dpID);
		$("#activatedStaffID").val(dpID);
		
	}
	
	function activateStaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activateStaffStatus') ; ?>/'+ $("#activatedStaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('branch/view/'.$branchData['agents_id'].'/'.$branchData['id'].'/');?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		

	}
	function deactivateStaffStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivateStaffStatus') ; ?>/'+ $("#deactivatedStaffID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('branch/view/'.$branchData['agents_id'].'/'.$branchData['id'].'/');?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
			;
}
	
	
	
</script>

<!-- Agent Brach's start  -->
<!-- /Main window -->

