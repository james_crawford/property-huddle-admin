<!-- Main window -->

<div class="main_container" id="users_page" style="padding-top:20px;">
	<div class="row-fluid">
		<?php
		$message = $this->session->flashdata('Success');
		if (isset($message) AND $message != '')
		{
			?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">x</button>
				<?php echo $message; ?>
			</div><?php } ?>


		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header">
				<i class="icon-group"></i>
				<h5>Data Providers</h5>
				<div class="widget-buttons">
					<a href="<?php echo site_url('dataprovider/add'); ?>" data-title="Add User" data-toggle="" data-target=""><i class="icon-plus"></i></a>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Data Provider Name</th>
							<th>Provider Reference</th>
							<th>Data URL</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						<?php
						//for($i=0; $i<=count($dataRecords); $i++); {

						foreach ($dataRecords as $key => $value)
						{
							?>
							<tr>
								<td><?php echo $value['name']; ?></td>
								<td><?php echo $value['data_provider_ref']; ?></td>
								<td><?php echo $value['store_url']; ?></td>
								<!--<span class="label label-success"></span>-->
								<td> 
									<?php
									if ($value['active'] == '1')
									{
										?>
										<a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalDeactivateBox" class="demo_notify_dialog" data-type="confirm"><span class="label label-success">Active</span></a>
										<?php
									}
									else
									{
										?>
										<a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalActivateBox" class="demo_notify_dialog" data-type="confirm"><span class="label">Inactive</span></a>
										<?php
									}
									?>
								</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
											Action
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu pull-right">
										  <!--<li><a href="#"><i class="icon-envelope"></i> Email</a></li>-->
											<li><a href="<?php echo site_url('dataprovider/edit/'.$value['id']); ?>"><i class="icon-edit"></i> Edit</a></li>
											<?php
											if ($value['active'] == '1')
											{
												?>
												<li><a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalDeactivateBox" class="demo_notify_dialog" data-type="confirm"><i class="icon-exchange"></i> Change Status</a></li> <!-- @TODO set dynamic active/deactive button -->
												<?php
											}
											else
											{
												?>
												<li><a href="#" onclick="setDPVal(<?php echo $value['id'] ?>)" data-toggle="modal" data-target="#modalActivateBox" class="demo_notify_dialog" data-type="confirm"><i class="icon-exchange"></i> Change Status</a></li> <!-- @TODO set dynamic active/deactive button -->
												<?php
											}
											?>
										</ul>
									</div>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
            </div> <!-- /widget-body -->
		</div> <!-- /widget -->
	</div>
</div>
<!-- /Main window -->
<!--- For Show Sucess Message at footer-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
$DataproviderMSG = $this->session->flashdata('DataprovidertsMSG');
$DataproviderMSGType = $this->session->flashdata('DataproviderMSGType');
if (isset($DataproviderMSG) AND $DataproviderMSG != '')
{
	?>
	<script>
		$(document).ready(function() {
			ShowMessage('<?php echo $DataproviderMSG; ?>','<?php echo $DataproviderMSGType; ?>');
		});
	</script>
<?php } ?>
<script>
	function setDPVal(dpID) {
		$("#deactivatedDpID").val(dpID);
		$("#activatedDpID").val(dpID);
		
	}
	
	function activateDPStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activateDataProviderStatus') ; ?>/'+ $("#activatedDpID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('dataprovider');?>');
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		

	}
	function deactivateDPStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivateDataProviderStatus') ; ?>/'+ $("#deactivatedDpID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('dataprovider');?>')
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
			;
}
	
	
	
</script>

<!-- Add New Data Provider Popup Form modal Box -->
<?php /* ?><div id="example_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:auto;">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
  <h3 id="myModalLabel">Add Data Provider</h3>
  </div>
  <div class="modal-body">
  <?php echo form_open("dataprovider/add");?>
  <p><!-- Load a Form in Popup Box-->
  <?php $this->load->view('add_dataproviders');?>
  </p>
  </div>
  <div class="modal-footer">
  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  <button class="btn btn-primary">Save changes</button>
  </div>
  <?php echo form_close();?>
  </div><?php */ ?>





