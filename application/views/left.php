<!-- Side menu -->
<?php
$curpage = $this->uri->segment(1);
$curpage1 = $this->uri->segment(2);
?>
<div class="sidebar-nav nav-collapse collapse">
	<div class="user_side clearfix">
		<img src="<?php echo base_url() ?>assets/img/og.jpg" alt="Oliver Gleave">
		<h5><?php echo $AdminInfo[0]['Name']; ?></h5>
		<a href="<?php echo site_url("profile"); ?>"><i class="icon-cog"></i> Settings</a>
	</div>
	<div class="accordion" id="accordion2">
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_F79999 <?php echo ($curpage == 'dashboard') ? 'active' : '' ?>" href="<?php echo site_url("dashboard"); ?>"><i class="icon-dashboard"></i> <span>Dashboard</span></a>
			</div>
		</div>

		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_C3F7A7 <?php echo ($curpage == 'dataprovider') ? 'active' : '' ?>" href="<?php echo site_url("dataprovider"); ?>"><i class="icon-inbox"></i> <span>Data Providers</span></a>
			</div>
		</div>

		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_9FDDF6 <?php echo ($curpage == 'agents') ? 'active' : '' ?>" href="<?php echo site_url("agents"); ?>"><i class="icon-user"></i> <span>Agents</span></a>
			</div>
		</div>

		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_F6F1A2 <?php echo ($curpage == 'branch') ? 'active' : '' ?>" href="<?php echo site_url("branch"); ?>"><i class="icon-sitemap"></i> <span>Branches</span></a>
			</div>
		</div>
		
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_F6F1A2 <?php echo ($curpage == 'properties') ? 'active' : '' ?>" href="<?php echo site_url("properties"); ?>"><i class="icon-home"></i> <span>Properties</span></a>
			</div>
		</div>


		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_C3F7A7 <?php echo ($curpage == 'users') ? 'active' : '' ?>" href="<?php echo site_url("users"); ?>"><i class="icon-user"></i> <span>Users</span></a>
			</div>
		</div>

		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_C3F7A7 <?php echo ($curpage == 'keywords') ? 'active' : '' ?>" href="<?php echo site_url("keywords"); ?>"><i class="icon-bar-chart"></i> <span>Keywords</span></a>
			</div>
		</div>
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_C3F7A7 <?php //echo ($curpage=='users')?'active':''  ?>" href="<?php echo site_url("dataengine"); ?>"><i class="icon-user"></i> <span>Data Engine</span></a>
			</div>
		</div>
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_C1F8A9" href="<?php echo site_url("dashboard"); ?>"><i class="icon-bar-chart"></i> <span>Statistics</span></a>
			</div>
		</div>
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle b_9FDDF6" href="http://support.jupix.co.uk/staff/index.php" target="_blank" ><i class="icon-bullhorn"></i> <span>Support Tickets</span></a>
			</div>
		</div>

	</div>
</div>
<!-- /Side menu -->