<!-- Add New Provider Main window -->
<div class="main_container" id="users_page" style="padding-top:20px;" >
	<div class="row-fluid">
		<font color="#FF0000"><?php echo $this->session->flashdata('ProviderIdError'); ?></font>
		<?php echo form_open("dataprovider/edit/".$this->uri->segment(3)); ?>

		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-list-alt"></i><h5>Edit Data Provider</h5>
				<div style="float:right">
					<a  style="margin-top:10px; margin-right: 5px;" href="<?php echo site_url("dataprovider"); ?>" class="btn" id="cancel">Back</a>
				</div>
			
			</div>

			<div id="register_form1" class="form-horizontal">
				<div class="widget-body">
					<div class="widget-forms clearfix ">

						<div class="control-group ">
							<label class="control-label">Data Provider Name</label>
							<div class="controls">
								<input type="text" name="name" id="name" value="<?php echo $dataRecords['name']; ?>"/>
								<?php echo form_error('name', '<p class="error1">'); ?> </div>
						</div>
						<div class="control-group ">
							<label class="control-label">Data Provider Email</label>
							<div class="controls">
								<input type="text" name="email" id="email" value="<?php echo $dataRecords['email']; ?>">
								<?php echo form_error('email', '<p class="error1">'); ?></div>
						</div>
						<div class="control-group ">
							<label class="control-label">Website URL</label>
							<div class="controls">
								<input type="text" name="website_url" id="website_url" value="<?php echo $dataRecords['website_url']; ?>">
								<?php echo form_error('website_url', '<p class="error1">'); ?></div>
						</div>
						<div class="control-group ">
							<label class="control-label">Contact Name</label>
							<div class="controls">
								<input type="text" name="contact_name" id="contact_name" value="<?php echo $dataRecords['contact_name']; ?>">
								<?php echo form_error('contact_name', '<p class="error1">'); ?></div>
						</div>
						<div class="control-group ">
							<label class="control-label">Contact Email</label>
							<div class="controls">
								<input type="text" name="contact_email" id="contact_email" value="<?php echo $dataRecords['contact_email']; ?>">
								<?php echo form_error('contact_email', '<p class="error1">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label">Phone Number</label>
							<div class="controls">
								<input type="text" name="contact_phone" id="contact_phone" value="<?php echo $dataRecords['contact_phone']; ?>">
								<?php echo form_error('contact_phone', '<p class="error1">'); ?></div>
							<input type="hidden" name="date_created" id="date_created" value="<?php //echo date();  ?>"/>
						</div>
						<div class="control-group ">
							<label class="control-label">Data Provider Address</label>
							<div class="controls">
								<textarea type="text" name="address" id="address" rows="8"><?php echo $dataRecords['address']; ?> </textarea>
								<?php echo form_error('address', '<p class="error1">'); ?></div>
						</div>



						<div class="control-group ">
							<label class="control-label">Provider ID</label>
							<div class="controls">
								<input type="text" name="id" id="id" value="<?php echo $dataRecords['id']; ?>" readonly="readonly">
								<?php echo form_error('id', '<p class="error1">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label">Data URL</label>
							<div class="controls">
								<input type="text" name="store_url" id="store_url" value="<?php echo $dataRecords['store_url']; ?>" readonly="readonly">
								<?php echo form_error('store_url', '<p class="error1">'); ?></div>
						</div>

						<div class="control-group ">
							<label class="control-label">Data Password</label>
							<div class="controls">
								<input type="text" name="store_password" id="store_password" value="<?php echo $dataRecords['store_password']; ?>" readonly="readonly">
								<?php echo form_error('store_password', '<p class="error1">'); ?></div>
						</div>


					</div>
				</div>
			</div>

		</div>

		<div class="widget-footer">
			<!-- <button type="submit" class="pull-left btn btn-info btn-small">Submit</button>-->
			<div class="modal-footer" style="padding-left:350px;" >
				<!--<button type="button" class="btn">Cancel</button>
				  <button type="cancel" class="pull-left btn btn-info btn-small">Cancel</button>-->
				<input class="btn btn-primary pull-left" type="submit" value="Save" />
			</div>
		</div>
		<?php echo form_close(); ?> </div> </div>
<!-- /Main window -->
