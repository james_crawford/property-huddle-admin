<!-- Main window -->
<div class="main_container" id="forms_page" style="padding-top:20px; margin-bottom: -35px;">
	<div class="row-fluid" >
		<div class="widget widget-padding span6" style="width:100%;" >
            <div class="widget-header"><i class="icon-list-alt"></i><h5><?php echo $title ?></h5>
                <div class="widget-buttons">
                    <button  onClick="window.location.href='<?php echo site_url("agents"); ?>'"  class="btn" id="cancel">Back</button>
                    <button  onClick="window.location.href='<?php echo site_url("agents/edit/".$agentData['id']); ?>'"  class="btn" id="cancel">Edit</button>
                    <a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>
			<div class="widget-body">
                <div class="widget-forms clearfix" >
                    <table width="100%" cellpadding="2" cellspaceing="2">
                        <tr>
                            <th style="width: 250px; text-align: left">Agent Name</th>
                            <td style="width: 20px;">:</td>
                            <td><?php echo $agentData['agent_name'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Registration Number</th>
                            <td>:</td>
                            <td><?php echo $agentData['company_registration_number'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Company VAT Number</th>
                            <td>:</td>
                            <td><?php echo $agentData['company_vat_number'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Agent Website</th>
                            <td>:</td>
                            <td><?php echo $agentData['agent_website'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Agent Email Address</th>
                            <td>:</td>
                            <td><?php echo $agentData['agent_email_address'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Agent Phone Number</th>
                            <td>:</td>
                            <td><?php echo $agentData['agent_telephone_number'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Trade Contact Name</th>
                            <td>:</td>
                            <td><?php echo $agentData['trade_contact']['trade_contact_name'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Trade Contact Email</th>
                            <td>:</td>
                            <td><?php echo $agentData['trade_contact']['trade_contact_email'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Trade Contact Phone Number</th>
                            <td>:</td>
                            <td><?php echo $agentData['trade_contact']['trade_contact_telephone_number'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Billing Contact Name</th>
                            <td>:</td>
                            <td><?php echo $agentData['billing']['billing_contact_name'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Billing Contact Email</th>
                            <td>:</td>
                            <td><?php echo $agentData['billing']['billing_contact_email'] ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Billing Contact Phone Number</th>
                            <td>:</td>
                            <td><?php echo $agentData['billing']['billing_contact_telephone_number'] ?></td>
                        </tr>
						 <tr>
                            <th style="text-align: left">About the Agent</th>
                            <td>:</td>
                            <td><?php echo $agentData['about_us'] ?></td>
                        </tr>
						
                        <tr>
                            <th style="text-align: left">Status</th>
                            <td>:</td>
                            <td>
								<?php
								if ($agentData['status'])
								{
									?>
									<span class="label label-success">Active</span>
									<?php
								}
								else
								{
									?>
									<span class="label">Inactive</span>
								<?php } ?>
                            </td>
                        </tr>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>


<!-- Agent Brach's start  -->
<div class="main_container" id="users_page">
	<div class="row-fluid">
		<div class="widget widget-padding span6" style="width:100%;">
			<div class="widget-header">
				<i class="icon-group"></i>
				<h5>Agent Branches</h5>
				<div class="widget-buttons">
					<!--<a href="" data-title="Add User" data-toggle="modal" data-target="#example_modal"><i class="icon-plus"></i></a>-->
					<a href="<?php echo site_url('branch/add/'.$agentData['id']); ?>" data-title="Add User" data-toggle="" data-target=""><i class="icon-plus"></i></a>
					<a href="#" data-title="Collapse" data-collapsed="false" class="collapse"><i class="icon-chevron-up"></i></a>
				</div>
            </div>
            <div class="widget-body">
				<table id="users" class="table table-striped table-bordered dataTable">
					<thead>
						<tr>
							<th>Branch Name</th>
							<th>Branch ID</th>
							<th>Branch Email</th>
							<th>Branch Sales Phone</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($branches as $key => $value)
						{
							?>
							<tr>
								<td><?php echo $value['branch_name']; ?></td>
								<td><?php echo $value['id']; ?></td>
								<td><?php echo $value['branch_email_address']; ?></td>
								<td><?php echo $value['branch_sales_telephone_number']; ?></td>
								<td>
									<?php
									if ($value['active'] == '1')
									{
										?>
										<a href="#"  onclick="setBranchVal(<?php echo $value['id'] ?>)"  data-toggle="modal" data-target="#modalDeactivateBranchBox" class="demo_notify_dialog" data-type="confirm"><span class="label label-success">Active</span></a>
										<?php
									}
									else
									{
										?>
										<a href="#"  onclick="setBranchVal(<?php echo $value['id'] ?>)"  data-toggle="modal" data-target="#modalActivateBranchBox" class="demo_notify_dialog" data-type="confirm"><span class="label">Inactive</span></a>
										<?php
									}
									?>
								</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
											Action
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu pull-right">
											<li><a href="<?php echo site_url('branch/add/'.$agentData['id'].'/'.$value['id']); ?>"><i class="icon-edit"></i>New Branch</a></li>
											<li><a href="<?php echo site_url('branch/edit/'.$agentData['id'].'/'.$value['id']); ?>"><i class="icon-edit"></i> Edit </a></li>
											<li><a href="<?php echo site_url('branch/view/'.$agentData['id'].'/'.$value['id']); ?>"><i class="icon-folder-open-alt"></i> View </a></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
            </div> <!-- /widget-body -->
		</div> <!-- /widget -->
	</div>
</div>
<script>
	
	function setBranchVal(branchID) {
		$("#deactivatedBranchID").val(branchID);
		$("#activatedBranchID").val(branchID);
	}
		function activateBranchStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/activateBranchStatus') ; ?>/'+ $("#activatedBranchID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('agents/view');?>/<?php echo $agentData['id'] ?>');
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
		

	}
	function deactivateBranchStatus()
	{
		$.ajax({
			url: '<?php echo site_url('ajax/deactivateBranchStatus') ; ?>/'+ $("#deactivatedBranchID").val(),
			success: function(data)
			{
				window.location.replace('<?php echo site_url('agents/view');?>/<?php echo $agentData['id'] ?>');
			},
			error: function(transport)
			{
				alert('There was an error with your request. Please retry and if the problem persists contact support'+ transport.responseText)
			}
		});
			;
}
	
			
</script>
<!-- Agent Brach's start  -->
<!-- /Main window -->

