<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dataprovider extends MY_Controller
{

	/**
	 * Index Page for this Dataprovider.
	 *
	 * Maps to the following URL
	 * 		http://192.168.2.13/propertyhuddle.com/index.php/dataprovider
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DataproviderModel', '', TRUE);
		$this->load->model('UniversalModel', '', TRUE);
		$this->load->model('Aws_Model', 'aws', TRUE);
		$this->load->model('Statistics_Model', 'statistics', TRUE);
		$this->load->library('session');
	}

	public function index()
	{
		$this->data['title'] = 'Data Provider';
		$this->data['dataRecords'] = $this->UniversalModel->getRecords("dataproviders");

		$this->middle = 'dataprovider';
		parent::template();
	}

## Add New Data Provider Controller

	function add()
	{
		$this->data['title'] = 'Add New - Data Provider';

		$this->form_validation->set_rules('name', 'data provider name', 'trim|required|xss_clean|callback_username_check');
		$this->form_validation->set_rules('address', 'data provider address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'data provider email', 'trim|required|xss_clean|valid_email|callback_email_check');
		$this->form_validation->set_rules('website_url', 'data provider website url', 'trim|required|xss_clean');
		$this->form_validation->set_rules('contact_name', 'data provider contact name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('contact_email', 'data provider contact email', 'trim|required|xss_clean|valid_email');
		$this->form_validation->set_rules('contact_phone', 'data provider contact phone', 'trim|required|xss_clean');


		if (empty($_POST))
		{
			$this->middle = 'add_dataproviders';
		}
		else
		{
			#Rendom Data Provider Id
			$id = $this->get_random_ProviderId();
			#Check Provider Id Already Exists or Not
			$Check = $this->DataproviderModel->checkProviderId($id);
			#Check Provider Id Already Exists or Not
			if ($Check)
			{
				$this->session->set_flashdata('ProviderIdError', 'Please Try Again!');
				$this->middle = 'add_dataproviders';
			}
			# Make Directory For Data Provider

			$folderPath = "../data/".$id;
			$folderName = $id;
			mkdir($folderPath, 0777);
			chmod($folderPath, 0777);
			//chown($folderPath, "propertyhuddle");
			//chgrp($media_path, "propertyhuddle");
			$uploadFolderPath = "../data/".$id.'/upload';
			mkdir($uploadFolderPath, 0777);
			chmod($uploadFolderPath, 0777);



			/* $location = '..data/';
			  $folderName = $id;
			  $folderpath = $location.$folderName;
			  mkdir($folderpath, 0777); */


			#Store Url For data Provider
			$store_url = ' http://data.propertyhuddle.com/'.$id;
			#Store Password For data Provider
			$store_password = $this->random_storePassword();
			$this->aws->sendEmailSES('support@propertyhuddle.com', 'PropertyHuddle', 'notifications@propertyhuddle.com', 'PropertyHuddle', 'Your data account has been set up', "Dear ".$this->input->post('name').", <br /> <br />We are pleased to let you know that your account with Property Huddle has been set up.  In order for us to begin displaying your properties you must upload your data in the RightMove ADF V.3 format using the following details: <br /><br />FTP Host : 217.160.95.91  <br /> FTP Folder : / (direct to your FTP root folder) <br /> FTP Password: ".$store_password." <br /> FTP Username : data".$folderName." <br /><br /> We would like to take this opportunity to welcome you to Property Huddle and many years of excellent servce.  <br /> Regards, <br /> <br /> <br /> <br /> The property Huddle Support Team ");
			$data = array('data_provider_ref' => $folderName,
				'name' => $this->input->post('name'),
				'id' => $id,
				'store_url' => $store_url,
				'store_password' => $store_password,
				'password' => $this->input->post('password'),
				'address' => $this->input->post('address'),
				'email' => $this->input->post('email'),
				'website_url' => $this->input->post('website_url'),
				'contact_name' => $this->input->post('contact_name'),
				'contact_email' => $this->input->post('contact_email'),
				'contact_phone' => $this->input->post('contact_phone'),
				'date_created' => date("Y-m-d H:i:s")
			);
			## Add New Data Provider
			$this->db->insert('dataproviders', $data);
			#$this->session->set_flashdata('Success', 'Data Provider Added Successfully !');

			$this->session->set_flashdata("DataprovidertsMSG", "Dataprovider details added successfully!!");
			$this->session->set_flashdata("DataproviderMSGType", "success");

			redirect("dataprovider");
		}
		parent::template();
	}

#### Edit Data Providers

	function edit()
	{

		$this->data['hedingmenu'] = 'dataprovider';
		$this->data['title'] = 'Edit - Data Provider';

		#Get Dataprovider Id and Check it's Exits in database or Not
		$DataProviderId = $this->uri->segment(3);

		$check = $this->DataproviderModel->checkProviderinId($DataProviderId);

		if (!$check)
		{
			redirect("dataprovider");
		}

		## Get Data From Id
		$where = array('id' => $DataProviderId);
		$dtl = $this->UniversalModel->getRecords("dataproviders", $where);
		$this->data['dataRecords'] = $dtl[0];

		#validate Values
		$this->form_validation->set_rules('name', 'data provider name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('address', 'data provider address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'data provider email', 'trim|required|xss_clean|valid_email|callback_email_check_update');
		$this->form_validation->set_rules('website_url', 'data provider website url', 'trim|required|xss_clean');
		$this->form_validation->set_rules('contact_name', 'data provider contact name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('contact_email', 'data provider contact email', 'trim|required|xss_clean|valid_email');
		$this->form_validation->set_rules('contact_phone', 'data provider contact phone', 'trim|required|xss_clean');


		if ($this->form_validation->run() == FALSE)
		{
			$this->middle = 'editdataproviders';
		}
		else
		{
			$this->middle = 'editdataproviders';
			$data = array(
				'name' => $this->input->post('name'),
				'address' => $this->input->post('address'),
				'email' => $this->input->post('email'),
				'website_url' => $this->input->post('website_url'),
				'contact_name' => $this->input->post('contact_name'),
				'contact_email' => $this->input->post('contact_email'),
				'contact_phone' => $this->input->post('contact_phone'),
				'date_created' => date("Y-m-d H:i:s")
			);
			## Update Data Provider
			$where = 'id';
			$this->session->set_flashdata('Success', 'Data Provider Updated Successfully !');
			$this->table = 'dataproviders';
			$result = $this->UniversalModel->save($this->table, $data, $where, $DataProviderId);
			redirect("dataprovider");
		}
		parent::template();
	}

## Delet Data Provider

	function delete()
	{
		$DataProviderId = $this->uri->segment(3);
		$this->DataproviderModel->DeletDataprovider($DataProviderId);
		$this->session->set_flashdata('Success', 'Data Provider Delete Successfully !');
		redirect("dataprovider");
	}

## Check User name Exits Or Not

	public function username_check($Username)
	{

		$data = array('name' => $Username);
		$result = $this->AdminModel->checkRecord('dataproviders', $data);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('username_check', 'Sorry ! Username already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	## Check Email Already Exista or Not

	public function email_check($Email)
	{

		$data = array('email' => $Email);
		$result = $this->AdminModel->checkRecord('dataproviders', $data);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('email_check', 'Sorry ! Email already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	#Check Email Exits or not on update

	public function email_check_update($Email)
	{

		$data = array('email' => $Email);
		$DpId = $this->uri->segment(3);

		$result = $this->AdminModel->checkRecord_update('dataproviders', $data, $DpId);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('email_check', 'Sorry ! Email already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	### for Genarate Rendom Provider ID

	function get_random_ProviderId($chars_min = 8, $chars_max = 8, $use_upper_case = false, $include_numbers = true, $include_special_chars = false)
	{
		$length = rand($chars_min, $chars_max);
		$selection = '';
		if ($include_numbers)
		{
			$selection .= "1234567890";
		}
		if ($include_special_chars)
		{
			$selection .= "!@04f7c318ad0360bd7b04c980f950833f11c0b1d1quot;#$%&[]{}?|";
		}

		$ProviderId = "";
		for ($i = 0; $i < $length; $i++)
		{
			$current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
			$ProviderId .= $current_letter;
		}

		return $ProviderId;
	}

	### for Genarate Rendom Store passowrs

	function random_storePassword($chars_min = 10, $chars_max = 10, $use_upper_case = false, $include_numbers = true, $include_special_chars = false)
	{
		$length = rand($chars_min, $chars_max);
		$selection = 'aeuoyibcdfghjklmnpqrstvwxz';
		if ($include_numbers)
		{
			$selection .= "1234567890";
		}
		if ($include_special_chars)
		{
			$selection .= "!@04f7c318ad0360bd7b04c980f950833f11c0b1d1quot;#$%&[]{}?|";
		}

		$password = "";
		for ($i = 0; $i < $length; $i++)
		{
			$current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
			$password .= $current_letter;
		}

		return $password;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */