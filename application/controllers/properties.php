<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Properties extends MY_Controller
{

	/**
	 * Index Page for this Dataprovider.
	 *
	 * Maps to the following URL
	 * 		http://192.168.2.13/propertyhuddle.com/index.php/dataprovider
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public $prop_sub_type = array(
			0 => "Not Specified", 1 => "Terraced", 2 => "End of Terrace", 3 => "Semi-Detached",
			4 => "Detached", 5 => "Mews", 6 => "Cluster House", 7 => "Ground Flat", 8 => "Flat",
			9 => "Studio", 10 => "Ground Maisonette", 11 => "Maisonette", 12 => "Bungalow",
			13 => "Terraced Bungalow", 14 => "Semi-Detached Bungalow", 15 => "Detached Bungalow",
			16 => "Mobile Home", 17 => "Hotel", 18 => "Guest House", 19 => "Commercial Property",
			20 => "Land", 21 => "Link Detached House", 22 => "Town House", 23 => "Cottage", 24 => "Chalet",
			27 => "Villa", 28 => "Apartment", 29 => "Penthouse", 30 => "Finca", 43 => "Barn Conversion",
			44 => "Serviced Apartments", 45 => "Parking", 46 => "Sheltered Housing",
			47 => "Retirement Property", 48 => "House Share", 49 => "Flat Share",
			50 => "Park Home", 51 => "Garages", 52 => "Farm House", 53 => "Equestrian",
			56 => "Duplex", 59 => "Triplex", 62 => "Longere", 65 => "Gite", 68 => "Barn",
			71 => "Trulli", 74 => "Mill", 77 => "Ruins", 80 => "Restaurant", 83 => "Cafe",
			86 => "Mill", 89 => "Trulli", 92 => "Castle", 95 => "Village House",
			101 => "Cave House", 104 => "Cortijo", 107 => "Farm Land", 110 => "Plot",
			113 => "Country House", 116 => "Stone House", 117 => "Caravan", 118 => "Lodge",
			119 => "Log Cabin", 120 => "Manor House", 121 => "Stately Home", 125 => "Off-Plan",
			128 => "Semi-detached Villa", 131 => "Detached Villa", 134 => "Bar", 137 => "Shop",
			140 => "Riad", 141 => "House Boat", 142 => "Hotel Room", 143 => "Block of Apartments",
			144 => "Private Halls", 178 => "Office", 181 => "Business Park", 184 => "Serviced Office",
			187 => "Retail Property (high street)", 190 => "Retail Property (out of town)",
			193 => "Convenience Store", 196 => "Garage", 199 => "Hairdresser / Barber Shop",
			202 => "Hotel", 205 => "Petrol Station", 208 => "Post Office", 211 => "Pub",
			214 => "Workshop & Retail space", 217 => "Distribution Warehouse", 220 => "Factory",
			223 => "Heavy Industrial", 226 => "Industrial Park", 229 => "Light Industrial",
			232 => "Storage", 235 => "Showroom", 238 => "Warehouse", 241 => "Land", 244 => "Commercial Development",
			247 => "Industrial Development", 250 => "Residential Development",
			253 => "Commercial Property", 256 => "Data Centre", 259 => "Farm", 262 => "Healthcare Facility",
			265 => "Marine Property", 268 => "Mixed Use", 271 => "Research & Development Facility",
			274 => "Science Park", 277 => "Guest House", 280 => "Hospitality", 283 => "Leisure Facility");
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UniversalModel', '', TRUE);
		$this->load->model('Branchmodel', '', TRUE);
		$this->load->model('DataproviderModel', '', TRUE);
		$this->load->model('AgentModel', '', TRUE);
		$this->load->model('Property_model', 'property', TRUE);
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('Common');
	}

	public function index()
	{
		$this->data['title'] = 'Properties';
		$this->data['propertyData'] = $this->property->loadAllProperties();
		$this->middle = 'properties';
		parent::template();
	}
	
	
	public function view()
	{
		$department = $this->uri->segment(3);
		$propertyRefKey = $this->uri->segment(4);
		$this->data['propertyData'] = $this->property->loadPropertyData($department,$propertyRefKey);
		$this->data['archiveData'] = $this->property->loadArchiveData($propertyRefKey);
		$this->data['title'] = ucwords($department)." - ".$this->data['propertyData']['address_1']." ".$this->data['propertyData']['address_2'].", ".$this->data['propertyData']['town'].", &pound;".number_format($this->data['propertyData']['price'], 0)." - ".$this->data['propertyData']['agent_name']." (".$this->data['propertyData']['branch_name'].")";
		$this->middle = 'view_property_'.$department;
		$this->data['letFrequency'] = array(0=>'Weekly', 1 => 'Monthly', 2 => 'Quarterly', 3 => 'Annual', 5 =>'Per person per week'); 
		$this->data['letType'] = array(0=>'Not Specified', 1 => 'Long Term', 2 => 'Short Term', 3 => 'Student', 4 =>'Commercial'); 
		$this->data['letFurnishType'] = array(0=>'Furnished', 1 => 'Part Furnished', 2 => 'Unfurnished', 3 => 'Not Specified', 4 =>'Furnished/UnFurnished'); 
		$this->data['priceQualifier'] = array('0'=>'', '1'=>'POA', '2'=>'Guide Price', '3'=>'Fixed Price', '4'=>'Offers in Excess of', '5'=>'OIRO', '6'=>'Sale by Tender', '7'=>'From', '9'=>'Shared Ownership', '10'=>'Offers Over', '11'=>'Part Buy Part Rent', '12'=>'Shared Equity');
		$this->data['yesNo'] = array('0'=>'No', '1'=>'Yes', 'N'=>'No', 'Y'=>'Yes');
		$this->data['status'] = array('0'=>'Available', '1'=>'SSTC', '2'=>'SSTCM', '3'=>'Under Offer','4'=>'Reserved', '5'=>'Let Agreed');
		$this->data['transType'] = array('1'=>'Sales', '2'=>'Lettings');
		$this->data['propSubType'] = $this->prop_sub_type;
		$this->data['areaUnits'] = array('0'=>'NA',  '1'=>'sq ft','2'=>'sq ms','3'=>' acres','4'=>'hectares');
		parent::template();
	}
	
	
}

?>
