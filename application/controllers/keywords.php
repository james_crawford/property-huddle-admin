<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Keywords extends MY_Controller
{
## class variables

	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminModel', '', TRUE);
	}

	## Keywords listing section

	public function index()
	{

		$this->data['title'] = "Searches";

		$this->data['keywords'] = $this->UniversalModel->getRecords('searches', NULL, array("id", "DESC"));

		$this->middle = "keywords";

		parent::template();
	}

	## DELETE Keywords data

	public function delete($id = '')
	{

		$this->UniversalModel->deleteRecord("searches", array("id" => $id));

		$this->session->set_flashdata("KeywordMSG", "Keyword deleted successfully!!");

		$this->session->set_flashdata("KeywordMSGType", "success");

		redirect("keywords");
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */