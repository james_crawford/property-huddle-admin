<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends MY_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminModel', '', TRUE);
	}

	public function index()
	{

		$this->data['title'] = 'Login';
		//echo '<pre>'; print_r($_POST);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('login', $this->data);
		}
		else
		{

			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));
			$validation = $this->AdminModel->validateUser($email, $password);
			if ($validation)
			{
				$result = $this->AdminModel->getAdminDetails($email);
				$this->session->set_userdata('id', $result[0]['id']);
				$this->session->set_userdata('Email', $result[0]['Email']);
				$this->session->set_userdata('Name', $result[0]['Name']);
				$this->session->set_userdata('adminLogin', TRUE);
				redirect('dashboard', 'refresh');
			}
			else
			{

				$this->session->set_flashdata('message', 'Invalid Login Details.');
				redirect('login', 'refresh');
			}
		}
	}

	public function checkAdminLogin()
	{
		if ($this->session->userdata("adminLogin") && $this->session->userdata("id") != '')
		{
			return true;
		}
		else
		{
			redirect('login', 'refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */