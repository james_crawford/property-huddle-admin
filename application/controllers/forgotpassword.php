<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Forgotpassword extends MY_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('AdminModel', '', TRUE);
		$this->load->model('SendEmailModel', '', TRUE);
		$this->load->model('TemplateModel', '', TRUE);
		$this->load->library('parser');
	}

	// Forgot password
	public function index()
	{

		$this->data['title'] = 'Forget Password';
		//echo '<pre>'; print_r($_POST);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('forgotpassword', $this->data);
		}
		else
		{

			$email = $this->input->post('email');
			$validation = $this->AdminModel->validateUserEmail($email);
			if (!$validation)
			{
				$this->session->set_flashdata('message', 'Sorry ! No email found.');
				redirect('forgotpassword', 'refresh');
			}
			else
			{

				$result = $this->AdminModel->getAdminDetails($email);

				$result[0]['siteurl'] = site_url('');
				$password = $this->AdminModel->generateRandomPassword(8);
				$this->AdminModel->changepassword($result[0]['id'], $password);

				/* Send Password in mail */
				$resultTemplate = $this->TemplateModel->getTemplateById(1);

				$mixed_search = array("{siteurl}", "{firstname}", "{username}", "{password}");
				$mixed_replace = array($result[0]['siteurl'], $result[0]['Name'], $result[0]['Email'], $password);
				$messagebody = str_replace($mixed_search, $mixed_replace, $resultTemplate[0]['Body']);
				$data = array();

				$data['title'] = $data['heading'] = $resultTemplate[0]['Subject'];
				$data['contents'] = $messagebody;

				$body = $this->parser->parse('emailtemplate/template', $data, TRUE);

				$this->SendEmailModel->sendEmail($result[0]['Email'], $resultTemplate[0]['Subject'], $body, $result[0]['Name']);
				$this->session->set_flashdata('message', 'New password sent to your email.');
				redirect('forgotpassword', 'refresh');
			}
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */