<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users extends MY_Controller
{
## class variables

	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminModel', '', TRUE);
	}

	## User listing section

	public function index()
	{

		$this->data['title'] = "Users";

		$this->data['users'] = $this->UniversalModel->getRecords('users', NULL, array("id", "DESC"));

		$this->middle = "users";

		parent::template();
	}

	## EDIT User data

	public function edit($id = '')
	{

		$this->data['userData'] = $this->UniversalModel->getRecords("users", array("id" => $id));

		if (empty($this->data['userData']))
		{
			$this->session->set_flashdata("agentsMSG", "Agent not exists!!");

			$this->session->set_flashdata("agentsMSGType", "error");

			redirect("users");
		}

		$this->data['title'] = "Edit User Details - ".ucfirst($this->data['userData'][0]['forename']);

		$this->form_validation->set_rules("email_address", "User email", "trim|required|xss_clean|valid_email");

		if ($this->form_validation->run() == FALSE)
		{

			$this->middle = "edit_user";
		}
		else
		{

			$data['title'] = $this->input->post("title");
			$data['forename'] = $this->input->post("forename");
			$data['surname'] = $this->input->post("surname");
			$data['email_address'] = $this->input->post("email_address");
			$data['telephone_number'] = $this->input->post("telephone_number");
			$data['house_name_number'] = $this->input->post("house_name_number");
			$data['street'] = $this->input->post("street");
			$data['address2'] = $this->input->post("address2");
			$data['town'] = $this->input->post("town");
			$data['country'] = $this->input->post("country");
			$data['county'] = $this->input->post("county");
			$data['postcode'] = $this->input->post("postcode");
			$data['house_to_let_sell'] = $this->input->post("house_to_let_sell");
			$data['need_to_move_by'] = $this->input->post("need_to_move_by");
			$data['offers_and_news'] = $this->input->post("newsletter");

			/* echo "<pre/>";
			  print_r($data);
			  die; */
			## Save agent data
			$this->UniversalModel->save("users", $data, "id", $id);

			$this->session->set_flashdata("agentsMSG", "User details updated successfully!!");

			$this->session->set_flashdata("agentsMSGType", "success");

			redirect("users");
		}

		parent::template();
	}

	## DELETE User data

	public function delete($id = '')
	{

		$this->UniversalModel->deleteRecord("users", array("id" => $id));

		$this->session->set_flashdata("agentsMSG", "User deleted successfully!!");

		$this->session->set_flashdata("agentsMSGType", "success");

		redirect("users");
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */