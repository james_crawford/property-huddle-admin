<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends MY_Controller
{

	/**
	 * Index Page for this Dataprovider.
	 *
	 * Maps to the following URL
	 * 		http://192.168.2.13/propertyhuddle.com/index.php/dataprovider
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profilemodel', '', TRUE);
		$this->load->model('UniversalModel', '', TRUE);
	}

	public function index()
	{
		$this->data['title'] = 'Update Profile';
		$result = $this->profilemodel->getprofileDetails($this->session->userdata("id"));
		$this->data['userdetails'] = $result;
		//$this->session->set_userdata('id',$result[0]['id']);


		$required_if = $this->input->post('password') ? '|required' : '';
		$this->form_validation->set_rules('password_confirm', 'Re-type Password', 'trim'.$required_if);
		$this->form_validation->set_rules('password', 'Password', 'trim|matches[password_confirm]');
		$this->form_validation->set_rules('username', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

		if ($this->form_validation->run() == FALSE)
		{
			$this->middle = 'adminprofile';
		}
		else
		{
			$this->profilemodel->update_profile();
			$this->data['message'] = 'Profile update successfully';
			$this->middle = 'adminprofile';
		}
		parent::template();
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */