<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Agents extends MY_Controller
{
## class variables

	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminModel', '', TRUE);
		$this->load->model('AgentModel', '', TRUE);
		$this->load->model('Staff_model', 'staff', TRUE);
		$this->load->library('Common');
		$this->load->model('branchmodel');
	}

	## Agent listing section

	public function index()
	{
		$this->data['title'] = "Agents";
		$agentData = $this->AgentModel->getAgentRecord();
		$this->data['agents'] = $agentData['agents'];
		for ($i = 0; $i < count($this->data['agents']); $i++)
		{
			$this->data['agents'][$i]['branches'] = count($this->UniversalModel->getRecords('branches', array("agents_id" => $this->data['agents'][$i]['id'])));
		}
		$this->middle = "agents";
		parent::template();
	}

## Add new agent details

	public function add()
	{

		$this->data['title'] = "Add New Agent";

		if (!isset($_POST['agent_name']))
		{

			$this->middle = "add_agent";
		}
		else
		{

			$data['agent_name'] = $this->input->post("agent_name");
			$data['agent_location'] = $this->input->post("agent_location");
			$data['about_us'] = $this->input->post("agent_about_us");
			$data['agent_postcode'] = $this->input->post("agent_postcode");
			$data['agent_short_code'] = $this->input->post("agent_short_code");
			$data['agent_type'] = $this->input->post("agent_type");
			$data['company_registration_number'] = $this->input->post("company_registration_number");
			$data['company_vat_number'] = $this->input->post("company_vat_number");
			$data['agent_website'] = $this->input->post("agent_website");
			$data['agent_email_address'] = $this->input->post("agent_email_address");
			$data['external_feed'] = $this->input->post('externalFeed');
			$data['agent_telephone_number'] = $this->input->post("agent_telephone_number");
			$data['date_created'] = date("Y-m-d H:i:s");

			$objlocation = $this->getGeoCoordinates($this->input->post("agent_postcode"));
			$data['latitude'] = $objlocation->lat;
			$data['longitude'] = $objlocation->lng;

			## Save agent data
			$this->UniversalModel->save("agents", $data);

			$this->session->set_flashdata("agentsMSG", "Agent details added successfully!!");

			$this->session->set_flashdata("agentsMSGType", "success");

			redirect("agents");
		}

		parent::template();
	}

	## EDIT agent data

	public function edit($id = '')
	{
		$this->data['agentData'] = $this->AgentModel->getAgentRecord($id, true);
		$this->data['agent'] = $this->AgentModel->getAgentName($id);

		if (empty($this->data['agentData']))
		{
			$this->session->set_flashdata("agentsMSG", "Agent not exists!!");

			$this->session->set_flashdata("agentsMSGType", "error");

			redirect("agents");
		}
		//now get the staff to populate the selctors

		$this->data['staff'] = $this->staff->getBranchStaff($id, true);
		$this->data['title'] = "Edit Agent Details - ".ucfirst($this->data['agentData']['agent_name']);
		if ($this->form_validation->run() == FALSE)
		{

			$this->middle = "edit_agent";
		}
		if (isset($_POST["agent_id"]))
		{
			$data['agent_name'] = $this->input->post("agent_name");
			$data['agent_location'] = $this->input->post("agent_location");
			$data['agent_postcode'] = $this->input->post("agent_postcode");
			$data['about_us'] = $this->input->post("agent_about_us");
			$data['agent_short_code'] = $this->input->post("agent_short_code");
			$data['agent_type'] = $this->input->post("agent_type");
			$data['company_registration_number'] = $this->input->post("company_registration_number");
			$data['company_vat_number'] = $this->input->post("company_vat_number");
			$data['agent_website'] = $this->input->post("agent_website");
			$data['agent_email_address'] = $this->input->post("agent_email_address");
			$data['agent_telephone_number'] = $this->input->post("agent_telephone_number");
			$data['trade_staff_id'] = $this->input->post("trade_staff_id");
			$data['billing_staff_id'] = $this->input->post("billing_staff_id");
			$objlocation = $this->getGeoCoordinates($this->input->post("agent_postcode"));
			$data['external_feed'] = $this->input->post('externalFeed');
			$data['latitude'] = $objlocation['lat'];
			$data['longitude'] = $objlocation['long'];
			## Save agent data
			$this->AgentModel->saveAgent($data, $id);
			$this->session->set_flashdata("agentsMSG", "Agent details updated successfully!!");

			$this->session->set_flashdata("agentsMSGType", "success");

			redirect("agents");
		}

		parent::template();
	}

	## VIEW agent data

	public function view($id = '')
	{

		$this->data['agentData'] = $this->data['agentData'] = $this->AgentModel->getAgentRecord($id, true);

		if (empty($this->data['agentData']))
		{

			$this->session->set_flashdata("agentsMSG", "Agent not exists!!");

			$this->session->set_flashdata("agentsMSGType", "error");

			redirect("agents");
		}

		$this->data['title'] = "View Agent Details - ".ucfirst($this->data['agentData']['agent_name']);

## Agent branches
		$this->data['branches'] = $this->UniversalModel->getRecords("branches", array("agents_id" => $this->data['agentData']['id']));

		$this->middle = "view_agent";

		parent::template();
	}

	## DELETE agent data

	public function delete($id = '')
	{

		$this->UniversalModel->deleteRecord("agents", array("id" => $id));

		$this->session->set_flashdata("agentsMSG", "Agent deleted successfully!!");

		$this->session->set_flashdata("agentsMSGType", "success");

		redirect("agents");
	}

## CHANGE STATUS for agent data

	public function changestatus($id = '', $status)
	{

		$status = ($status) ? 0 : 1;

		$data = array("status" => $status);

		$this->UniversalModel->save("agents", $data, "id", $id);

		$this->session->set_flashdata("agentsMSG", "Agent status updated successfully!!");

		$this->session->set_flashdata("agentsMSGType", "success");

		redirect("agents");
	}

	## Google geo coordinate API

	public function getGeoCoordinates($postcode)
	{
		$this->db->select('lat, long');
		$this->db->from('postcode_lat_long');
		$this->db->where('postcode', $postcode);
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$result = $query->result_array();
			$return['lat'] = $result[0]['lat'];
			$return['long'] = $result[0]['long'];
		}
		else
		{
			$return['latitude'] = "";
			$return['longitude'] = "";
			;
		}
		return $return;
	}

	public function test()
	{
		$this->load->model('AgentModel', '', TRUE);
		$this->AgentModel->getAgentRecord(false, true);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */