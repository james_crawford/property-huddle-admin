<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DataproviderModel', '', TRUE);
		$this->load->model('statistics_Model', 'statistics', TRUE);
		$this->load->library('session');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->data['stats'] = $this->statistics->getDashboardPropertyStatistics();
		$this->data['title'] = 'Dashboard';
		$this->middle = 'index';
		parent::template();
	}

	public function demoData()
	{
		$this->db->select('property_ref_key_id,
					postcode_1,
					postcode_2,
					summary,
					bedrooms,
					price,
					date_created,
					display_address,
					new_home,
					trans_type_id,
					status_id,
					published,
					property_sub_type_id,
					property_ref_key.branches_id');
		$this->db->from('property_residential_sales');
		$this->db->join('property_ref_key', 'property_residential_sales.property_ref_key_id = property_ref_key.id');
		$this->db->where('active', '1');
		$this->db->where('published', '1');
		$query = $this->db->get();
		$this->data['sales'] = $query->result_array();

		$this->db->select('property_ref_key_id,
					postcode_1,
					postcode_2,
					summary,
					bedrooms,
					price,
					date_created,
					display_address,
					new_home,
					trans_type_id,
					status_id,
					published,
					property_sub_type_id,
					property_ref_key.branches_id');
		$this->db->from('property_residential_lettings');
		$this->db->join('property_ref_key', 'property_residential_lettings.property_ref_key_id = property_ref_key.id');
		$this->db->where('active', '1');
		$this->db->where('published', '1');
		$lettingsQuery = $this->db->get();
		$this->data['lettings'] = $lettingsQuery->result_array();


		$this->db->select('property_ref_key_id,
					postcode_1,
					postcode_2,
					summary,
					bedrooms,
					price,
					date_created,
					display_address,
					trans_type_id,
					status_id,
					published,
					property_sub_type_id,
					property_ref_key.branches_id');
		$this->db->from('property_commercial');
		$this->db->join('property_ref_key', 'property_commercial.property_ref_key_id = property_ref_key.id');
		$this->db->where('active', '1');
		$this->db->where('published', '1');
		$comQuery = $this->db->get();
		$this->data['commercial'] = $comQuery->result_array();







		$this->data['title'] = 'Demo Data';

		$this->middle = 'demoData';
		parent::template();
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */