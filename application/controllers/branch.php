<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class branch extends MY_Controller
{

	/**
	 * Index Page for this Dataprovider.
	 *
	 * Maps to the following URL
	 * 		http://192.168.2.13/propertyhuddle.com/index.php/dataprovider
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';
	var $sdfs = '';
	var $imageerrors;
	var $imgerrormessage;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UniversalModel', '', TRUE);
		$this->load->model('Branchmodel', '', TRUE);
		$this->load->model('DataproviderModel', '', TRUE);
		$this->load->model('AgentModel', '', TRUE);
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('Common');
	}

	public function index()
	{
		$this->data['title'] = 'Branch';
		$this->data['dataRecords'] = $this->Branchmodel->getBranchRecords();
		$this->middle = 'branch';
		parent::template();
	}

	function uploadphotos($fieldname)
	{
		$this->imageerrors = '';
		if (!empty($_FILES[$fieldname]['name']))
		{

			if (!$this->upload->do_upload($fieldname))
			{
				$this->imageerrors = $this->upload->display_errors();
				return false;
			}
			else
			{
				$this->data['upload_data'] = $this->upload->data();
				return true;
			}
		}
		else
			return false;
	}

## Add New Data Provider Controller

	function add()
	{
		if ($this->uri->segment(3) != '')
		{
			$this->data['agentID'] = $this->uri->segment(3);
			$this->data['agentName'] = $this->AgentModel->getAgentName($this->data['agentID']);
		}
		else
		{
			$this->data['agentID'] = 0;
		}
		$this->data['agents'] = $this->AgentModel->getAllAgentIDs();
		$this->data['dataProviders'] = $this->DataproviderModel->getDataProviderDropDown();

		//$this->data['agentData'] = $this->UniversalModel->getRecords("agents");
		$this->data['title'] = 'Add New - Branch';

		$this->form_validation->set_rules('branch_name', 'branch name', 'trim|required|xss_clean|callback_username_check');


		if (!isset($_POST['branch_name']))
		{
			$this->middle = 'add_branch';
		}
		else
		{
			//upload images
			$this->load->library('upload');
			$config['upload_path'] = 'assets/branch/';
			$path = $config['upload_path'];
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = '1024';
			$config['max_width'] = '1920';
			$config['max_height'] = '1280';
			$this->upload->initialize($config);
			$branch_office_manager_photo = '';
			$branch_photo = '0';
			$team_photo = '0';
			$branch_logo = '0';
			$imageerrors = false;

			$branchID = '';
			do
			{
				$branchID = $this->UniversalModel->getRandomID();
				$checkID = $this->UniversalModel->checkID("branches", array("id" => $branchID));
			}
			while ($checkID);
			if (!empty($_FILES['branch_photo']['name']))
			{
				if ($this->uploadphotos('branch_photo'))
				{
					$branch_photo = $this->data['upload_data']['file_name'];
					$branchPhotoID = $this->image->saveBranchImage($branch_photo, $branchID, 'Your Local Branch.', '1');
				}
			}
			else
			{
				$branchPhotoID = '0';
			}

			if (!empty($_FILES['team_photo']['name']))
			{
				if ($this->uploadphotos('team_photo'))
				{
					$team_photo = $this->data['upload_data']['file_name'];
					$teamPhotoID = $this->image->saveBranchImage($team_photo, $branchID, 'Your Local Team.', '1');
				}
			}
			else
			{
				$teamPhotoID = '0';
			}

			if (!empty($_FILES['branch_logo']['name']))
			{
				if ($this->uploadphotos('branch_logo'))
				{
					$branch_logo = $this->data['upload_data']['file_name'];
					$logoPhotoID = $this->image->saveBranchImage($branch_logo, $branchID, 'Logo', '1');
				}
			}
			else
			{
				$logoPhotoID = '0';
			}
			//upload images
			if ($imageerrors)
			{
				//$this->data['message']= $this->imgerrormessage;
				$this->data['message'] = 'You got some errors on upload photos'.$this->imageerrors;
				$this->middle = 'add_branch';
			}
			else
			{

			} $objlocation = $this->getGeoCoordinates($this->input->post('branch_location'));
			$branchLat = $objlocation->lat;
			$branchLong = $objlocation->lng;
			$data = array('agents_id' => $this->input->post('agentID'),
				'id' => $branchID,
				'branch_name' => $this->input->post('branch_name'),
				'branch_location' => $this->input->post('branch_location'),
				'branch_postcode' => $this->input->post('branch_postcode'),
				'company_registration_number' => $this->input->post('company_registration_number'),
				'company_vat_number' => $this->input->post('company_vat_number'),
				'branch_website' => $this->input->post('branch_website'),
				'branch_email_address' => $this->input->post('branch_email_address'),
				'branch_sales_telephone_number' => $this->input->post('branch_sales_telephone_number'),
				'branch_lettings_telephone_number' => $this->input->post('branch_lettings_telephone_number'),
				'branch_commercial_telephone_number' => $this->input->post('branch_commercial_telephone_number'),
				'branch_new_homes_telephone_number' => $this->input->post('branch_new_homes_telephone_number'),
				'line_one' => $this->input->post('line_one'),
				'line_two' => $this->input->post('line_two'),
				'postcode' => $this->input->post('postcode'),
				'town' => $this->input->post('town'),
				'external_feed' => $this->input->post('externalFeed'),
				'branch_image_id' => $branchPhotoID,
				'team_image_id' => $teamPhotoID,
				'branch_logo_image_id' => $logoPhotoID,
				'branch_description' => $this->input->post('branch_description'),
				'date_created' => time(),
				'latitude' => $branchLat,
				'longitude' => $branchLong
			);
			## Add New Data Provider
			$this->db->insert('branches', $data);
			$dataproviders = $this->DataproviderModel->getDataProviderDropDown(false, true);
			foreach ($dataproviders as $key => $value)
			{
				$currentDPBranch = $value;
				$branchRefArray[$currentDPBranch]['department_code'] = 0;
				$branchRefArray[$currentDPBranch]['dataproviders_id'] = $key;
				$branchRefArray[$currentDPBranch]['branches_id'] = $branchID;
				if (isset($_POST['sales_branch_ref']) && $_POST['sales_branch_ref'] == $_POST[$currentDPBranch])
				{
					$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('sales_branch_ref');
					$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 1;
				}
				if (isset($_POST['lettings_branch_ref']) && $_POST['lettings_branch_ref'] == $_POST[$currentDPBranch])
				{
					$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('lettings_branch_ref');
					$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 2;
				}
				if (isset($_POST['commercial_branch_ref']) && $_POST['commercial_branch_ref'] == $_POST[$currentDPBranch])
				{
					$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('commercial_branch_ref');
					$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 4;
				}
				if (isset($_POST['landAgency_branch_ref']) && $_POST['landAgency_branch_ref'] == $_POST[$currentDPBranch])
				{
					$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('landAgency_branch_ref');
					$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 8;
				}
			}
			foreach ($branchRefArray as $branchData)
			{
				$this->db->insert('branch_department', $branchData);
			}
			#$this->session->set_flashdata('Success', 'Branch Added Successfully !');
			$this->session->set_flashdata("BranchMSG", "Branch details added successfully!!");
			$this->session->set_flashdata("BranchMSGType", "success");
			if ($this->data['agentID'] != '0')
			{
				redirect("agents/view/".$this->data['agentID']);
			}
			else
			{
				redirect("branch");
			}
		}
		parent::template();
	}

#### Edit Data Providers

	function edit()
	{
		$id = $branchID = $this->uri->segment(4);
		$this->data['branchData'] = $this->Branchmodel->getBranchRecords($id, true);
		if (empty($this->data['branchData']))
		{
			$this->session->set_flashdata("branchMSG", "Branch does not exist!");
			redirect("branch");
		}
		$this->data['dataProviders'] = $this->DataproviderModel->getDataProviderDropDown();
		$this->data['branchRefData'] = $this->Branchmodel->getBranchProfileData($id);
		$this->data['agent'] = $this->AgentModel->getAgentName($this->uri->segment(3));
		$this->data['agentID'] = $this->uri->segment(3);

		$this->data['title'] = "Edit Branch Details - ".ucfirst($this->data['branchData']['branch_name']);
		$this->data['staff'] = $this->staff->getBranchStaff($id);
		$this->form_validation->set_rules('branch_name', 'branch name', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$this->middle = 'edit_branch';
		}
		else
		{
			//upload images
			$this->load->library('upload');
			$config['upload_path'] = 'assets/branch/';
			$path = $config['upload_path'];
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = '1024';
			$config['max_width'] = '1920';
			$config['max_height'] = '1280';
			$this->upload->initialize($config);
			$branch_office_manager_photo = '0';
			$imageerrors = false;
			if (!empty($_FILES['branch_photo']['name']))
			{
				if ($this->uploadphotos('branch_photo'))
				{
					$branch_photo = $this->data['upload_data']['file_name'];
					$branchPhotoID = $this->image->saveBranchImage($branch_photo, $id, 'Your Local Branch.', '1');
				}
			}
			else
			{
				$branchPhotoID = $this->image->getBranchImage($id);
			}

			if (!empty($_FILES['team_photo']['name']))
			{
				if ($this->uploadphotos('team_photo'))
				{
					$team_photo = $this->data['upload_data']['file_name'];
					$teamPhotoID = $this->image->saveBranchImage($team_photo, $id, 'Your Local Team.', '1');
				}
			}
			else
			{
				$teamPhotoID = $this->image->getBranchImage($id, 'team_image_id');
			}

			if (!empty($_FILES['branch_logo']['name']))
			{
				if ($this->uploadphotos('branch_logo'))
				{
					$branch_logo = $this->data['upload_data']['file_name'];
					$LogoPhotoID = $this->image->saveBranchImage($branch_logo, $id, 'Logo', '1');
				}
			}
			else
			{
				$LogoPhotoID = $this->image->getBranchImage($id, 'branch_logo_image_id');
			}

			//upload images
			if ($imageerrors)
			{
				$this->data['message'] = $this->imgerrormessage;
				$this->data['message'] = 'You got some errors on upload photos'.$this->imageerrors;
				$this->middle = 'edit_branch';
			}
			else
			{
				## Save branch data
				$objlocation = $this->getGeoCoordinates($this->input->post('branch_location'));
				$branchLat = $objlocation->lat;
				$branchLong = $objlocation->lng;

				$data = array('agents_id' => $this->data['agentID'],
					'branch_name' => $this->input->post('branch_name'),
					'branch_location' => $this->input->post('branch_location'),
					'branch_postcode' => $this->input->post('branch_postcode'),
					'company_registration_number' => $this->input->post('company_registration_number'),
					'company_vat_number' => $this->input->post('company_vat_number'),
					'branch_website' => $this->input->post('branch_website'),
					'branch_email_address' => $this->input->post('branch_email_address'),
					'branch_sales_telephone_number' => $this->input->post('branch_sales_telephone_number'),
					'branch_lettings_telephone_number' => $this->input->post('branch_lettings_telephone_number'),
					'branch_commercial_telephone_number' => $this->input->post('branch_commercial_telephone_number'),
					'branch_new_homes_telephone_number' => $this->input->post('branch_new_homes_telephone_number'),
					'trade_staff_id' => $this->input->post('trade_staff_id'),
					'billing_staff_id' => $this->input->post('billing_staff_id'),
					'manager_staff_id' => $this->input->post('manager_staff_id'),
					'branch_image_id' => $branchPhotoID,
					'team_image_id' => $teamPhotoID,
					'branch_logo_image_id' => $LogoPhotoID,
					'branch_description' => $this->input->post('branch_description'),
					'external_feed' => $this->input->post('externalFeed'),
					'date_created' => time(),
					'latitude' => $branchLat,
					'longitude' => $branchLong,
					'line_one' => $this->input->post('line_one'),
					'line_two' => $this->input->post('line_two'),
					'postcode' => $this->input->post('postcode'),
					'town' => $this->input->post('town')
				);
				$this->db->where('id', $branchID);
				$this->db->update("branches", $data);
				$dataproviders = $this->DataproviderModel->getDataProviderDropDown(false, true);
				foreach ($dataproviders as $key => $value)
				{
					$currentDPBranch = $value;
					$branchRefArray[$currentDPBranch]['department_code'] = 0;
					$branchRefArray[$currentDPBranch]['dataproviders_id'] = $key;
					$branchRefArray[$currentDPBranch]['branches_id'] = $branchID;
					if ($_POST['sales_branch_ref_hidden'] != '' && $_POST['sales_branch_ref_hidden'] == $_POST[$currentDPBranch])
					{
						$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('sales_branch_ref_hidden');
						$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 1;
					}
					if ($_POST['lettings_branch_ref_hidden'] != '' & $_POST['lettings_branch_ref_hidden'] == $_POST[$currentDPBranch])
					{
						$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('lettings_branch_ref_hidden');
						$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 2;
					}
					if ($_POST['commercial_branch_ref_hidden'] != '' && $_POST['commercial_branch_ref_hidden'] == $_POST[$currentDPBranch])
					{
						$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('commercial_branch_ref_hidden');
						$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 4;
					}
					if ($_POST['landAgency_branch_ref_hidden'] != '' && $_POST['landAgency_branch_ref_hidden'] == $_POST[$currentDPBranch])
					{
						$branchRefArray[$currentDPBranch]['branch_ref'] = $this->input->post('landAgency_branch_ref_hidden');
						$branchRefArray[$currentDPBranch]['department_code'] = $branchRefArray[$currentDPBranch]['department_code'] + 8;
					}
					$this->db->select('branches_id, dataproviders_id, branch_ref');
					$this->db->from('branch_department');
					$this->db->where('branches_id', $branchRefArray[$currentDPBranch]['branches_id']);
					$this->db->where('dataproviders_id', $branchRefArray[$currentDPBranch]['dataproviders_id']);
					$query = $this->db->get();
					if ($query->num_rows() > 0)
					{
						$this->db->where('branches_id', $branchRefArray[$currentDPBranch]['branches_id']);
						$this->db->where('dataproviders_id', $branchRefArray[$currentDPBranch]['dataproviders_id']);
						$this->db->update('branch_department', $branchRefArray[$currentDPBranch]);
					}
					else
					{
						$this->db->insert('branch_department', $branchRefArray[$currentDPBranch]);
					}
				}

				#$this->session->set_flashdata('Success', 'Branch Update Successfully !');
				$this->session->set_flashdata("BranchMSG", "Branch details updated successfully!!");
				$this->session->set_flashdata("BranchMSGType", "success");

				redirect("agents/view/".$this->uri->segment(4));
			}
		}
		parent::template();
	}

## Delet Data Provider

	function delete()
	{
		$branchId = $this->uri->segment(3);
		$branrecord = $this->UniversalModel->getRecords("branches", array("id" => $branchId));
		//echo $branrecord[0]['branch_office_manager_photo'];
		//echo $branrecord[0]['branch_photo'];
		//echo $branrecord[0]['team_photo'];
		//echo $branrecord[0]['branch_logo'];
		//$imgpath=base_url().'assets/branch/';
		$imgpath = './assets/branch/';
		$bompimg = $imgpath.$branrecord[0]['branch_office_manager_photo'];
		$bpimg = $imgpath.$branrecord[0]['branch_photo'];
		$tppimg = $imgpath.$branrecord[0]['team_photo'];
		$blimg = $imgpath.$branrecord[0]['branch_logo'];

		if ($branrecord[0]['branch_office_manager_photo'] != '' and file_exists($bompimg))
			unlink($bompimg);

		if ($branrecord[0]['branch_photo'] != '' and file_exists($bpimg))
			unlink($bpimg);

		if ($branrecord[0]['team_photo'] != '' and file_exists($tppimg))
			unlink($tppimg);

		if ($branrecord[0]['branch_logo'] != '' and file_exists($blimg))
			unlink($blimg);

		$this->UniversalModel->deleteRecord("branches", array("id" => $branchId));
		#$this->session->set_flashdata("Success","Branch deleted successfully!!");
		$this->session->set_flashdata("BranchMSG", "Branch details Deleted successfully!!");
		$this->session->set_flashdata("BranchMSGType", "success");
		redirect("branch");
	}

## Check User name Exits Or Not

	public function username_check($Username)
	{

		$data = array('branch_name' => $Username);
		$result = $this->AdminModel->checkRecord('branches', $data);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('username_check', 'Sorry ! Username already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	## Check Email Already Exista or Not

	public function email_check($Email)
	{

		$data = array('branch_email_address' => $Email);
		$result = $this->AdminModel->checkRecord('branches', $data);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('email_check', 'Sorry ! Email already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	#Check Email Exits or not on update

	public function email_check_update($Email)
	{

		$data = array('email' => $Email);
		$DpId = $this->uri->segment(3);

		$result = $this->AdminModel->checkRecord_update('dataproviders', $data, $DpId);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('email_check', 'Sorry ! Email already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	## Google geo coordinate API

	public function getGeoCoordinates($location)
	{
		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($location).'&sensor=false');
		$output = json_decode($geocode);
		return $output->results[0]->geometry->location;
	}

	public function view($branchId = '')
	{
		$branchId = $this->uri->segment(4);
		$this->data['branchData'] = $this->Branchmodel->getBranchData($branchId, true);
		$this->data['staff'] = $this->staff->getStaffRecords($branchId);
		
		if (empty($this->data['branchData']))
		{
			$this->session->set_flashdata("staffMSG", "Staff Member does not exist!");
			$this->session->set_flashdata("staffMSG", "error");
			redirect("staff");
		}

		$this->data['title'] = "View branch Details - ".ucfirst($this->data['branchData']['branch_name']);

		$this->middle = "view_branch";

		parent::template();
	}


	public function addDemoStaff()
	{
		$this->db->select('id, agents_id');
		$this->db->from('branches');
		$query = $this->db->get();
		$result = $query->result_array();

		foreach ($result as $branch)
		{
			$branchID = $branch['id'];
			$agentID = $branch['agents_id'];
			$data = array(array('branches_id' => $branchID, 'name' => 'trade contact', 'email' => 'trade@email.com', 'telephone' => '01908123456', 'staff_image_id' => 0, 'branch_agent_staff_type' => 0, 'agent_id' => $agentID),
				array('branches_id' => $branchID, 'name' => 'manager staff', 'email' => 'manger@email.com', 'telephone' => '01908356835', 'staff_image_id' => 0, 'branch_agent_staff_type' => 1, 'agent_id' => $agentID),
				array('branches_id' => $branchID, 'name' => 'billing staff', 'email' => 'billing@email.com', 'telephone' => '0190834572472', 'staff_image_id' => 0, 'branch_agent_staff_type' => 0, 'agent_id' => $agentID));
			$this->db->insert_batch('branch_agent_staff', $data);
			;
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */