<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Ajax extends MY_Controller
{

	public function deactivateDataProviderStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('dataproviders', array('active' => '0')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	public function activateDataProviderStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('dataproviders', array('active' => '1')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	public function deactivateAgentStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('agents', array('active' => '0')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	public function activateAgentStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('agents', array('active' => '1')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	public function deactivateBranchStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('branches', array('active' => '0')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	public function activateBranchStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('branches', array('active' => '1')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	public function returnJsonDPS()
	{
		$this->db->select('id, name');
		$this->db->from('dataproviders');
		$this->db->where('active', '1');
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$array = $query->result_array();
			foreach ($array as $dataprovider)
			{
				$name = str_replace(' ', '_', $dataprovider['name']);
				$result[$dataprovider['id']] = $name;
			}

			$jsonResult = json_encode($result);
			echo $jsonResult;
		}
		else
		{
			echo "false";
		}
	}

	public function getAllBranchNames()
	{
		$result[0] = 'Please select a Branch';
		$this->db->select('id, branch_name');
		$this->db->from('branches');
		$this->db->where('active', '1');
		$this->db->where('agents_id', $this->uri->segment(3));
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$array = $query->result_array();
			foreach ($array as $branch)
			{
				$result[$branch['id']] = $branch['branch_name'];
			}
		}
		else
		{
			$result = array('x' => 'No Branches Created');
		}
		$jsonResult = json_encode($result);
		echo $jsonResult;
	}

	public function setBranchRef()
	{
		$branchID = '';
		do
		{
			$branchID = $this->UniversalModel->getRandomID();
			$checkID = $this->UniversalModel->checkID("branches", array("id" => $branchID));
			if ((string) $branchID[0] == 0)
			{
				$checkID = false;
			}
		}
		while ($checkID);
		echo $branchID;
	}

	public function getDPName($id)
	{
		$this->db->select('name');
		$this->db->from('dataproviders');
		$this->db->where('id', $this->uri->segment(3));
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$result = $query->result_array();
			echo $result[0]['name'];
		}
	}

	
	public function getBranchNumbers()
	{
		$this->db->select('branch_sales_telephone_number,branch_lettings_telephone_number,branch_commercial_telephone_number,branch_new_homes_telephone_number');
		$this->db->from('branches');
		$this->db->where('id', $this->uri->segment(3));
		$query = $this->db->get();
		if ($query->num_rows() >= 1)
		{
			$array = $query->result_array();
			if($array[0]['branch_sales_telephone_number'] !='')
			{
			$result['1number_'.$array[0]['branch_sales_telephone_number']] = "(Sales) ".$array[0]['branch_sales_telephone_number'];
			}
			if($array[0]['branch_lettings_telephone_number'] !='')
			{
				$result['2number_'.$array[0]['branch_sales_telephone_number']] = "(Lettings) ".$array[0]['branch_lettings_telephone_number'];
			}
			if($array[0]['branch_commercial_telephone_number'] !='')
			{
				$result['3number_'.$array[0]['branch_sales_telephone_number']] = "(Commercial) ".$array[0]['branch_commercial_telephone_number'];
			}
			if($array[0]['branch_new_homes_telephone_number'] !='')
			{
				$result['4number_'.$array[0]['branch_sales_telephone_number']] = "(New Homes) ".$array[0]['branch_new_homes_telephone_number'];
			}
		}
		if(empty($result))
		{
			$result[0] = 'Please assign phone numbers in branches';
		}else
		{
			$result[0] = 'Please select a telephone number';
		}
		
		$jsonResult = json_encode($result);
		echo $jsonResult;
	}
	
	
	public function deactivateStaffStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('branch_agent_staff', array('active' => '0')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	public function activateStaffStatus()
	{
		$this->db->where('id', $this->uri->segment(3));
		if ($this->db->update('branch_agent_staff', array('active' => '1')))
		{
			echo "done";
		}
		else
		{
			echo "fail";
		}
	}

	
}

?>
