<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class staticpages extends MY_Controller
{

	/**
	 * Index Page for this Dataprovider.
	 *
	 * Maps to the following URL
	 * 		http://192.168.2.13/propertyhuddle.com/index.php/dataprovider
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';
	var $sdfs = '';
	var $imageerrors;
	var $imgerrormessage;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UniversalModel', '', TRUE);

		$this->load->model('Staticpagesmodel', '', TRUE);

		$this->load->library('session');
	}

	public function index()
	{
		$this->data['title'] = 'Static Pages';

		$this->data['dataRecords'] = $this->Staticpagesmodel->getRecords();

		$this->middle = 'staticpages';

		parent::template();
	}

## Add New Page

	function add()
	{

		$this->data['title'] = 'Add Page Content';

		$this->form_validation->set_rules('page_description', 'page description', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{

			$this->middle = 'add_page';
		}
		else
		{

			$data = array('page_name' => $this->input->post('selectpageid'),
				'page_content' => $this->input->post('page_description'),
				'date_created' => date("Y-m-d H:i:s")
			);
			## Add New Data Provider
			$this->db->insert('static_pages', $data);
			$this->session->set_flashdata("PageMSG", "Page content added successfully!!");
			$this->session->set_flashdata("PageMSGType", "success");

			redirect("staticpages");
		}

		parent::template();
	}

#### Edit Data Providers

	function edit()
	{

		redirect("staticpages");
	}

## Delet Data Provider

	function delete()
	{
		$pageId = $this->uri->segment(3);
		$branrecord = $this->Staticpagesmodel->deleterecord("static_pages", array("id" => $pageId));
		#$this->session->set_flashdata("Success","Branch deleted successfully!!");
		$this->session->set_flashdata("PageMSG", "Page details Deleted successfully!!");
		$this->session->set_flashdata("PageMSGType", "success");
		redirect("staticpages");
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */