<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Staff extends MY_Controller
{

	/**
	 * Index Page for this Dataprovider.
	 *
	 * Maps to the following URL
	 * 		http://192.168.2.13/propertyhuddle.com/index.php/dataprovider
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';
	var $sdfs = '';
	var $imageerrors;
	var $imgerrormessage;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UniversalModel', '', TRUE);
		$this->load->model('Branchmodel', '', TRUE);
		$this->load->model('DataproviderModel', '', TRUE);
		$this->load->model('AgentModel', '', TRUE);
		$this->load->model('Staff_model', 'staff', TRUE);
		$this->load->model('BranchModel', '', TRUE);
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('Common');
	}

	public function index()
	{
		$this->data['title'] = 'Branch';
		$this->data['dataRecords'] = $this->Branchmodel->getBranchRecords();
		$this->middle = 'staff';
		parent::template();
	}

	function uploadphotos($fieldname)
	{
		$this->imageerrors = '';
		if (!empty($_FILES[$fieldname]['name']))
		{

			if (!$this->upload->do_upload($fieldname))
			{
				$this->imageerrors = $this->upload->display_errors();
				return false;
			}
			else
			{
				$this->data['upload_data'] = $this->upload->data();
				return true;
			}
		}
		else
			return false;
	}

## Add New Data Provider Controller

	function add()
	{

		if ($this->uri->segment(3) != '')
		{
			$this->data['branchID'] = $this->uri->segment(3);
			$this->data['branchName'] = $this->BranchModel->getBranchName($this->data['branchID']);
			$this->data['telephoneNumbers'] = $this->BranchModel->getBranchNumbers($this->data['branchID']);
			$this->data['branchData'] = $this->BranchModel->getBranchData($this->data['branchID']);
		}
		else
		{
			$this->data['branchID'] = 0;
		}
		$this->data['agents'] = $this->AgentModel->getAllAgentIDs();
		$this->data['branches'] = $this->BranchModel->getAllBranchIDs();
		$this->data['dataProviders'] = $this->DataproviderModel->getDataProviderDropDown();

		//$this->data['agentData'] = $this->UniversalModel->getRecords("agents");
		$this->data['title'] = 'Add New - Staff';

		if (!isset($_POST['staff_fullname']))
		{
			$this->middle = 'add_staff';
		}
		else
		{
			//upload images
			$this->load->library('upload');
			$config['upload_path'] = 'assets/branch/';
			$path = $config['upload_path'];
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = '1024';
			$config['max_width'] = '1920';
			$config['max_height'] = '1280';
			$this->upload->initialize($config);
			$branch_office_manager_photo = '';
			$branch_photo = '0';
			$team_photo = '0';
			$branch_logo = '0';
			$imageerrors = false;

			if (!empty($_FILES['staff_photo']['name']))
			{
				if ($this->uploadphotos('staff_photo'))
				{
					$staff_photo = $this->data['upload_data']['file_name'];
					$staffPhotoID = $this->image->saveBranchImage($staff_photo, $this->data['branchID'], $_POST['staff_fullname'], '1');
				}
			}
			else
			{
				$staffPhotoID = '0';
			}
			if ($imageerrors)
			{
				//$this->data['message']= $this->imgerrormessage;
				$this->data['message'] = 'You got some errors on upload photos'.$this->imageerrors;
				$this->middle = 'add_branch';
			}
			else
			{
				
			}

			if (isset($_POST['staffTelephoneNumber']))
			{
				$telephone = str_replace('1number_', '', $_POST['staffTelephoneNumber']);
				$telephone = str_replace('2number_', '', $telephone);
				$telephone = str_replace('3number_', '', $telephone);
				$telephone = str_replace('4number_', '', $telephone);
			}

			$data = array('agent_id' => $this->input->post('agentID'),
				'name' => $this->input->post('staff_fullname'),
				'email' => $this->input->post('staff_email'),
				'telephone' => $telephone,
				'staff_image_id' => $staffPhotoID,
				'branches_id' => $this->input->post('branchID'),
				'branch_agent_staff_type' => $this->input->post('staff_type')
			);
			## Add New Data Provider
			$this->db->insert('branch_agent_staff', $data);
			if ($this->data['agentID'] != '0')
			{
				redirect("branch/view/".$this->input->post('agentID')."/".$this->input->post('branchID'));
			}
			else
			{
				redirect("branch");
			}
		}
		parent::template();
	}

#### Edit Data Providers

	function edit()
	{

			$this->data['branchID'] = $this->uri->segment(3);
			$this->data['staffID'] = $this->uri->segment(4);
			$this->data['branchName'] = $this->BranchModel->getBranchName($this->data['branchID']);
			$this->data['telephoneNumbers'] = $this->BranchModel->getBranchNumbers($this->data['branchID']);
			$this->data['branchData'] = $this->BranchModel->getBranchData($this->data['branchID']);
			$this->data['staffData'] = $this->staff->getStaffData($this->uri->segment(4));

		$this->data['agents'] = $this->AgentModel->getAllAgentIDs();
		$this->data['branches'] = $this->BranchModel->getAllBranchIDs();
		$this->data['dataProviders'] = $this->DataproviderModel->getDataProviderDropDown();

		//$this->data['agentData'] = $this->UniversalModel->getRecords("agents");
		$this->data['title'] = 'Edit - Staff';

		if (!isset($_POST['staff_fullname']))
		{
			$this->middle = 'edit_staff';
		}
		else
		{
			//upload images
			$this->load->library('upload');
			$config['upload_path'] = 'assets/branch/';
			$path = $config['upload_path'];
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = '1024';
			$config['max_width'] = '1920';
			$config['max_height'] = '1280';
			$this->upload->initialize($config);
			$branch_office_manager_photo = '';
			$branch_photo = '0';
			$team_photo = '0';
			$branch_logo = '0';
			$imageerrors = false;

			if (!empty($_FILES['staff_photo']['name']))
			{
				if ($this->uploadphotos('staff_photo'))
				{
					$staff_photo = $this->data['upload_data']['file_name'];
					$staffPhotoID = $this->image->saveBranchImage($staff_photo, $this->data['branchID'], $_POST['staff_fullname'], '1');
				}
			}
			else
			{
				$staffPhotoID = $this->image->getStaffImage($this->data['staffID']);
			}
			if ($imageerrors)
			{
				//$this->data['message']= $this->imgerrormessage;
				$this->data['message'] = 'You got some errors on upload photos'.$this->imageerrors;
				$this->middle = 'edit_staff';
			}
			else
			{
				
			}

			if (isset($_POST['staffTelephoneNumber']))
			{
				$telephone = str_replace('1number_', '', $_POST['staffTelephoneNumber']);
				$telephone = str_replace('2number_', '', $telephone);
				$telephone = str_replace('3number_', '', $telephone);
				$telephone = str_replace('4number_', '', $telephone);
			}

			$data = array('agent_id' => $this->input->post('agentID'),
				'name' => $this->input->post('staff_fullname'),
				'email' => $this->input->post('staff_email'),
				'telephone' => $telephone,
				'staff_image_id' => $staffPhotoID,
				'branches_id' => $this->input->post('branchID'),
				'branch_agent_staff_type' => $this->input->post('staff_type')
			);
			## Add New Data Provider
			$this->db->where('id', $this->uri->segment(4));
			$this->db->update('branch_agent_staff', $data);
			if ($this->data['agentID'] != '0')
			{
				redirect("branch/view/".$this->input->post('agentID')."/".$this->input->post('branchID'));
			}
			else
			{
				redirect("branch");
			}
		}
		parent::template();
	}

## Delet Data Provider

	function delete()
	{
		$branchId = $this->uri->segment(3);
		$branrecord = $this->UniversalModel->getRecords("branches", array("id" => $branchId));
		//echo $branrecord[0]['branch_office_manager_photo'];
		//echo $branrecord[0]['branch_photo'];
		//echo $branrecord[0]['team_photo'];
		//echo $branrecord[0]['branch_logo'];
		//$imgpath=base_url().'assets/branch/';
		$imgpath = './assets/branch/';
		$bompimg = $imgpath.$branrecord[0]['branch_office_manager_photo'];
		$bpimg = $imgpath.$branrecord[0]['branch_photo'];
		$tppimg = $imgpath.$branrecord[0]['team_photo'];
		$blimg = $imgpath.$branrecord[0]['branch_logo'];

		if ($branrecord[0]['branch_office_manager_photo'] != '' and file_exists($bompimg))
			unlink($bompimg);

		if ($branrecord[0]['branch_photo'] != '' and file_exists($bpimg))
			unlink($bpimg);

		if ($branrecord[0]['team_photo'] != '' and file_exists($tppimg))
			unlink($tppimg);

		if ($branrecord[0]['branch_logo'] != '' and file_exists($blimg))
			unlink($blimg);

		$this->UniversalModel->deleteRecord("branches", array("id" => $branchId));
		#$this->session->set_flashdata("Success","Branch deleted successfully!!");
		$this->session->set_flashdata("BranchMSG", "Branch details Deleted successfully!!");
		$this->session->set_flashdata("BranchMSGType", "success");
		redirect("branch");
	}

## Check User name Exits Or Not

	public function username_check($Username)
	{

		$data = array('branch_name' => $Username);
		$result = $this->AdminModel->checkRecord('branches', $data);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('username_check', 'Sorry ! Username already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	## Check Email Already Exista or Not

	public function email_check($Email)
	{

		$data = array('branch_email_address' => $Email);
		$result = $this->AdminModel->checkRecord('branches', $data);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('email_check', 'Sorry ! Email already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	#Check Email Exits or not on update

	public function email_check_update($Email)
	{

		$data = array('email' => $Email);
		$DpId = $this->uri->segment(3);

		$result = $this->AdminModel->checkRecord_update('dataproviders', $data, $DpId);

		if ($result == TRUE)
		{

			$this->form_validation->set_message('email_check', 'Sorry ! Email already exist.');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}

	## Google geo coordinate API

	public function getGeoCoordinates($location)
	{
		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($location).'&sensor=false');
		$output = json_decode($geocode);
		return $output->results[0]->geometry->location;
	}

	public function view()
	{
		redirect("branch");
	}

	public function addDemoStaff()
	{
		$this->db->select('id, agents_id');
		$this->db->from('branches');
		$query = $this->db->get();
		$result = $query->result_array();

		foreach ($result as $branch)
		{
			$branchID = $branch['id'];
			$agentID = $branch['agents_id'];
			$data = array(array('branches_id' => $branchID, 'name' => 'trade contact', 'email' => 'trade@email.com', 'telephone' => '01908123456', 'staff_image_id' => 0, 'branch_agent_staff_type' => 0, 'agent_id' => $agentID),
				array('branches_id' => $branchID, 'name' => 'manager staff', 'email' => 'manger@email.com', 'telephone' => '01908356835', 'staff_image_id' => 0, 'branch_agent_staff_type' => 1, 'agent_id' => $agentID),
				array('branches_id' => $branchID, 'name' => 'billing staff', 'email' => 'billing@email.com', 'telephone' => '0190834572472', 'staff_image_id' => 0, 'branch_agent_staff_type' => 0, 'agent_id' => $agentID));
			$this->db->insert_batch('branch_agent_staff', $data);
			;
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */