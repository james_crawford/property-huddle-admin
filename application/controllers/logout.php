<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Logout extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	var $error = array();
	var $template = array();
	var $middle = '';
	var $left = '';
	var $right = '';

	public function __construct()
	{
		parent::__construct();

		$this->session->unset_userdata('id');
		$this->session->unset_userdata('Email');
		$this->session->unset_userdata('adminLogin');
		$this->session->set_flashdata('message', 'You are logged out successfully');
	}

	public function index()
	{
		redirect('login', 'refresh');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */