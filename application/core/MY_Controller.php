<?php 
class MY_Controller extends CI_Controller 
 {
  //set the class variable for layout
	var $data      = array();
	var $error     = array();
	
	var $template  = array();
	var $middle   =''; 
	var $left     ='';
	var $right    = '';
	var $header_slider = '';
	var $header_search = '';
	var $breadcrumb = '';

   function MY_Controller()
	{
	
	   parent::__construct();
		
	} 

// This function set the layout for each view
				
	 public function template(){
			
		$this->checkAdminLogin();
		
		$this->data['AdminInfo'] = $this->AdminModel->getAdminDetails();

			
		if (!$this->middle){
				
			 $this->middle = 'index';
		}
		
		$template['header'] = $this->load->view('header', $this->data, true);
		$template['left']   = $this->load->view('left', $this->data, true);
		$template['middle'] = $this->load->view($this->middle, $this->data, true);
		$template['right'] = '';
		$template['footer'] = $this->load->view('footer', $this->data, true);
		$template['data'] = $this->data;
		$this->load->view('template', $template);
	}
	

	public function checkAdminLogin()
	{  
	if($this->session->userdata("id")!=''){
			return true;
		}else{
			redirect('login', 'refresh');
		}
	}

  //load Ckeditor....................
  //$path is relative path of ckeditor
  //$width is width of ckeditor
    
	 function editor($path,$width)
	 {
	  //Load Library For Ckeditor.........
	  $this->load->library('ckeditor');
	  $this->load->library('ckFinder');
	  //configure base path of ckeditor folder 
	  $this->ckeditor->basePath = base_url().'js/ckeditor/';
	  $this->ckeditor-> config['toolbar'] = 'Full';
	  $this->ckeditor->config['language'] = 'en';
	  $this->ckeditor-> config['width'] = $width;
	  //configure ckfinder with ckeditor config 
	  $this->ckfinder->SetupCKEditor($this->ckeditor,$path); 
	 }
	 
				
}