<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/*
 * Library that provide common utilities to classes and methods
 */

class Common
{

	/**
	 * Display variable data in source or screen
	 * @param mixed $var - the variable to be displayed
	 * @param boolean $display - whether to display inthe HTML source on visibly on the page
	 * @return void
	 */
	public static function printd($var, $display = true)
	{
		if ($display)
		{
			echo '<pre>';
		}
		print_r($var);
		if ($display)
		{
			echo '</pre>';
		}
	}

	public static function highlight($var, $display = true)
	{
		if ($display)
		{
			echo '<pre>------------------------------ ';
		}
		echo $var;
		if ($display)
		{
			echo ' ------------------------------</pre>';
		}
	}

	/**
	 * CI does not nativley return a MYSQL result as an ASSOC array
	 * this functon will do the trick
	 *
	 * @param Object $result CI Query result object
	 * @return Array An associative arry of the data provided
	 */
	function assocArray($result)
	{
		foreach ($result as $row)
		{
			$data[] = $row;
		}
		return $data;
	}

	public function convertBool($value)
	{
		print_r($value);
		switch (strtoupper($value))
		{
			case 'Y':
			case 'TRUE':
				{
					return '1';
					break;
				}
			case 'FALSE':
			case 'N':
				{
					return '0';
					break;
				}
			default:
				{
					return $value;
					break;
				}
		}
	}

	function tolatin1($val)
	{
		return strtr(
						$val, array(
					"\x80" => "e", "\x81" => " ", "\x82" => "'", "\x83" => 'f',
					"\x84" => '"', "\x85" => "...", "\x86" => "+", "\x87" => "#",
					"\x88" => "^", "\x89" => "0/00", "\x8A" => "S", "\x8B" => "<",
					"\x8C" => "OE", "\x8D" => " ", "\x8E" => "Z", "\x8F" => " ",
					"\x90" => " ", "\x91" => "`", "\x92" => "'", "\x93" => '"',
					"\x94" => '"', "\x95" => "*", "\x96" => "-", "\x97" => "--",
					"\x98" => "~", "\x99" => "(TM)", "\x9A" => "s", "\x9B" => ">",
					"\x9C" => "oe", "\x9D" => " ", "\x9E" => "z", "\x9F" => "Y"));
	}

	public static function getHash()
	{
		$valid_chars = VALIDCHARS;
		$length = HASHLENGTH;
		// start with an empty random string
		$random_string = "";

		// count the number of chars in the valid chars string so we know how many choices we have
		$num_valid_chars = strlen($valid_chars);

		// repeat the steps until we've created a string of the right length
		for ($i = 0; $i < $length; $i++)
		{
			// pick a random number from 1 up to the number of valid chars
			$random_pick = mt_rand(1, $num_valid_chars);

			// take the random character out of the string of valid chars
			// subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
			$random_char = $valid_chars[$random_pick - 1];

			// add the randomly-chosen char onto the end of our string so far
			$random_string .= $random_char;
		}

		// return our finished random string
		return $random_string;
	}

	public function deptToInt($department)
	{
		switch ($department)
		{
			case'residential_sales':
				{
					return 1;
					break;
				}
			case'residential_lettings':
				{
					return 2;
					break;
				}
			case'commercial':
				{
					return 3;
					break;
				}
		}
	}

	public static function execInBackground($cmd)
	{
		if (substr(php_uname(), 0, 7) == "Windows")
		{
			pclose(popen("start /B ".$cmd, "r"));
		}
		else
		{
			echo " \n ".$cmd." \n ";
			exec($cmd." > /dev/null &");
		}
	}

}

?>
